import thunk from "redux-thunk";
import {
  createStore,
  combineReducers,
  applyMiddleware,
  compose
} from "redux";
import receiptReducer from "./reducers/receiptReducer";
import receiptTypeReducer from "./reducers/receiptTypeReducer";
import profileReducer from "./reducers/profileReducer";
import appReducer from "./reducers/appReducer";
import invoiceReducer from "./reducers/invoiceReducer";
import productReducer from "./reducers/productReducer";
import userReducer from "./reducers/userReducer";
import settingsReducer from "./reducers/settingReducer";
import roleReducer from "./reducers/roleReducer";
import SalesAccountReducer from "./reducers/salesAccountReducer";
import SalesJournalReducer from "./reducers/salesJournalReducer";
import DebtorsJournalReducer from "./reducers/debtorsJournalReducer";
import SalesStaffReducer from "./reducers/salesStaffReducer";
import ExpensesTypeReducer from "./reducers/expensesTypeReducer";
import ExpensesReducer from "./reducers/expensesReducer";
import CreditorReducer from "./reducers/creditorReducer";
import CreditsReducer from "./reducers/creditsReducer";
import CashbookReducer from "./reducers/cashbookReducer";
import PurchaseReducer from "./reducers/purchasesReducer";
import StockReducer from "./reducers/stockReducer";

const combinedReducers = combineReducers({
  profile: profileReducer,
  receipt: receiptReducer,
  receiptType: receiptTypeReducer,
  invoice: invoiceReducer,
  purchases: PurchaseReducer,
  app: appReducer,
  product: productReducer,
  user: userReducer,
  settings: settingsReducer,
  role: roleReducer,
  salesAccounts: SalesAccountReducer,
  salesJournals: SalesJournalReducer,
  debtorsJournals: DebtorsJournalReducer,
  salesStaff: SalesStaffReducer,
  expensesType: ExpensesTypeReducer,
  expenses: ExpensesReducer,
  creditors: CreditorReducer,
  credits: CreditsReducer,
  cashbook: CashbookReducer,
  stock: StockReducer,
});

const InitialStates = {
  profile: {
    name: "",
    email: "",
    id: "",
    phone: "",
    roles: [],
    is_admin: false,
    loading: true,
    passwordMismatch: false,
    passwordResetable: false
  },
  receipt: {
    receiptModalVisible: false,
    addReceiptDrawerVisible: false,
    receiptFilterDrawerVisible: false,
    receiptDetail: [],
    datasource: null,
    loading: false
  },
  invoice: {
    invoiceModalVisible: false,
    invoiceDrawerVisible: false,
    invoiceReceipts: [],
    datasource: null,
    loading: false,
    invoiceDetailDataSource: [],
    prefix: ""
  },
  purchases: {
    purchasesDrawerVisible: false,
    purchasesDetailDataSource: [],
    loading: false,
    datasource: null,
    totalCost: 0,
    totalItems: 0,
    totalQuantity: 0
  },
  receiptType: {
    loading: false,
    datasource: null
  },
  stock: {
    filteredDatasource: [],
    datasource: [],
    loading: false,
    stockDrawerVisible: false,
  },
  product: {
    datasource: null,
    loading: false,
    productId: "",
    productName: "",
    productPrice: "",
    productModalVisible: false,
    productUpdateAction: false,
    productModalTitle: "",
    productSales: {
      data: [],
      info: []
    }
  },
  role: {
    loading: false,
    datasource: null
  },
  user: {
    username: "",
    useremail: "",
    userphone: "",
    userrole: "",
    userUpdateAction: false,
    userModalVisible: false,
    datasource: null,
    loading: false
  },
  app: {
    login: false
  },
  salesAccounts: {
    datasource: [],
    loading: false,
    salesAccountFilterDrawerVisible: false
  },
  salesJournals: {
    datasource: null,
    loading: false,
    salesJournalFilterDrawerVisible: false
  },
  expensesType: {
    loading: false,
    datasource: null,
    addExpensesTypeVisible: false
  },
  expenses: {
    loading: false,
    datasource: null,
    addExpensesVisible: false
  },
  creditors: {
    loading: false,
    datasource: null,
    addCreditorVisible: false,
    creditorUpdateAction: false
  },
  credits: {
    loading: false,
    datasource: null,
    addCreditVisible: false
  },
  cashbook: {
    loading: false,
    datasource: null
  },
  salesStaff: {
    datasource: null,
    loading: false,
    salesDatasource: null,
    journalDatasource: null,
    salesLoading: false,
    journalLoading: false
  },
  debtorsJournals: {
    datasource: null,
    loading: false,
    debtorsJournalFilterDrawerVisible: false,
    debtorDrawerVisible: false
  },
  settings: {
    loading: false,
    logoUrl: "",
    details: []
  }
};
const middleware = [thunk];

const store = createStore(
  combinedReducers,
  InitialStates,
  applyMiddleware(...middleware)

);
// const store = createStore(
//   combinedReducers,
//   InitialStates,
//   compose(
//     applyMiddleware(...middleware),
//     window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
//   )
// );

export default store;