import {
    PostDataService
} from "../../services/post-data-service";
import {
    BASEAPIURL
} from "../../env";
import {
    MakeGraphQLQuery
} from "../../services/utils";
export const UPDATE_SALES_JOURNAL_DATASOURCE = 'UPDATE_SALES_JOURNAL_DATASOURCE';
export const UPDATE_SALES_JOURNAL_LOADING = 'UPDATE_SALES_JOURNAL_LOADING';
export const UPDATE_SALES_JOURNAL_DRAWER_FILTER_VISIBLE = 'UPDATE_SALES_JOURNAL_DRAWER_FILTER_VISIBLE';
export const fetch_salesJournal = (dispatch) => {
    dispatch(update_sales_journal_loading(true))
    const queryName = "salesJournals"

    PostDataService(
        BASEAPIURL,
        MakeGraphQLQuery("query", queryName, {
            orderBy: {
                field: "updated_at",
                order: `DESC`
            }
        }, ["id", "invoice{id num}", "arears", "created_at"]), {
            success: data => {
                if (data.data && data.data[queryName]) {
                    data = data.data[queryName].map((data) => {
                        data['key'] = data.id;
                        return data;
                    })
                    dispatch(update_sales_journal_datasource(data))
                }
            },
            always: () => {
                dispatch(update_sales_journal_loading(false))
            }
        }
    );
}

export const update_sales_journal_datasource = (value) => {
    return {
        type: UPDATE_SALES_JOURNAL_DATASOURCE,
        payload: value
    }
}

export const update_sales_journal_loading = (value) => {
    return {
        type: UPDATE_SALES_JOURNAL_LOADING,
        payload: value
    }
}

export const toggle_sales_journal_filter_drawer_visible = value => {
    return {
        type: UPDATE_SALES_JOURNAL_DRAWER_FILTER_VISIBLE,
        payload: value
    }
}