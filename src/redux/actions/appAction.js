export const APP_LOGIN = "APP_LOGIN";
export const UPDATE_APP_LOGIN = "UPDATE_APP_LOGIN";

export const toggle_app_login = {
    type: APP_LOGIN
}
export const update_app_login = (value) => {
    return {
        type: UPDATE_APP_LOGIN,
        payload: value
    }
}