import {
    PostDataService
} from "../../services/post-data-service";
import {
    BASEAPIURL
} from "../../env";
import {
    MakeGraphQLQuery
} from "../../services/utils";
export const UPDATE_PROFILE = "UPDATE_PROFILE";
export const UPDATE_PROFILE_LOADING = "UPDATE_PROFILE_LOADING";
export const UPDATE_PROFILE_PASSWORD_MISMATCH = "UPDATE_PROFILE_PASSWORD_MISMATCH";
export const UPDATE_PROFILE_PASSWORD_RESETABLE = "UPDATE_PROFILE_PASSWORD_RESETABLE";

export const fetch_profile = dispatch => {
    let queryName = "profile";

    PostDataService(
        BASEAPIURL,
        MakeGraphQLQuery(
            "query",
            "profile",
            [],
            ["name", "email", "id", "phone", "roles{id name}"]
        ), {
            success: data => {
                if (data.data[queryName]) {
                    for (let role of data.data[queryName].roles) {
                        if (role.name.toLowerCase() === "admin") {
                            data.data[queryName]['is_admin'] = true;
                        }
                    }
                    dispatch({
                        type: UPDATE_PROFILE,
                        payload: data.data[queryName]
                    });
                }
            },
            always: () => {
                dispatch(update_profile_loading(false));
            }
        }
    );
};

export const update_profile_datasource = (value) => {
    return {
        type: UPDATE_PROFILE,
        payload: value
    }
}

export const update_profile_password_mismatch = (value) => {
    return {
        type: UPDATE_PROFILE_PASSWORD_MISMATCH,
        payload: value
    }
}

export const update_profile_password_resetable = (value) => {
    return {
        type: UPDATE_PROFILE_PASSWORD_RESETABLE,
        payload: value
    }
}

export const update_profile_loading = value => {
    return {
        type: UPDATE_PROFILE_LOADING,
        payload: value
    };
};