import {
    PostDataService
} from "../../services/post-data-service";
import {
    BASEAPIURL
} from "../../env";
import {
    MakeGraphQLQuery
} from "../../services/utils";
export const UPDATE_PURCHASE_INVOICE_DATASOURCE = "UPDATE_PURCHASE_INVOICE_DATASOURCE";
export const UPDATE_PURCHASE_INVOICE_LOADING = "UPDATE_PURCHASE_INVOICE_LOADING";
export const TOGGLE_PURCHASE_INVOICE_DRAWER_VISIBLE = "TOGGLE_PURCHASE_INVOICE_DRAWER_VISIBLE";
export const UPDATE_PURCHASE_INVOICE_TOTAL_COST = "UPDATE_PURCHASE_INVOICE_TOTAL_COST"
export const UPDATE_PURCHASE_INVOICE_TOTAL_ITEMS = "UPDATE_PURCHASE_INVOICE_TOTAL_ITEMS"
export const UPDATE_PURCHASE_INVOICE_TOTAL_QUANTITY = "UPDATE_PURCHASE_INVOICE_TOTAL_QUANTITY"

export const fetch_purchase_invoices = dispatch => {
    let queryName = "purchaseInvoices";
    dispatch(update_purchase_invoice_loading(true));
    PostDataService(
        BASEAPIURL,
        MakeGraphQLQuery(
            "query",
            queryName, {
                orderBy: {
                    field: "updated_at",
                    order: `DESC`
                }
            },
            [
                "id",
                "number",
                "name",
                "creditor{id name}",
                "amount",
                "balance_bd",
                "carriage_cost",
                "amount_paid",
                "is_served",
                "created_at",
                "is_void"
            ]
        ), {
            success: data => {
                // console.log(data)
                if (data.data[queryName]) {
                    dispatch(
                        update_purchase_invoice_datasource(
                            data.data[queryName].map(dt => {
                                dt["key"] = dt["id"];
                                return dt;
                            })
                        )
                    );
                } else {
                    dispatch(
                        update_purchase_invoice_datasource([])
                    );
                }
            },
            always: () => {
                dispatch(update_purchase_invoice_loading(false));
            }
        }
    );


};

export const update_purchase_invoice_loading = value => {
    return {
        type: UPDATE_PURCHASE_INVOICE_LOADING,
        payload: value
    };
};

export const update_purchase_invoice_datasource = value => {
    return {
        type: UPDATE_PURCHASE_INVOICE_DATASOURCE,
        payload: value
    };
};

export const update_purchase_invoice_total_cost = value => {
    return {
        type: UPDATE_PURCHASE_INVOICE_TOTAL_COST,
        payload: value
    }
}
export const update_purchase_invoice_total_items = value => {
    return {
        type: UPDATE_PURCHASE_INVOICE_TOTAL_ITEMS,
        payload: value
    }
}
export const update_purchase_invoice_total_quantity = value => {
    return {
        type: UPDATE_PURCHASE_INVOICE_TOTAL_QUANTITY,
        payload: value
    }
}

export const update_purchase_invoice_drawer_visible = value => {
    return {
        type:TOGGLE_PURCHASE_INVOICE_DRAWER_VISIBLE,
        payload: value
    };
};