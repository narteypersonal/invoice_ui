import {
    PostDataService
} from "../../services/post-data-service";
import {
    BASEAPIURL
} from "../../env";
import {
    MakeGraphQLQuery
} from "../../services/utils";

export const UPDATE_USERS_DATASOURCE = "UPDATE_USERS_DATASOURCE";
export const UPDATE_USERS_LOADING = "UPDATE_USERS_LOADING";
export const UPDATE_USER_MODAL_VISIBLE = "UPDATE_USER_MODAL_VISIBLE";
export const UPDATE_USER_EDIT_DETAILS = "UPDATE_USER_EDIT_DETAILS";
export const UPDATE_USER_ADD_ACTION = "UPDATE_USER_ADD_ACTION";

export const fetch_users = (dispatch) => {

    dispatch(update_users_loading(true))
    let queryName = "appStaffs";

    PostDataService(
        BASEAPIURL,
        MakeGraphQLQuery(
            "query",
            queryName,
            [],
            ["id", "name", "email", "phone", "roles{id, name}", "is_active"]
        ), {
            success: data => {
                // console.log(data);
                if (data.data[queryName]) {
                    data = data.data[queryName];

                    dispatch(update_users_datasource(data.map(dt => {
                        dt["key"] = dt["id"];
                        return dt;
                    })))

                }
            },
            always: () => {
                dispatch(update_users_loading(false))
            }
        }
    );
}

export const update_users_datasource = (value) => {
    return {
        type: UPDATE_USERS_DATASOURCE,
        payload: value
    }
}

export const update_users_loading = (value) => {
    return {
        type: UPDATE_USERS_LOADING,
        payload: value
    }
}

export const update_user_modal_visible = (value) => {
    return {
        type: UPDATE_USER_MODAL_VISIBLE,
        payload: value
    }
}

export const update_user_edit_details = (value) => {
    return {
        type: UPDATE_USER_EDIT_DETAILS,
        payload: value
    }
}

export const update_user_add_action = (value) => {
    return {
        type: UPDATE_USER_ADD_ACTION,
        payload: value
    }
}