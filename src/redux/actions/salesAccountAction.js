import {
    PostDataService
} from "../../services/post-data-service";
import {
    BASEAPIURL
} from "../../env";
import {
    MakeGraphQLQuery
} from "../../services/utils";

export const UPDATE_SALES_ACCOUNT_DATASOURCE = 'UPDATE_SALES_ACCOUNT_DATASOURCE';
export const UPDATE_SALES_ACCOUNT_LOADING = 'UPDATE_SALES_ACCOUNT_LOADING';
export const UPDATE_SALES_ACCOUNT_DRAWER_FILTER_VISIBLE = 'UPDATE_SALES_ACCOUNT_DRAWER_FILTER_VISIBLE';
export const fetch_salesAccount = (dispatch) => {

    dispatch(update_sales_account_loading(true))
    const queryName = "salesAccounts"

    PostDataService(
        BASEAPIURL,
        MakeGraphQLQuery("query", queryName, {
            orderBy: {
                field: "updated_at",
                order: `DESC`
            }
        }, ["invoice{id num}", "sales_total", "created_at"]), {
            success: data => {
                if (data.data && data.data["salesAccounts"]) {
                    data = data.data["salesAccounts"].map((data) => {
                        data['key'] = data.id;
                        return data;
                    })
                    dispatch(update_sales_account_datasource(data))
                }
            },
            always: () => {
                dispatch(update_sales_account_loading(false))
            }
        }
    );
}


export const update_sales_account_datasource = (value) => {
    return {
        type: UPDATE_SALES_ACCOUNT_DATASOURCE,
        payload: value
    }
}

export const update_sales_account_loading = (value) => {
    return {
        type: UPDATE_SALES_ACCOUNT_LOADING,
        payload: value
    }
}

export const toggle_sales_account_filter_drawer_visible = value => {
    return {
        type: UPDATE_SALES_ACCOUNT_DRAWER_FILTER_VISIBLE,
        payload: value
    }
}