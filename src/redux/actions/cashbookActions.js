import {
    PostDataService
} from "../../services/post-data-service";
import {
    BASEAPIURL
} from "../../env";
import {
    MakeGraphQLQuery
} from "../../services/utils";
export const UPDATE_CASHBOOK_LOADING = "UPDATE_CASHBOOK_LOADING";
export const UPDATE_CASHBOOK_DATASOURCE = "UPDATE_CASHBOOK_DATASOURCE";

export const fetch_cashbooks = dispatch => {
    dispatch(update_cashbook_loading(true));

    let queryName = "cashbooks";

    PostDataService(
        BASEAPIURL,
        MakeGraphQLQuery("query", queryName, {
            orderBy: {
                field: "created_at",
                order: `DESC`
            }
        }, ["id", "folio_name", "folio_number", "folio_id", "credit", "debit", "balance_bd", "description", "created_at"]), {
            success: data => {
                if (data.data[queryName]) {
                    data = data.data[queryName].map(dt => {
                        dt["key"] = dt["id"];
                        return dt;
                    });

                    dispatch(update_cashbook_datasource(data));
                } else {
                    dispatch(update_cashbook_datasource([]));
                }
            },
            always: () => {
                dispatch(update_cashbook_loading(false));
            }
        }
    );
}

export const update_cashbook_loading = value => {
    return {
        type: UPDATE_CASHBOOK_LOADING,
        payload: value
    }
}

export const update_cashbook_datasource = value => {
    return {
        type: UPDATE_CASHBOOK_DATASOURCE,
        payload: value
    }
}