import {
    PostDataService
} from "../../services/post-data-service";
import {
    BASEAPIURL
} from "../../env";
import {
    MakeGraphQLQuery
} from "../../services/utils";

export const UPDATE_EXPENSES_TYPE_DATASOURCE = "UPDATE_EXPENSES_TYPE_DATASOURCE";
export const UPDATE_EXPENSES_TYPE_LOADING = "UPDATE_EXPENSES_TYPE_LOADING";
export const UPDATE_EXPENSE_TYPE_EXPENSES_LOADING = "UPDATE_EXPENSE_TYPE_EXPENSES_LOADING";
export const UPDATE_ADD_EXPENSES_TYPE_VISIBLE = "UPDATE_ADD_EXPENSES_TYPE_VISIBLE";
export const UPDATE_EXPENSE_TYPE_BUTTTON_LOADING = "UPDATE_EXPENSE_TYPE_BUTTTON_LOADING";
export const UPDATE_EXPENSE_TYPE_ACTION = "UPDATE_EXPENSE_TYPE_ACTION";
export const UPDATE_EXPENSE_TYPE_EDIT_DETAILS = "UPDATE_EXPENSE_TYPE_EDIT_DETAILS";
export const REMOVE_EXPENSE_TYPE_DATASOURCE = "REMOVE_EXPENSE_TYPE_DATASOURCE";

export const fetch_expensesType = dispatch => {
    dispatch(update_expenses_type_loading(true));

    let queryName = "expenseTypes";

    PostDataService(
        BASEAPIURL,
        MakeGraphQLQuery("query", queryName, {
            orderBy: {
                field: "name",
                order: `ASC`
            }
        }, ["id", "name", "description", "created_at"]), {
            success: data => {
                if (data.data[queryName]) {
                    data = data.data[queryName].map(dt => {
                        dt["key"] = dt["id"];
                        return dt;
                    });

                    dispatch(update_expenses_type_datasource(data));
                }
            },
            always: () => {
                dispatch(update_expenses_type_loading(false));
            }
        }
    );
}

export const fetch_expenseType_expenses = (dispatch, expenseTypeName = "", callback = () => {}) => {
    dispatch(update_expense_type_expenses_loading(true));
    dispatch(update_expense_type_button_loading({
        [`loading${expenseTypeName}Expenses`]: true
    }))
    return () => {

        let queryName = "expenseTypes";

        PostDataService(
            BASEAPIURL,
            MakeGraphQLQuery("query", queryName, {
                data: {
                    name: expenseTypeName
                },
                orderBy: {
                    field: "name",
                    order: `ASC`
                }
            }, ["id", "name", "description", 'expenses(orderBy: {field: "created_at",order: DESC}){id num amount description created_at expenseType {id name}}']), {
                success: data => {
                    if (data.data[queryName]) {
                        let collection = [];
                        data = data.data[queryName];
                        data.forEach(dt => {
                            dt.expenses.forEach(expense => {
                                expense['key'] = expense.id;
                                collection.push(expense)
                            })
                        })
                        callback(collection);
                    } else
                        return [];
                },
                always: () => {
                    dispatch(update_expense_type_expenses_loading(false));
                    dispatch(update_expense_type_button_loading({
                        [`loading${expenseTypeName}Expenses`]: false
                    }))
                }
            }
        );
    }

}

export const remove_expenseType = (dispatch, id) => {

    return () => {
        dispatch(update_expense_type_button_loading({
            [`removeIconLoading${id}`]: true
        }));
        let queryName = "removeExpenseType";
        let data = {
            data: {
                id: id
            }
        };
        PostDataService(
            BASEAPIURL,
            MakeGraphQLQuery("mutation", queryName, data, ["id", "name", "description", "created_at"]), {
                success: data => {
                    if (data.data[queryName]) {
                        data = data.data[queryName]
                        dispatch(remove_expense_type_datasource(data));
                    }
                },
                always: () => {
                    dispatch(update_expense_type_button_loading({
                        [`removeIconLoading${id}`]: false
                    }));
                }
            }
        );
    }
}


export const update_expenses_type_datasource = value => {
    return {
        type: UPDATE_EXPENSES_TYPE_DATASOURCE,
        payload: value
    }
}

export const update_expenses_type_loading = value => {
    return {
        type: UPDATE_EXPENSES_TYPE_LOADING,
        payload: value
    }
}

export const update_add_expenses_type_visible = value => {
    return {
        type: UPDATE_ADD_EXPENSES_TYPE_VISIBLE,
        payload: value
    }
}

export const update_expense_type_expenses_loading = value => {
    return {
        type: UPDATE_EXPENSE_TYPE_EXPENSES_LOADING,
        payload: value
    }
}

export const update_expense_type_button_loading = (value) => {
    return {
        type: UPDATE_EXPENSE_TYPE_BUTTTON_LOADING,
        payload: value,
    }
}

export const update_expense_type_action = value => {
    return {
        type: UPDATE_EXPENSE_TYPE_ACTION,
        payload: value
    }
}

export const update_expense_type_edit_details = value => {
    return {
        type: UPDATE_EXPENSE_TYPE_EDIT_DETAILS,
        payload: value
    }
}

export const remove_expense_type_datasource = value => {
    return {
        type: REMOVE_EXPENSE_TYPE_DATASOURCE,
        payload: value
    }
}