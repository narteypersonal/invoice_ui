import {
    PostDataService
} from "../../services/post-data-service";
import {
    BASEAPIURL
} from "../../env";
import {
    MakeGraphQLQuery
} from "../../services/utils";

export const UPDATE_EXPENSES_DATASOURCE = "UPDATE_EXPENSES_DATASOURCE";
export const UPDATE_ADD_EXPENSES_VISIBLE = "UPDATE_ADD_EXPENSES_VISIBLE";
export const UPDATE_EXPENSES_LOADING = "UPDATE_EXPENSES_LOADING";
export const UPDATE_EXPENSES_BUTTON_LOADING = "UPDATE_EXPENSES_BUTTON_LOADING";
export const REMOVE_EXPENSES_DATASOURCE = "REMOVE_EXPENSES_DATASOURCE";
export const UPDATE_EXPENSES_EDIT_DETAILS = "UPDATE_EXPENSES_EDIT_DETAILS";
export const UPDATE_EXPENSES_ACTION = "UPDATE_EXPENSES_ACTION";

export const fetch_expenses = dispatch => {
    dispatch(update_expenses_loading(true));

    let queryName = "expenses";

    PostDataService(
        BASEAPIURL,
        MakeGraphQLQuery("query", queryName, {
            orderBy: {
                field: "created_at",
                order: `DESC`
            }
        }, ["id", "amount", "description", "num", "created_at", "expenseType{id name}"]), {
            success: data => {
                if (data.data[queryName]) {
                    data = data.data[queryName].map(dt => {
                        dt["key"] = dt["id"];
                        return dt;
                    });

                    dispatch(update_expenses_datasource(data));
                } else {
                    dispatch(update_expenses_datasource([]));
                }
            },
            always: () => {
                dispatch(update_expenses_loading(false));
            }
        }
    );
}
export const remove_expenses = (dispatch, id) => {

    return () => {
        dispatch(update_expenses_button_loading({
            [`removeIconLoading${id}`]: true
        }));
        let data = {
            data: {
                id: id
            }
        };
        PostDataService(
            BASEAPIURL,
            MakeGraphQLQuery("mutation", "removeExpenses", data, ["id", "num", "description"]), {
                success: data => {
                    if (data.data["removeExpenses"]) {
                        data = data.data["removeExpenses"]
                        dispatch(remove_expenses_datasource(data));
                    }
                },
                always: () => {
                    dispatch(update_expenses_button_loading({
                        [`removeIconLoading${id}`]: false
                    }));
                }
            }
        );
    }
}

export const update_expenses_edit_details = (value) => {
    return {
        type: UPDATE_EXPENSES_EDIT_DETAILS,
        payload: value
    }
}

export const update_expenses_button_loading = value => {
    return {
        type: UPDATE_EXPENSES_BUTTON_LOADING,
        payload: value
    }
}

export const remove_expenses_datasource = value => {
    return {
        type: REMOVE_EXPENSES_DATASOURCE,
        payload: value
    }
}

export const update_expenses_datasource = value => {
    return {
        type: UPDATE_EXPENSES_DATASOURCE,
        payload: value
    }
}

export const update_add_expenses_visible = value => {
    return {
        type: UPDATE_ADD_EXPENSES_VISIBLE,
        payload: value
    }
}

export const update_expenses_loading = value => {
    return {
        type: UPDATE_EXPENSES_LOADING,
        payload: value
    }
}

export const update_expenses_action = (value) => {
    return {
        type: UPDATE_EXPENSES_ACTION,
        payload: value
    }
}