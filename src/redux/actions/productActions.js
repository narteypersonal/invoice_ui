import {
    PostDataService
} from "../../services/post-data-service";
import {
    BASEAPIURL
} from "../../env";
import {
    MakeGraphQLQuery
} from "../../services/utils";

export const UPDATE_PRODUCT_SALES_DATASOURCE = "UPDATE_PRODUCT_SALES_DATASOURCE";
export const UPDATE_PRODUCT_DATASOURCE = "UPDATE_PRODUCT_DATASOURCE";
export const UPDATE_PRODUCT_LOADING = "UPDATE_PRODUCT_LOADING";
export const UPDATE_PRODUCT_MODAL_VISIBLE = "UPDATE_PRODUCT_MODAL_VISIBLE";
export const UPDATE_PRODUCT_ACTION = "UPDATE_PRODUCT_ACTION";
export const UPDATE_PRODUCT_MODAL_TITLE = "UPDATE_PRODUCT_MODAL_TITLE";
export const UPDATE_PRODUCT_EDIT_DETAILS = "UPDATE_PRODUCT_EDIT_DETAILS";
export const UPDATE_PRODUCT_BUTTTON_LOADING = "UPDATE_PRODUCT_BUTTTON_LOADING";
export const REMOVE_PRODUCT_DATASOURCE = "REMOVE_PRODUCT_DATASOURCE";
export const UPDATE_PRODUCT_SALES_LOADING = "UPDATE_PRODUCT_SALES_LOADING";

export const fetch_product_sales = (dispatch, {from ="", to=""}) => {
    return () => {
        dispatch(update_product_sales_loading(true));

        let queryName = "productSales";
        let queryOptions = from && to ? {
            orderBy: {
                field: "quantity",
                order: `ASC`
            },
            created_at: {
                from,
                to
            }} : {
                orderBy: {
                    field: "quantity",
                    order: `ASC`
                }
            }
        PostDataService(
            BASEAPIURL,
            MakeGraphQLQuery("query", queryName, {
                ...queryOptions
            }, []), {
                success: data => {
                    
                    if (data.data[queryName]) {
                        data = JSON.parse(data.data[queryName]);
                        let processedData = [];
                        let productCounts = [];
                        // console.log(data)
                        for(let key in data){
                            let quantity = 0;
                            let sale = 0;
                            processedData.push(
                                data[key].map((dt) => {
                                    quantity += dt.quantity;
                                    sale += dt.sub_total;
                                    return {quantity_sold: dt.quantity, total: dt.sub_total, name: dt.product.name, time: dt.created_at}
                                })
                            );
                            productCounts.push({name:key,quantity, sale});
                        }
                        // console.log(processedData.flat());
                        dispatch(update_product_sales_datasource({data: processedData.flat(), info: productCounts, raw: data}));
                    }
                },
                always: () => {
                    dispatch(update_product_sales_loading(false));
                }
            }
        );
    }
}

export const fetch_products = (dispatch) => {
    return () => {
        dispatch(update_product_loading(true));

        let queryName = "products";

        PostDataService(
            BASEAPIURL,
            MakeGraphQLQuery("query", queryName, {
                orderBy: {
                    field: "name",
                    order: `ASC`
                }
            }, ["id", "name", "price", "quantity", "quantity_sold"]), {
                success: data => {
                    if (data.data[queryName]) {
                        data = data.data[queryName].map(dt => {
                            dt["key"] = dt["id"];
                            return dt;
                        });

                        dispatch(update_product_datasource(data));
                    }
                },
                always: () => {
                    dispatch(update_product_loading(false));
                }
            }
        );
    }
}

export const remove_product = (dispatch, id) => {

    return () => {
        dispatch(update_product_button_loading({
            [`removeIconLoading${id}`]: true
        }));
        let data = {
            data: {
                id: id
            }
        };
        PostDataService(
            BASEAPIURL,
            MakeGraphQLQuery("mutation", "removeProduct", data, ["id", "name"]), {
                success: data => {
                    if (data.data["removeProduct"]) {
                        data = data.data["removeProduct"]
                        dispatch(remove_product_datasource(data));

                    }
                },
                always: () => {
                    dispatch(update_product_button_loading({
                        [`removeIconLoading${id}`]: false
                    }));
                }
            }
        );
    }
}

export const update_product_sales_loading = (value) => {
    return {
        type: UPDATE_PRODUCT_SALES_LOADING,
        payload: value
    }
}
export const update_product_loading = (value) => {
    return {
        type: UPDATE_PRODUCT_LOADING,
        payload: value
    }
}

export const update_product_sales_datasource = (value) => {
    return {
        type: UPDATE_PRODUCT_SALES_DATASOURCE,
        payload: value
    }
}
export const update_product_datasource = (value) => {
    return {
        type: UPDATE_PRODUCT_DATASOURCE,
        payload: value
    }
}

export const update_product_modal_visible = (value) => {
    return {
        type: UPDATE_PRODUCT_MODAL_VISIBLE,
        payload: value
    }
}

export const update_product_edit_details = (value) => {
    return {
        type: UPDATE_PRODUCT_EDIT_DETAILS,
        payload: value
    }
}

export const update_product_modal_title = (value) => {
    return {
        type: UPDATE_PRODUCT_MODAL_TITLE,
        payload: value
    }
}

export const update_product_add_action = (value) => {
    return {
        type: UPDATE_PRODUCT_ACTION,
        payload: value
    }
}

export const update_product_button_loading = (value) => {
    return {
        type: UPDATE_PRODUCT_BUTTTON_LOADING,
        payload: value,
    }
}

export const remove_product_datasource = (value) => {
    return {
        type: REMOVE_PRODUCT_DATASOURCE,
        payload: value,
    }
}