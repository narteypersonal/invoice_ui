import {
    PostDataService
} from "../../services/post-data-service";
import {
    BASEAPIURL
} from "../../env";
import {
    MakeGraphQLQuery
} from "../../services/utils";

export const UPDATE_DEBTOR_JOURNAL_LOADING = "UPDATE_DEBTOR_JOURNAL_LOADING";
export const UPDATE_DEBTOR_JOURNAL_DATASOURCE = "UPDATE_DEBTOR_JOURNAL_DATASOURCE";
export const UPDATE_DEBTOR_DRAWER_VISIBLE = "UPDATE_DEBTOR_DRAWER_VISIBLE";
export const UPDATE_DEBTOR_BUTTON_LOADING = "UPDATE_DEBTOR_BUTTON_LOADING";
export const UPDATE_DEBTOR_DETAIL = "UPDATE_DEBTOR_DETAIL";

export const fetch_debtorsJournal = (dispatch) => {

    dispatch(update_debtor_journal_loading(true))
    const queryName = "debtorsJournal"

    PostDataService(
        BASEAPIURL,
        MakeGraphQLQuery("query", queryName, {
            orderBy: {
                field: "updated_at",
                order: `DESC`
            }
        }, ["id", "customer_no", "total_sale", "amount_paid", "balance_bd", "is_void", "created_at", "updated_at", "invoices(orderBy:{field:\"updated_at\", order: DESC},limit:50){num customer_name customer_no customer_phone customer_email created_at balance amount_recieved is_void}"]), {
            success: data => {
                if (data.data && data.data[queryName]) {
                    data = data.data[queryName].map((data) => {
                        data['key'] = data.id;
                        return data;
                    })
                    dispatch(update_debtor_journal_datasource(data))
                }
            },
            always: () => {
                dispatch(update_debtor_journal_loading(false))
            }
        }
    );
}

export const update_debtor_journal_loading = value => {
    return {
        type: UPDATE_DEBTOR_JOURNAL_LOADING,
        payload: value
    }
}

export const update_debtor_journal_datasource = value => {
    return {
        type: UPDATE_DEBTOR_JOURNAL_DATASOURCE,
        payload: value
    }
}

export const toggle_debtor_detail_drawer_visible = value => {
    return {
        type: UPDATE_DEBTOR_DRAWER_VISIBLE,
        payload: value
    }
}

export const update_debtor_button_loading = value => {
    return {
        type: UPDATE_DEBTOR_BUTTON_LOADING,
        payload: value
    };
};

export const update_debtor_detail = value => {
    return {
        type: UPDATE_DEBTOR_DETAIL,
        payload: value
    };
};
export const show_debtor_detail = (dispatch, record) => {
    dispatch(
        update_debtor_button_loading({
            [`debtorDetailButton${record.id}IsLoading`]: true
        })
    );
    return () => {
        let data = {
            id: record.id,
            customer_no: record.customer_no
        };
        data = {
            data
        };
        PostDataService(
            BASEAPIURL,
            MakeGraphQLQuery("query", "debtorJournal", data, [
                "invoices{id num net_total is_void amount_recieved customer_name created_at balance}"
            ]), {

                success: data => {
                    if (data.data["debtorJournal"]) {
                        data = data.data["debtorJournal"];
                        data["key"] = data.id;
                        dispatch(update_debtor_detail(data.invoices));
                        dispatch(toggle_debtor_detail_drawer_visible(true));


                    }
                },
                always: () => {
                    dispatch(
                        update_debtor_button_loading({
                            [`debtorDetailButton${record.id}IsLoading`]: false
                        })
                    );
                }
            }
        );
    };
};