import {
    PostDataService
} from "../../services/post-data-service";
import {
    BASEAPIURL
} from "../../env";
import {
    MakeGraphQLQuery
} from "../../services/utils";

export const UPDATE_SALES_STAFF_DATASOURCE = "UPDATE_SALES_STAFF_DATASOURCE";
export const UPDATE_SALES_STAFF_LOADING = "UPDATE_SALES_STAFF_LOADING";
export const UPDATE_STAFF_JOURNAL_SALES_DATASOURCE = "UPDATE_STAFF_JOURNAL_SALES_DATASOURCE";
export const UPDATE_STAFF_JOURNAL_SALES_LOADING = "UPDATE_STAFF_JOURNAL_SALES_LOADING";
export const UPDATE_STAFF_ACCOUNT_SALES_LOADING =
    "UPDATE_STAFF_ACCOUNT_SAELES_LOADING";
export const UPDATE_STAFF_ACCOUNT_SALES_DATASOURCE =
    "UPDATE_STAFF_ACCOUNT_SALES_DATASOURCE";
export const fetch_salesStaff = dispatch => {
    dispatch(update_sales_staff_loading(true));
    let queryName = "appSalesStaff";

    PostDataService(
        BASEAPIURL,
        MakeGraphQLQuery(
            "query",
            queryName, {
                orderBy: {
                    field: "name",
                    order: `ASC`
                }
            },
            ["id", "name", "email", "phone", "roles{id, name}", "is_active"]
        ), {
            success: data => {
                // console.log(data);
                if (data.data[queryName]) {
                    data = data.data[queryName];

                    dispatch(
                        update_sales_staff_datasource(
                            data.map(dt => {
                                dt["key"] = dt["id"];
                                return dt;
                            })
                        )
                    );
                }
            },
            always: () => {
                dispatch(update_sales_staff_loading(false));
            }
        }
    );
};

export const fetch_staffJournalSales = (dispatch, record) => {
    return () => {
        dispatch(update_staff_journal_sales_loading(true));
        let queryName = "salesJournals";

        return PostDataService(
            BASEAPIURL,
            MakeGraphQLQuery(
                "query",
                queryName, {
                    data: {
                        staff_id: record.id
                    },
                    orderBy: {
                        field: "created_at",
                        order: `DESC`
                    }
                },
                ["id", "invoice{id num}", "arears", "created_at"]
            ), {
                success: data => {
                    // console.log(data);
                    if (data.data[queryName]) {
                        data = data.data[queryName];

                        dispatch(
                            update_staff_journal_sales_datasource(
                                data.map(dt => {
                                    dt["key"] = dt["id"];
                                    return dt;
                                })
                            )
                        );
                    }
                },
                always: () => {
                    dispatch(update_staff_journal_sales_loading(false));
                }
            }
        );
    }
};
export const fetch_staffAccountSales = (dispatch, record) => {
    return () => {
        dispatch(update_staff_account_sales_loading(true));
        let queryName = "salesAccounts";

        return PostDataService(
            BASEAPIURL,
            MakeGraphQLQuery(
                "query",
                queryName, {
                    data: {
                        staff_id: record.id
                    },
                    orderBy: {
                        field: "created_at",
                        order: `DESC`
                    }
                },
                ["id", "invoice{id num}", "sales_total", "created_at"]
            ), {
                success: data => {
                    // console.log(data);
                    if (data.data[queryName]) {
                        data = data.data[queryName];

                        dispatch(
                            update_staff_account_sales_datasource(
                                data.map(dt => {
                                    dt["key"] = dt["id"];
                                    return dt;
                                })
                            )
                        );
                    }
                },
                always: () => {
                    dispatch(update_staff_account_sales_loading(false));
                }
            }
        );
    }
};
export const update_sales_staff_datasource = value => {
    return {
        type: UPDATE_SALES_STAFF_DATASOURCE,
        payload: value
    };
};

export const update_sales_staff_loading = value => {
    return {
        type: UPDATE_SALES_STAFF_LOADING,
        payload: value
    };
};

export const update_staff_account_sales_datasource = value => {
    return {
        type: UPDATE_STAFF_ACCOUNT_SALES_DATASOURCE,
        payload: value
    };
};

export const update_staff_journal_sales_datasource = value => {
    return {
        type: UPDATE_STAFF_JOURNAL_SALES_DATASOURCE,
        payload: value
    };
}

export const update_staff_account_sales_loading = value => {
    return {
        type: UPDATE_STAFF_ACCOUNT_SALES_LOADING,
        payload: value
    };
};
export const update_staff_journal_sales_loading = value => {
    return {
        type: UPDATE_STAFF_JOURNAL_SALES_LOADING,
        payload: value
    };
};