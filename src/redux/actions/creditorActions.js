import {
    PostDataService
} from "../../services/post-data-service";
import {
    BASEAPIURL
} from "../../env";
import {
    MakeGraphQLQuery
} from "../../services/utils";
export const UPDATE_CREDITOR_DATASOURCE = "UPDATE_CREDITOR_DATASOURCE";
export const UPDATE_CREDITORS_LOADING = "UPDATE_CREDITORS_LOADING";
export const UPDATE_ADD_CREDITOR_VISIBLE = "UPDATE_ADD_CREDITOR_VISIBLE";
export const UPDATE_CREDITOR_BUTTON_LOADING = "UPDATE_CREDITOR_BUTTON_LOADING";
export const UPDATE_CREDITOR_CREDITS_LOADING = "UPDATE_CREDITOR_CREDITS_LOADING";
export const UPDATE_CREDITOR_ACTION = "UPDATE_CREDITOR_ACTION";
export const UPDATE_CREDITOR_EDIT_DETAILS = "UPDATE_CREDITOR_EDIT_DETAILS";
export const REMOVE_CREDITOR_DATASOURCE = "REMOVE_CREDITOR_DATASOURCE";

export const fetch_creditors = dispatch => {
    dispatch(update_creditors_loading(true));

    let queryName = "creditors";

    PostDataService(
        BASEAPIURL,
        MakeGraphQLQuery("query", queryName, {
            orderBy: {
                field: "created_at",
                order: `DESC`
            }
        }, ["id", "name", "contact", "description", "created_at"]), {
            success: data => {
                if (data.data[queryName]) {
                    data = data.data[queryName].map(dt => {
                        dt["key"] = dt["id"];
                        return dt;
                    });

                    dispatch(update_creditors_datasource(data));
                } else {
                    dispatch(update_creditors_datasource([]));
                }
            },
            always: () => {
                dispatch(update_creditors_loading(false));
            }
        }
    );
}

export const fetch_creditor_credits = (dispatch, creditor = "", callback = () => {}) => {
    dispatch(update_creditor_credits_loading(true));
    dispatch(update_creditor_button_loading({
        [`loading${creditor.name}Creditor`]: true
    }))
    return () => {

        let queryName = "creditors";

        PostDataService(
            BASEAPIURL,
            MakeGraphQLQuery("query", queryName, {
                data: {
                    id: creditor.id
                }
            }, ["id", "name", 'credits(orderBy: {field: "created_at",order: DESC}){id number debit credit description created_at}']), {
                success: data => {
                    if (data.data[queryName]) {
                        let collection = [];
                        data = data.data[queryName];
                        data.forEach(dt => {
                            dt.credits.forEach(credit => {
                                credit['key'] = credit.id;
                                collection.push(credit)
                            })
                        })
                        callback(collection);
                    } else
                        return [];
                },
                always: () => {
                    dispatch(update_creditor_credits_loading(false));
                    dispatch(update_creditor_button_loading({
                        [`loading${creditor.name}Creditor`]: false
                    }))
                }
            }
        );
    }

}

export const remove_creditor = (dispatch, id) => {

    return () => {
        dispatch(update_creditor_button_loading({
            [`removeIconLoading${id}`]: true
        }));
        let queryName = "removeCreditor";
        let data = {
            data: {
                id: id
            }
        };
        PostDataService(
            BASEAPIURL,
            MakeGraphQLQuery("mutation", queryName, data, ["id", "name", "contact", "description"]), {
                success: data => {
                    if (data.data[queryName]) {
                        data = data.data[queryName]
                        dispatch(remove_creditor_datasource(data));
                    }
                },
                always: () => {
                    dispatch(update_creditor_button_loading({
                        [`removeIconLoading${id}`]: false
                    }));
                }
            }
        );
    }
}

export const update_creditors_datasource = value => {
    return {
        type: UPDATE_CREDITOR_DATASOURCE,
        payload: value
    }
}

export const update_creditors_loading = value => {
    return {
        type: UPDATE_CREDITORS_LOADING,
        payload: value
    }
}

export const update_add_creditor_visible = value => {
    return {
        type: UPDATE_ADD_CREDITOR_VISIBLE,
        payload: value
    }
}

export const update_creditor_button_loading = value => {
    return {
        type: UPDATE_CREDITOR_BUTTON_LOADING,
        payload: value
    }
}

export const update_creditor_action = value => {
    return {
        type: UPDATE_CREDITOR_ACTION,
        payload: value
    }
}

export const update_creditor_edit_details = value => {
    return {
        type: UPDATE_CREDITOR_EDIT_DETAILS,
        payload: value
    }
}
export const update_creditor_credits_loading = value => {
    return {
        type: UPDATE_CREDITOR_CREDITS_LOADING,
        payload: value
    }
}



export const remove_creditor_datasource = value => {
    return {
        type: REMOVE_CREDITOR_DATASOURCE,
        payload: value
    }
}