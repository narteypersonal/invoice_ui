import {
  PostDataService
} from "../../services/post-data-service";
import {
  BASEAPIURL
} from "../../env";
import {
  MakeGraphQLQuery
} from "../../services/utils";
export const UPDATE_INVOICE_DATASOURCE = "UPDATE_INVOICE_DATASOURCE";
export const UPDATE_INVOICE_LOADING = "UPDATE_INVOICE_LOADING";
export const UPDATE_INVOICE_MODAL_VISIBLE = "TOGGLE_INVOICE_MODAL_VISIBLE";
export const UPDATE_INVOICE_BUTTON_LOADING = "UPDATE_INVOICE_BUTTON_LOADING";
export const UPDATE_INVOICE_DATA_REPLACE = "UPDATE_INVOICE_DATA_REPLACE";
export const UPDATE_INVOICE_DETAIL_MODAL_VISIBLE =
  "UPDATE_INVOICE_DETAIL_MODAL_VISIBLE";
export const UPDATE_INVOICE_DETAIL = "UPDATE_INVOICE_DETAIL";
export const UPDATE_INVOICE_PREFIX = "UPDATE_INVOICE_PREFIX";
export const UPDATE_INVOICE_VIA_RECEIPT = "UPDATE_INVOICE_VIA_RECEIPT";
export const TOGGLE_INVOICE_DRAWER_VISIBLE = "TOGGLE_INVOICE_DRAWER_VISIBLE";
export const UPDATE_INVOICE_RECEIPTS = "UPDATE_INVOICE_RECEIPTS";
export const fetch_invoices = dispatch => {
  let queryName = "invoices";
  dispatch(update_invoice_loading(true));
  PostDataService(
    BASEAPIURL,
    MakeGraphQLQuery(
      "query",
      queryName, {
        orderBy: {
          field: "updated_at",
          order: `DESC`
        }
      },
      [
        "id",
        "num",
        "customer_name",
        "customer_no",
        "net_total",
        "discount_total",
        "amount_recieved",
        "balance",
        "created_at",
        "is_void"
      ]
    ), {
      success: data => {
        if (data.data[queryName]) {
          dispatch(
            update_invoice_datasource(
              data.data[queryName].map(dt => {
                dt["key"] = dt["id"];
                return dt;
              })
            )
          );
        } else {
          dispatch(
            update_invoice_datasource([])
          );
        }
      },
      always: () => {
        dispatch(update_invoice_loading(false));
      }
    }
  );


};
export const update_invoice_validity = (dispatch, record) => {
  dispatch(
    update_invoice_button_loading({
      [`invoiceValidityButton${record.id}IsLoading`]: true
    })
  );

  let data = {
    id: record.id,
    is_void: !record.is_void
  };
  data = {
    data
  };

  return () => {
    PostDataService(
      BASEAPIURL,
      MakeGraphQLQuery("mutation", "changeInvoiceValidity", data, [
        "id",
        "num",
        "customer_name",
        "customer_no",
        "net_total",
        "discount_total",
        "amount_recieved",
        "balance",
        "created_at",
        "is_void"
      ]), {
        success: data => {
          if (data.data["changeInvoiceValidity"]) {
            data = data.data["changeInvoiceValidity"];
            data["key"] = data.id;
            
            dispatch({
              type: UPDATE_INVOICE_DATA_REPLACE,
              payload: data
            });
          }
        },
        always: () => {
          dispatch(
            update_invoice_button_loading({
              [`invoiceValidityButton${record.id}IsLoading`]: false
            })
          );
        }
      }
    );
  };
};

export const show_invoice_detail = (dispatch, record) => {
  dispatch(
    update_invoice_button_loading({
      [`invoiceDetailButton${record.id}IsLoading`]: true
    })
  );
  return () => {
    let data = {
      id: record.id
    };
    data = {
      data
    };
    PostDataService(
      BASEAPIURL,
      MakeGraphQLQuery("query", "invoice", data, [
        "id num net_total gross_total is_void amount_recieved discount_total tax_total customer_name customer_no customer_phone customer_email created_at balance receipts(orderBy:{field:\"updated_at\", order: DESC}){id,customer_no,amount,description,depositor, invoice{id, num}, receipttype{id, name}, created_at,is_void } listProducts{product{name},   quantity   sub_total   tax_rate   discount_rate }"
      ]), {

        success: data => {
          if (data.data["invoice"]) {
            data = data.data["invoice"];
            data["key"] = data.id;
            
            dispatch(update_invoice_detail(data));
            dispatch(toggle_invoice_drawer_visible(true));
            dispatch(update_invoice_receipts(data.receipts));

          }
        },
        always: () => {
          dispatch(
            update_invoice_button_loading({
              [`invoiceDetailButton${record.id}IsLoading`]: false
            })
          );
        }
      }
    );
  };
};

export const update_invoice_loading = value => {
  return {
    type: UPDATE_INVOICE_LOADING,
    payload: value
  };
};

export const update_invoice_modal_visible = value => {
  return {
    type: UPDATE_INVOICE_MODAL_VISIBLE,
    payload: value
  };
};

export const update_invoice_datasource = value => {
  return {
    type: UPDATE_INVOICE_DATASOURCE,
    payload: value
  };
};

export const update_invoice_via_receipt = value => {
  return {
    type: UPDATE_INVOICE_VIA_RECEIPT,
    payload: value
  }
}

export const update_invoice_detail = value => {
  return {
    type: UPDATE_INVOICE_DETAIL,
    payload: value
  };
};

export const update_invoice_detail_modal_visible = value => {
  return {
    type: UPDATE_INVOICE_DETAIL_MODAL_VISIBLE,
    payload: value
  };
};

export const update_invoice_button_loading = value => {
  return {
    type: UPDATE_INVOICE_BUTTON_LOADING,
    payload: value
  };
};

export const update_invoice_prefix = value => {
  return {
    type: UPDATE_INVOICE_PREFIX,
    payload: value
  };
};

export const update_invoice_receipts = value => {
  return {
    type: UPDATE_INVOICE_RECEIPTS,
    payload: value
  }
}

export const get_invoice_prefix = dispatch => {
  return () => {
    PostDataService(
      BASEAPIURL,
      MakeGraphQLQuery("query", "invoiceNumber", [], ["prefix"]), {
        success: data => {
          
          if (data.data["invoiceNumber"]) {
            dispatch(update_invoice_prefix(data.data["invoiceNumber"]));
          }
        }
      }
    );
  };
};

export const toggle_invoice_drawer_visible = value => {
  return {
    type: TOGGLE_INVOICE_DRAWER_VISIBLE,
    payload: value
  }
}