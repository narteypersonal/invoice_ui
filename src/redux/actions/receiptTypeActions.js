import {
    PostDataService
} from "../../services/post-data-service";
import {
    BASEAPIURL
} from "../../env";
import {
    MakeGraphQLQuery
}
from "../../services/utils";

export const UPDATE_RECEIPTTYPE_DATA = "UPDATE_RECEIPTTYPE_DATA";
export const UPDATE_RECEIPTTYPE_LOADING = "UPDATE_RECEIPTTYPE_LOADING";

export const fetch_receiptType = (dispatch) => {
    dispatch(update_receipttype_loading(true));
    let receiptTypeData = JSON.parse(sessionStorage.getItem("receiptTypeData"));
    let queryName = "receiptTypes";
    if (receiptTypeData !== null &&
        Object.keys(receiptTypeData).length > 0) {
        dispatch({
            type: UPDATE_RECEIPTTYPE_DATA,
            payload: receiptTypeData.map(dt => {
                dt["key"] = dt["id"];
                return dt;
            })
        });
        dispatch(update_receipttype_loading(false));
        return;
    }
    PostDataService(
        BASEAPIURL,
        MakeGraphQLQuery("query", queryName, [], ["id", "name"]), {
            success: data => {
                // console.log(data);
                if (data.data[queryName]) {

                    sessionStorage.setItem(
                        "receiptTypeData",
                        JSON.stringify(
                            data.data[queryName]
                        )
                    );
                    dispatch({
                        type: UPDATE_RECEIPTTYPE_DATA,
                        payload: data.data[queryName].map(dt => {
                            dt["key"] = dt["id"];
                            return dt;
                        })
                    });
                }
            },
            always: () => {
                dispatch(update_receipttype_loading(false));
            }
        }
    );

}

export const update_receipttype_loading = (value) => {
    return {
        type: UPDATE_RECEIPTTYPE_LOADING,
        payload: value
    }
}