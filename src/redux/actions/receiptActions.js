import {
    PostDataService
} from "../../services/post-data-service";
import {
    BASEAPIURL
} from "../../env";
import {
    MakeGraphQLQuery
} from "../../services/utils";
export const UPDATE_RECEIPT_DATASOURCE = "UPDATE_RECEIPT_DATASOURCE";
export const UPDATE_LOADING = "UPDATE_LOADING";
export const UPDATE_SEARCHTEXT = "UPDATE_SEARCHTEXT";
export const TOGGLE_RECEIPT_MODAL_VISIBLE = "TOGGLE_RECEIPT_MODAL_VISIBLE";
export const UPDATE_RECEIPT_DATA_REPLACE = "UPDATE_RECEIPT_DATA_REPLACE";
export const UPDATE_RECEIPT_BUTTON_LOADING = "UPDATE_RECEIPT_BUTTON_LOADING";
export const UPDATE_RECEIPT_DETAIL = "UPDATE_RECEIPT_DETAIL";
export const TOGGLE_ADD_RECEIPT_DRAWER_VISIBLE = "TOGGLE_ADD_RECEIPT_DRAWER_VISIBLE";
export const TOGGLE_RECEIPT_FILTER_DRAWER_VISIBLE = "TOGGLE_RECEIPT_FILTER_DRAWER_VISIBLE";

export const fetch_receipt = (dispatch) => {
    dispatch(update_loading(true));
    let queryName = "receipts";

    PostDataService(
        BASEAPIURL,
        MakeGraphQLQuery("query", queryName, {
            orderBy: {
                field: "updated_at",
                order: `DESC`
            }
        }, ["id", "description", "depositor", "amount", "invoice{id, num}", "receipttype{id, name}", "created_at", "is_void"]), {
            success: data => {
                if (data.data[queryName]) {

                    dispatch({
                        type: UPDATE_RECEIPT_DATASOURCE,
                        payload: data.data[queryName].map(dt => {
                            dt["key"] = dt["id"];
                            return dt;
                        })
                    });
                } else {
                    dispatch({
                        type: UPDATE_RECEIPT_DATASOURCE,
                        payload: []
                    });
                }
            },
            always: () => {
                dispatch(update_loading(false));
            }
        }
    );

}

export const update_receipt_validity = (dispatch, record) => {
    dispatch(update_receipt_button_loading({
        [`receiptValidityButton${record.id}IsLoading`]: true
    }));

    let data = {
        id: record.id,
        is_void: !record.is_void
    };
    data = {
        data
    };

    return () => {
        PostDataService(
            BASEAPIURL,
            MakeGraphQLQuery("mutation", "changeReceiptValidity", data, [
                "id",
                "invoice{ id num}",
                "depositor",
                "description",
                "amount",
                "receipttype{id name}",
                "created_at",
                "is_void"
            ]), {
                success: data => {
                    if (data.data["changeReceiptValidity"]) {
                        data = data.data["changeReceiptValidity"];
                        data['key'] = data.id
                        console.log(data);
                        dispatch({
                            type: UPDATE_RECEIPT_DATA_REPLACE,
                            payload: data
                        });
                    }
                },
                always: () => {
                    dispatch(update_receipt_button_loading({
                        [`receiptValidityButton${record.id}IsLoading`]: false
                    }))
                }
            }
        );
    }

}

export const update_receipt_button_loading = value => {
    return {
        type: UPDATE_RECEIPT_BUTTON_LOADING,
        payload: value
    }
}

export const update_loading = (value) => {
    return {
        type: UPDATE_LOADING,
        payload: value
    }
}

export const updated_searchText = (value) => {
    return {
        type: UPDATE_SEARCHTEXT,
        payload: value
    }
}

export const update_receipt_datasource = (value) => {
    return {
        type: UPDATE_RECEIPT_DATASOURCE,
        payload: value
    }
}
export const update_receipt_detail = (value) => {
    return {
        type: UPDATE_RECEIPT_DETAIL,
        payload: value
    }
}

export const toggle_receipt_modal_visible = (value) => {
    return {
        type: TOGGLE_RECEIPT_MODAL_VISIBLE,
        payload: value
    }
}
export const toggle_add_receipt_drawer_visible = (value) => {
    return {
        type: TOGGLE_ADD_RECEIPT_DRAWER_VISIBLE,
        payload: value
    }
}
export const toggle_receipt_filter_drawer_visible = (value) => {
    return {
        type: TOGGLE_RECEIPT_FILTER_DRAWER_VISIBLE,
        payload: value
    }
}