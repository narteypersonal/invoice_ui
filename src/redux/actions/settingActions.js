import {
    PostDataService
} from "../../services/post-data-service";
import {
    BASEAPIURL
} from "../../env";
import {
    MakeGraphQLQuery
} from "../../services/utils";

export const UPDATE_SETTINGS_LOADING = "UPDATE_SETTINGS_LOADING";
export const UPDATE_SETTINGS_DATASOURCE = "UPDATE_SETTINGS_DATASOURCE";

export const fetch_settings = dispatch => {
    dispatch(update_settings_loading(true))
    PostDataService(
        BASEAPIURL,
        MakeGraphQLQuery(
            "query",
            "setting",
            [],
            [
                "company_name",
                "company_email",
                "company_phone",
                "company_address",
                "logo_url"
            ]
        ), {
            success: data => {

                if (data.data["setting"]) {
                    dispatch(update_settings_datasource(data.data["setting"]))
                }
            },
            always: () => {
                dispatch(update_settings_loading(false))
            }
        }
    );
}

export const update_settings_loading = value => {
    return {
        type: UPDATE_SETTINGS_LOADING,
        payload: value
    }
}

export const update_settings_datasource = value => {
    return {
        type: UPDATE_SETTINGS_DATASOURCE,
        payload: value
    }
}