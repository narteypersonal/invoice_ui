import {
    PostDataService
} from "../../services/post-data-service";
import {
    BASEAPIURL
} from "../../env";
import {
    MakeGraphQLQuery
} from "../../services/utils";

export const UPDATE_CREDITS_DATASOURCE = "UPDATE_CREDITS_DATASOURCE";
export const UPDATE_CREDITS_LOADING = "UPDATE_CREDITS_LOADING";
export const UPDATE_CREDITS_ACTION = "UPDATE_CREDITS_ACTION";
export const UPDATE_CREDITS_EDIT_DETAILS = "UPDATE_CREDITS_EDIT_DETAILS";
export const UPDATE_CREDIT_VISIBLE = "UPDATE_CREDIT_VISIBLE";
export const UPDATE_ADD_CREDIT_VISIBLE = "UPDATE_ADD_CREDIT_VISIBLE";
export const REMOVE_CREDIT_DATASOURCE = "REMOVE_CREDIT_DATASOURCE";
export const UPDATE_CREDIT_BUTTON_LOADING = "UPDATE_CREDIT_BUTTON_LOADING";

export const update_credits_datasource = value => {
    return {
        type: UPDATE_CREDITS_DATASOURCE,
        payload: value
    };
};

export const update_credits_loading = value => {
    return {
        type: UPDATE_CREDITS_LOADING,
        payload: value
    };
};

export const remove_credit = (dispatch, id) => {

    return () => {
        dispatch(update_credit_button_loading({
            [`removeIconLoading${id}`]: true
        }));
        let queryName = "removeCredit";
        let data = {
            data: {
                id: id
            }
        };
        PostDataService(
            BASEAPIURL,
            MakeGraphQLQuery("mutation", queryName, data, ["id", "number", "description"]), {
                success: data => {
                    if (data.data[queryName]) {
                        data = data.data[queryName]
                        dispatch(remove_credit_datasource(data));
                    }
                },
                always: () => {
                    dispatch(update_credit_button_loading({
                        [`removeIconLoading${id}`]: false
                    }));
                }
            }
        );
    }
}

export const update_credits_action = value => {
    return {
        type: UPDATE_CREDITS_ACTION,
        payload: value
    };
};

export const update_credits_edit_details = value => {
    return {
        type: UPDATE_CREDITS_EDIT_DETAILS,
        payload: value
    };
};

export const update_credit_visible = value => {
    return {
        type: UPDATE_CREDIT_VISIBLE,
        payload: value
    };
};

export const update_add_credit_visible = value => {
    return {
        type: UPDATE_ADD_CREDIT_VISIBLE,
        payload: value
    }
}

export const update_credit_button_loading = value => {
    return {
        type: UPDATE_CREDIT_BUTTON_LOADING,
        payload: value
    }
}

export const remove_credit_datasource = value => {
    return {
        type: REMOVE_CREDIT_DATASOURCE,
        payload: value
    }
}