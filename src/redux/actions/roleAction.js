import {
    PostDataService
} from "../../services/post-data-service";
import {
    MakeGraphQLQuery,
    setSessionStorageItem
} from "../../services/utils";
import {
    BASEAPIURL
} from "../../env";


export const UPDATE_ROLE_LOADING = "UPDATE_ROLE_LOADING";
export const UPDATE_ROLE_DATASOURCE = "UPDATE_ROLE_DATASOURCE";


export const fetch_roles = (dispatch) => {

    // console.log("rolesStaff");
    dispatch(update_role_loading(true));

    PostDataService(
        BASEAPIURL,
        MakeGraphQLQuery("query", "rolesStaff", [], ["name", "id"]), {
            success: data => {
                if (data.data && data.data["rolesStaff"]) {
                    data = data.data["rolesStaff"].map((data) => {
                        data['key'] = data.id;
                        return data;
                    })
                    setSessionStorageItem(
                        "roleData",
                        JSON.stringify(data)
                    );

                    dispatch(update_role_datasource(data))
                }
            },
            always: () => {
                dispatch(update_role_loading(false))
            }
        }
    );

}

export const update_role_datasource = (value) => {
    return {
        type: UPDATE_ROLE_DATASOURCE,
        payload: value
    }
}
export const update_role_loading = (value) => {
    return {
        type: UPDATE_ROLE_LOADING,
        payload: value
    }
}