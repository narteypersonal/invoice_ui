import {
    PostDataService
} from "../../services/post-data-service";
import {
    BASEAPIURL
} from "../../env";
import {
    MakeGraphQLQuery
} from "../../services/utils";

export const UPDATE_STOCK_LOADING = "UPDATE_STOCK_LOADING";
export const UPDATE_STOCK_DATASOURCE = "UPDATE_STOCK_DATASOURCE";
export const UPDATE_STOCK_DRAWER_VISIBLE = "UPDATE_STOCK_DRAWER_VISIBLE";

export const fetch_stock = dispatch => {
    return () => {
        let queryName = "stocks";
        dispatch(update_stock_loading(true));
        PostDataService(
            BASEAPIURL,
            MakeGraphQLQuery(
                "query",
                queryName, {
                orderBy: {
                    field: "updated_at",
                    order: `DESC`
                }
            }, [
                "id",
                "product{id name}",
                "quantity",
                "created_at",
                "is_void"
            ]
            ), {
            success: data => {
                if (data.data[queryName]) {
                    dispatch(
                        update_stock_datasource(
                            data.data[queryName].map(dt => {
                                dt["key"] = dt["id"];
                                return dt;
                            })
                        )
                    );
                } else {
                    dispatch(
                        update_stock_datasource([])
                    );
                }
                },
                always: () => {
                    dispatch(update_stock_loading(false));
                }
            }
        );
    }
    


};

export const update_stock_drawer_visible = value => {
    return {
        type: UPDATE_STOCK_DRAWER_VISIBLE,
        payload: value
    }
}
export const update_stock_loading = (value) => {
    return {
        type: UPDATE_STOCK_LOADING,
        payload: value
    }
}

export const update_stock_datasource = (values) => {
    
    return {
        type: UPDATE_STOCK_DATASOURCE,
        payload: values
    }
}