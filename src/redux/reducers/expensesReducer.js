import {
    UPDATE_EXPENSES_DATASOURCE,
    UPDATE_EXPENSES_LOADING,
    UPDATE_ADD_EXPENSES_VISIBLE,
    UPDATE_EXPENSES_BUTTON_LOADING,
    REMOVE_EXPENSES_DATASOURCE,
    UPDATE_EXPENSES_ACTION,
    UPDATE_EXPENSES_EDIT_DETAILS
} from "../actions/expensesActions";

const ExpensesReducer = (state = {}, {
    type,
    payload
}) => {
    switch (type) {
        case UPDATE_EXPENSES_DATASOURCE:
            return Object.assign({}, state, {
                datasource: payload
            });
        case UPDATE_EXPENSES_LOADING:
            return Object.assign({}, state, {
                loading: payload
            });

        case UPDATE_ADD_EXPENSES_VISIBLE:
            return Object.assign({}, state, {
                addExpensesVisible: payload
            });

        case UPDATE_EXPENSES_BUTTON_LOADING:
            return Object.assign({}, state, payload);

        case REMOVE_EXPENSES_DATASOURCE:
            return Object.assign({}, state, {
                datasource: state.datasource.filter(dt => {
                    if (dt.id !== payload.id) return true;
                })
            });

        case UPDATE_EXPENSES_ACTION:
            return Object.assign({}, state, {
                expensesUpdateAction: payload
            });
        case UPDATE_EXPENSES_EDIT_DETAILS:
            return Object.assign({}, state, payload)
        default:
            return state;
    }
}

export default ExpensesReducer;