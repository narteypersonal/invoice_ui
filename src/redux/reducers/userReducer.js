import {
    UPDATE_USERS_LOADING,
    UPDATE_USERS_DATASOURCE,
    UPDATE_USER_MODAL_VISIBLE,
    UPDATE_USER_ADD_ACTION,
    UPDATE_USER_EDIT_DETAILS
} from "../actions/userActions";

const userReducer = (state = {}, {
    type,
    payload
}) => {
    switch (type) {
        case UPDATE_USERS_DATASOURCE:
            return Object.assign({}, state, {
                datasource: payload
            });

        case UPDATE_USERS_LOADING:
            return Object.assign({}, state, {
                loading: payload
            });

        case UPDATE_USER_MODAL_VISIBLE:
            return Object.assign({}, state, {
                userModalVisible: payload
            });

        case UPDATE_USER_EDIT_DETAILS:
            return Object.assign({}, state, payload);

        case UPDATE_USER_ADD_ACTION:
            return Object.assign({}, state, {
                userUpdateAction: payload
            });
        default:
            return state
    }
}

export default userReducer;