import { UPDATE_STOCK_DATASOURCE, UPDATE_STOCK_LOADING, UPDATE_STOCK_DRAWER_VISIBLE } from "../actions/stockActions";

 const StockReducer = (state={}, {type, payload}) => {
    switch (type) {
        case UPDATE_STOCK_DATASOURCE:
            return {
                ...state, datasource: [...payload]
            };

        case UPDATE_STOCK_LOADING:
            return {...state, loading: payload}

        case UPDATE_STOCK_DRAWER_VISIBLE:
            return {...state, stockDrawerVisible: payload}

        default:
            return state;
    }
}

export default StockReducer;