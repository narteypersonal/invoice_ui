import {
    UPDATE_RECEIPT_DATASOURCE,
    UPDATE_LOADING,
    UPDATE_SEARCHTEXT,
    TOGGLE_RECEIPT_MODAL_VISIBLE,
    UPDATE_RECEIPT_DATA_REPLACE,
    UPDATE_RECEIPT_BUTTON_LOADING,
    UPDATE_RECEIPT_DETAIL,
    TOGGLE_ADD_RECEIPT_DRAWER_VISIBLE,
    TOGGLE_RECEIPT_FILTER_DRAWER_VISIBLE
} from "../actions/receiptActions";

const receiptReducer = (state = {}, {
    type,
    payload
}) => {
    switch (type) {
        case UPDATE_RECEIPT_DATASOURCE:
            return Object.assign({}, state, {
                datasource: Object.assign([], state.datasource, payload)
            });
        case UPDATE_RECEIPT_DETAIL:
            return Object.assign({}, state, {
                receiptDetail: payload
            });
        case UPDATE_LOADING:
            return Object.assign({}, state, {
                loading: payload
            });
        case UPDATE_SEARCHTEXT:
            return Object.assign({}, state, {
                searchText: payload
            });
        case TOGGLE_RECEIPT_MODAL_VISIBLE:
            return Object.assign({}, state, {
                receiptModalVisible: payload
            })
        case TOGGLE_ADD_RECEIPT_DRAWER_VISIBLE:
            return Object.assign({}, state, {
                addReceiptDrawerVisible: payload
            })
        case TOGGLE_RECEIPT_FILTER_DRAWER_VISIBLE:
            return Object.assign({}, state, {
                receiptFilterDrawerVisible: payload
            })
        case UPDATE_RECEIPT_BUTTON_LOADING:
            return Object.assign({}, state, payload);
        case UPDATE_RECEIPT_DATA_REPLACE:
            return Object.assign({}, state, {
                datasource: state.datasource.map(dtsrc => {
                    if (dtsrc.id === payload.id && dtsrc.num === payload.num) {
                        return payload;
                    }
                    return dtsrc;
                })
            });
        default:
            return state;
    }
};

export default receiptReducer;