import {
    UPDATE_EXPENSES_TYPE_DATASOURCE,
    UPDATE_EXPENSES_TYPE_LOADING,
    UPDATE_ADD_EXPENSES_TYPE_VISIBLE,
    UPDATE_EXPENSE_TYPE_BUTTTON_LOADING,
    UPDATE_EXPENSE_TYPE_ACTION,
    UPDATE_EXPENSE_TYPE_EDIT_DETAILS,
    REMOVE_EXPENSE_TYPE_DATASOURCE
} from "../actions/expensesTypeActions"

const ExpensesTypeReducer = (state = {}, {
    type,
    payload
}) => {
    switch (type) {
        case UPDATE_EXPENSES_TYPE_DATASOURCE:
            return Object.assign({}, state, {
                datasource: payload
            });

        case UPDATE_EXPENSES_TYPE_LOADING:
            return Object.assign({}, state, {
                loading: payload
            });

        case UPDATE_ADD_EXPENSES_TYPE_VISIBLE:
            return Object.assign({}, state, {
                addExpensesTypeVisible: payload
            });
        case UPDATE_EXPENSE_TYPE_BUTTTON_LOADING:
            return Object.assign({}, state, payload)

        case UPDATE_EXPENSE_TYPE_ACTION:
            return Object.assign({}, state, {
                expensesTypeUpdateAction: payload
            });

        case UPDATE_EXPENSE_TYPE_EDIT_DETAILS:
            return Object.assign({}, state, payload);

        case REMOVE_EXPENSE_TYPE_DATASOURCE:
            return Object.assign({}, state, {
                datasource: state.datasource.filter(dt => {
                    if (dt.id !== payload.id) return true;
                })
            });
        default:
            return state
    }
}

export default ExpensesTypeReducer;