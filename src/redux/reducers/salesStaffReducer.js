import {
    UPDATE_SALES_STAFF_DATASOURCE,
    UPDATE_SALES_STAFF_LOADING,
    UPDATE_STAFF_ACCOUNT_SALES_LOADING,
    UPDATE_STAFF_ACCOUNT_SALES_DATASOURCE,
    UPDATE_STAFF_JOURNAL_SALES_DATASOURCE,
    UPDATE_STAFF_JOURNAL_SALES_LOADING
} from "../actions/salesStaffAction";

const SalesStaffReducer = (state = {}, {
    type,
    payload
}) => {
    switch (type) {
        case UPDATE_SALES_STAFF_DATASOURCE:
            return Object.assign({}, state, {
                datasource: payload
            });

        case UPDATE_SALES_STAFF_LOADING:
            return Object.assign({}, state, {
                loading: payload
            });

        case UPDATE_STAFF_ACCOUNT_SALES_DATASOURCE:
            return Object.assign({}, state, {
                salesDatasource: payload
            });

        case UPDATE_STAFF_ACCOUNT_SALES_LOADING:
            return Object.assign({}, state, {
                salesLoading: payload
            })

        case UPDATE_STAFF_JOURNAL_SALES_DATASOURCE:
            return Object.assign({}, state, {
                journalDatasource: payload
            });
        case UPDATE_STAFF_JOURNAL_SALES_LOADING:
            return Object.assign({}, state, {
                journalLoading: payload
            });

        default:
            return state;
    }
}

export default SalesStaffReducer;