import {
    UPDATE_INVOICE_DATASOURCE,
    UPDATE_INVOICE_LOADING,
    UPDATE_INVOICE_MODAL_VISIBLE,
    UPDATE_INVOICE_BUTTON_LOADING,
    UPDATE_INVOICE_DATA_REPLACE,
    UPDATE_INVOICE_DETAIL_MODAL_VISIBLE,
    UPDATE_INVOICE_DETAIL,
    UPDATE_INVOICE_PREFIX,
    UPDATE_INVOICE_VIA_RECEIPT,
    TOGGLE_INVOICE_DRAWER_VISIBLE,
    UPDATE_INVOICE_RECEIPTS
} from "../actions/invoiceActions";

const invoiceReducer = (state = {}, {
    type,
    payload
}) => {
    switch (type) {
        case UPDATE_INVOICE_DATASOURCE:
            return Object.assign({}, state, {
                datasource: payload
            });
        case UPDATE_INVOICE_VIA_RECEIPT:

            return Object.assign({}, state, {
                datasource: state.datasource.map((data) => {

                    if (data.num === payload.invoice.num) {

                        data.balance = payload.invoice.balance;
                        data.amount_recieved = payload.invoice.amount_recieved;
                        return data;
                    }

                    return data;
                })
            });

        case UPDATE_INVOICE_LOADING:
            return Object.assign({}, state, {
                loading: payload
            });

        case UPDATE_INVOICE_MODAL_VISIBLE:
            return Object.assign({}, state, {
                invoiceModalVisible: payload
            });

        case UPDATE_INVOICE_BUTTON_LOADING:
            return Object.assign({}, state, payload);

        case UPDATE_INVOICE_DATA_REPLACE:
            return Object.assign({}, state, {
                datasource: state.datasource.map(dtsrc => {
                    if (dtsrc.id === payload.id && dtsrc.num === payload.num) {
                        return payload;
                    }
                    return dtsrc;
                })
            });

        case UPDATE_INVOICE_DETAIL_MODAL_VISIBLE:
            return Object.assign({}, state, {
                invoiceDetailModalVisible: payload
            });

        case UPDATE_INVOICE_DETAIL:
            return Object.assign({}, state, {
                invoiceDetailDataSource: payload
            });

        case UPDATE_INVOICE_PREFIX:
            return Object.assign({}, state, {
                prefix: payload
            });

        case TOGGLE_INVOICE_DRAWER_VISIBLE:
            return Object.assign({}, state, {
                invoiceDrawerVisible: payload
            });

        case UPDATE_INVOICE_RECEIPTS:
            return Object.assign({}, state, {
                invoiceReceipts: payload
            })

        default:
            return state;
    }
}

export default invoiceReducer;