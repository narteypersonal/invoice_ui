import {
    UPDATE_PURCHASE_INVOICE_LOADING,
    UPDATE_PURCHASE_INVOICE_DATASOURCE,
    TOGGLE_PURCHASE_INVOICE_DRAWER_VISIBLE,
    UPDATE_PURCHASE_INVOICE_TOTAL_COST,
    UPDATE_PURCHASE_INVOICE_TOTAL_ITEMS,
    UPDATE_PURCHASE_INVOICE_TOTAL_QUANTITY
} from "../actions/purchaseActions";
const PurchaseReducer = (state = {}, {
    type,
    payload
}) => {
    switch (type) {
        case UPDATE_PURCHASE_INVOICE_LOADING:

            return {
                ...state,
                loading: payload
            };

        case UPDATE_PURCHASE_INVOICE_DATASOURCE:
            return {
                ...state, datasource: payload
            };

        case TOGGLE_PURCHASE_INVOICE_DRAWER_VISIBLE:
            return {
                ...state,
                purchasesDrawerVisible: payload
            };
        case UPDATE_PURCHASE_INVOICE_TOTAL_COST:
            return{
                ...state,
                totalCost: payload
            };
        case UPDATE_PURCHASE_INVOICE_TOTAL_ITEMS:
            return {
                ...state,
                totalItems: payload
            };
        case UPDATE_PURCHASE_INVOICE_TOTAL_QUANTITY:
            return {
                ...state,
                totalQuantity: payload
            };

        default:
            return state;

    }
}

export default PurchaseReducer;