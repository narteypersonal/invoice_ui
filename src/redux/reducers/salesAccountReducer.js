import {
    UPDATE_SALES_ACCOUNT_DATASOURCE,
    UPDATE_SALES_ACCOUNT_LOADING,
    UPDATE_SALES_ACCOUNT_DRAWER_FILTER_VISIBLE
} from "../actions/salesAccountAction";

const SalesAccountReducer = (state = {}, {
    type,
    payload
}) => {
    switch (type) {
        case UPDATE_SALES_ACCOUNT_DATASOURCE:
            return Object.assign({}, state, {
                datasource: payload
            });

        case UPDATE_SALES_ACCOUNT_LOADING:
            return Object.assign({}, state, {
                loading: payload
            });
        case UPDATE_SALES_ACCOUNT_DRAWER_FILTER_VISIBLE:
            return Object.assign({}, state, {
                salesAccountFilterDrawerVisible: payload
            });

        default:
            return state;
    }

}

export default SalesAccountReducer;