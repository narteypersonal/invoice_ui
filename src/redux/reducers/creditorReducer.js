import {
    UPDATE_CREDITOR_DATASOURCE,
    UPDATE_CREDITORS_LOADING,
    UPDATE_ADD_CREDITOR_VISIBLE,
    UPDATE_CREDITOR_ACTION,
    UPDATE_CREDITOR_BUTTON_LOADING,
    UPDATE_CREDITOR_EDIT_DETAILS,
    REMOVE_CREDITOR_DATASOURCE
} from "../actions/creditorActions";


const CreditorReducer = (state = {}, {
    type,
    payload
}) => {
    switch (type) {
        case UPDATE_CREDITOR_DATASOURCE:
            return Object.assign({}, state, {
                datasource: payload
            });

        case UPDATE_CREDITORS_LOADING:
            return Object.assign({}, state, {
                loading: payload
            });

        case UPDATE_ADD_CREDITOR_VISIBLE:
            return Object.assign({}, state, {
                addCreditorVisible: payload
            });

        case UPDATE_CREDITOR_ACTION:
            return Object.assign({}, state, {
                creditorUpdateAction: payload
            });

        case REMOVE_CREDITOR_DATASOURCE:
            return Object.assign({}, state, {
                datasource: state.datasource.filter(dt => {
                    if (dt.id !== payload.id) return true;
                })
            });

        case UPDATE_CREDITOR_EDIT_DETAILS:
            return Object.assign({}, state, payload);

        case UPDATE_CREDITOR_BUTTON_LOADING:
            return Object.assign({}, state, payload);

        default:
            return state;
    }
}

export default CreditorReducer;