import {
    UPDATE_PRODUCT_DATASOURCE,
    UPDATE_PRODUCT_LOADING,
    UPDATE_PRODUCT_MODAL_VISIBLE,
    UPDATE_PRODUCT_ACTION,
    UPDATE_PRODUCT_MODAL_TITLE,
    UPDATE_PRODUCT_EDIT_DETAILS,
    UPDATE_PRODUCT_BUTTTON_LOADING,
    REMOVE_PRODUCT_DATASOURCE,
    UPDATE_PRODUCT_SALES_DATASOURCE
} from "../actions/productActions";

const productReducer = (state = {}, {
    type,
    payload
}) => {
    switch (type) {
        case UPDATE_PRODUCT_SALES_DATASOURCE:
            return Object.assign({}, state, {
                productSales: payload
            });
        case UPDATE_PRODUCT_DATASOURCE:
            return Object.assign({}, state, {
                datasource: payload
            });

        case UPDATE_PRODUCT_LOADING:
            return Object.assign({}, state, {
                loading: payload
            });

        case UPDATE_PRODUCT_MODAL_VISIBLE:
            return Object.assign({}, state, {
                productModalVisible: payload
            });

        case UPDATE_PRODUCT_ACTION:
            return Object.assign({}, state, {
                productUpdateAction: payload
            });

        case UPDATE_PRODUCT_MODAL_TITLE:
            return Object.assign({}, state, {
                productModalTitle: payload
            });

        case UPDATE_PRODUCT_EDIT_DETAILS:
            return Object.assign({}, state, payload);

        case UPDATE_PRODUCT_BUTTTON_LOADING:
            return Object.assign({}, state, payload);

        case REMOVE_PRODUCT_DATASOURCE:
            return Object.assign({}, state, {
                datasource: state.datasource.filter(dt => {
                    if (dt.id !== payload.id) return true;
                })
            });

        default:
            return state;
    }
}

export default productReducer;