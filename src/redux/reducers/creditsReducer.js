import {
    UPDATE_CREDITS_DATASOURCE,
    UPDATE_CREDITS_ACTION,
    UPDATE_CREDITS_EDIT_DETAILS,
    UPDATE_CREDITS_LOADING,
    UPDATE_ADD_CREDIT_VISIBLE,
    UPDATE_CREDIT_BUTTON_LOADING,
    REMOVE_CREDIT_DATASOURCE
} from "../actions/creditsActions";

const CreditsReducer = (state = {}, {
    type,
    payload
}) => {
    switch (type) {
        case UPDATE_CREDITS_DATASOURCE:
            return Object.assign({}, state, {
                datasource: payload
            });

        case UPDATE_CREDITS_ACTION:
            return Object.assign({}, state, {
                updateCreditAction: payload
            });

        case UPDATE_CREDITS_EDIT_DETAILS:
            return Object.assign({}, state,
                payload
            );

        case UPDATE_CREDITS_LOADING:
            return Object.assign({}, state, {
                loading: payload
            });
        case UPDATE_ADD_CREDIT_VISIBLE:
            return Object.assign({}, state, {
                addCreditVisible: payload
            });
        case UPDATE_CREDIT_BUTTON_LOADING:
            return Object.assign({}, state, payload);

        case REMOVE_CREDIT_DATASOURCE:
            return Object.assign({}, state, {
                datasource: state.datasource.filter(dt => {
                    if (dt.id !== payload.id) return true;
                })
            })
        default:
            return state;
    }
}

export default CreditsReducer;