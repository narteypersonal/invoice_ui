import {
    APP_LOGIN,
    UPDATE_APP_LOGIN
} from "../actions/appAction";

const appReducer = (state = {}, {
    type,
    payload
}) => {
    switch (type) {
        case APP_LOGIN:
            return Object.assign({}, state, {
                login: !state.login
            });
        case UPDATE_APP_LOGIN:
            return Object.assign({}, state, {
                login: payload
            });

        default:
            return state;
    }
}


export default appReducer;