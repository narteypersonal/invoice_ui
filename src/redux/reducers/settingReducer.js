import {
    UPDATE_SETTINGS_LOADING,
    UPDATE_SETTINGS_DATASOURCE
} from "../actions/settingActions";

const settingsReducer = (state = {}, {
    type,
    payload
}) => {
    switch (type) {
        case UPDATE_SETTINGS_LOADING:
            return Object.assign({}, state, {
                loading: payload
            });

        case UPDATE_SETTINGS_DATASOURCE:
            return Object.assign({}, state, {
                details: payload
            });

        default:
            return state;
    }
};

export default settingsReducer;