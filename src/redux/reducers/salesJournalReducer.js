import {
    UPDATE_SALES_JOURNAL_DATASOURCE,
    UPDATE_SALES_JOURNAL_LOADING,
    UPDATE_SALES_JOURNAL_DRAWER_FILTER_VISIBLE
} from "../actions/salesJournalAction";

const SalesJournalReducer = (state = {}, {
    type,
    payload
}) => {
    switch (type) {
        case UPDATE_SALES_JOURNAL_DATASOURCE:
            return Object.assign({}, state, {
                datasource: payload
            });

        case UPDATE_SALES_JOURNAL_LOADING:
            return Object.assign({}, state, {
                loading: payload
            });
        case UPDATE_SALES_JOURNAL_DRAWER_FILTER_VISIBLE:
            return Object.assign({}, state, {
                salesJournalFilterDrawerVisible: payload
            });

        default:
            return state;
    }

}

export default SalesJournalReducer;