import {
    UPDATE_PROFILE,
    UPDATE_PROFILE_LOADING,
    UPDATE_PROFILE_PASSWORD_MISMATCH,
    UPDATE_PROFILE_PASSWORD_RESETABLE
} from "../actions/profileActions";

const profileReducer = (state = {}, {
    type,
    payload
}) => {
    switch (type) {

        case UPDATE_PROFILE:
            return Object.assign({}, state, payload);

        case UPDATE_PROFILE_LOADING:
            return Object.assign({}, state, {
                loading: payload
            });

        case UPDATE_PROFILE_PASSWORD_MISMATCH:
            return Object.assign({}, state, {
                passwordMismatch: payload
            });
        case UPDATE_PROFILE_PASSWORD_RESETABLE:
            return Object.assign({}, state, {
                passwordResetable: payload
            });

        default:
            return state;
    }
}

export default profileReducer;