import {
    UPDATE_RECEIPTTYPE_DATA,
    UPDATE_RECEIPTTYPE_LOADING
} from "../actions/receiptTypeActions";

const receiptTypeReducer = (state = {}, {
    type,
    payload
}) => {
    switch (type) {
        case UPDATE_RECEIPTTYPE_DATA:
            return Object.assign({}, state, {
                datasource: payload
            });
        case UPDATE_RECEIPTTYPE_LOADING:
            return Object.assign({}, state, {
                loading: payload
            });

        default:
            return state;
    }
};

export default receiptTypeReducer;