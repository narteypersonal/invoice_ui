import {
    UPDATE_ROLE_LOADING,
    UPDATE_ROLE_DATASOURCE
} from "../actions/roleAction";


const roleReducer = (state = {}, {
    type,
    payload
}) => {
    switch (type) {
        case UPDATE_ROLE_DATASOURCE:
            return Object.assign({}, state, {
                datasource: payload
            });
        case UPDATE_ROLE_LOADING:
            return Object.assign({}, state, {
                loading: payload
            });

        default:
            return state;
    }
}

export default roleReducer;