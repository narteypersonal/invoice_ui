import {
    UPDATE_DEBTOR_JOURNAL_DATASOURCE,
    UPDATE_DEBTOR_JOURNAL_LOADING,
    UPDATE_DEBTOR_DRAWER_VISIBLE,
    UPDATE_DEBTOR_DETAIL,
    UPDATE_DEBTOR_BUTTON_LOADING
} from "../actions/debtorsJournalActions";

const DebtorsJournalReducer = (state = {}, {
    type,
    payload
}) => {
    switch (type) {
        case UPDATE_DEBTOR_JOURNAL_DATASOURCE:
            return Object.assign({}, state, {
                datasource: payload
            });
        case UPDATE_DEBTOR_JOURNAL_LOADING:
            return Object.assign({}, state, {
                loading: payload
            });

        case UPDATE_DEBTOR_DRAWER_VISIBLE:
            return Object.assign({}, state, {
                debtorDrawerVisible: payload
            });

        case UPDATE_DEBTOR_BUTTON_LOADING:
            return Object.assign({}, state, payload);

        case UPDATE_DEBTOR_DETAIL:
            return Object.assign({}, state, {
                debtorDetailDataSource: payload
            });

        default:
            return state;
    }
}

export default DebtorsJournalReducer;