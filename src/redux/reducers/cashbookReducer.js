import {
    UPDATE_CASHBOOK_DATASOURCE,
    UPDATE_CASHBOOK_LOADING
} from "../actions/cashbookActions";

const CashbookReducer = (state = {}, {
    type,
    payload
}) => {
    switch (type) {
        case UPDATE_CASHBOOK_DATASOURCE:
            return Object.assign({}, state, {
                datasource: payload
            });

        case UPDATE_CASHBOOK_LOADING:
            return Object.assign({}, state, {
                loading: payload
            });

        default:
            return state;
    }
}

export default CashbookReducer;