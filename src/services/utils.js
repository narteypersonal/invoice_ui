export const MakeGraphQLQuery = (
  queryType,
  queryName,
  queryVariables = [],
  queryData = []
) => {
  let variables = "";
  let data = "";
  for (let key in queryVariables) {
    if (typeof key === "string") {
      variables += `${key} : ${deepParseJSON(queryVariables[key])} `;
    } else {
      variables += `${queryVariables[key]} `;
    }
  }

  for (let dt of queryData) {
    data += `${dt} `;
  }

  return {
    query: `${queryType}{${queryName}${variables ? "(" + variables + ")" : ""}${
      data ? "{" + data + "}" : ""
    }}`
  };
};

export const setSessionStorageItem = (key, value, callback) => {
  let promise = new Promise((resolve, reject) => {
    resolve(sessionStorage.setItem(key, value));
  });

  promise.then(callback);
};

const deepParseJSON = data => {
  let variables = "";
  let obj = false;
  for (let key in data) {
    if (Number.parseInt(key) >= 0) {
      if (typeof data[key] === "object")
        variables += `${deepParseJSON(data[key])} `;
      else if (key !== "order") variables += `${JSON.stringify(data[key])} `;
    } else if (typeof key === "string") {
      obj = true;
      if (typeof data[key] === "object")
        variables += `${key} : ${deepParseJSON(data[key])} `;
      else if (key !== "order")
        variables += `${key} : ${JSON.stringify(data[key])} `;
      else variables += `${key} : ${data[key]} `;
    } else {
      variables += `${data[key]} `;
    }
  }

  return obj ? `{${variables}}` : `[${variables}]`;
};