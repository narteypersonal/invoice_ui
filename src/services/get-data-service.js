import {
    HttpResponseHandler
} from "./http-response-handler";
export const GetDataService = (
    url,
    data,
    config = {
        headers: {},
        success: data => {
            console.log(data);
        },
        error: err => {
            console.log(err);
        },
        res: response => {
            return HttpResponseHandler(response);
        }
    }
) => {
    return fetch(url, {
            mode: "cors",
            headers: config.headers
        })
        .then(config.res)
        .then(config.success)
        .catch(config.error);
};