import {
  message
} from "antd";

const {
  warning,
  error
} = message;
export const HttpResponseHandler = response => {
  try {
    switch (response.status) {
      case 200:
      case 201:
        return response.json();
      case 401:
        warning("Access denied. Unauthorized");
        return null;
      case 404:
        warning("Server not found");
        return null;
      case 500:
        warning("Server side error. Please contact server master");
        return null;
      case 503:
        warning("Service not available");
        return null;
      case 504:
        warning("Service failed to responed");
        return null;
      case 599:
        warning("Network failure");
        return null;
      default:
        return `Unhandled HTTP response ${response.status}`;
    }
  } catch (ex) {
    error("An unexpected error occurred");

  }
};