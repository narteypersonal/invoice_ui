import {
  HttpResponseHandler
} from "./http-response-handler";
import {
  message
} from "antd";
export const PostDataService = (
  url,
  data,
  config
) => {
  const headers = {
    ...{
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': (sessionStorage.getItem("userData")) ?
        `Bearer ${
                JSON.parse(sessionStorage.getItem("userData"))["access_token"]
              }` : ""
    },
    ...config.headers
  }

  let defaults = Object.assign({}, {
    success: data => {
      console.log(data);
    },
    always: () => {

    },
    error: err => {
      console.log(err);
      message.error("Failed to contact server. Please check your internet connection and try again.")
    },
    res: response => {
      try {
        defaults.always();
      } catch (ex) {
        console.log(`Error in always(): ${ex}`)
      }
      return HttpResponseHandler(response);
    }
  }, config);

  data = JSON.stringify(data);
  // console.log(data);

  return fetch(url, {
      method: "post",
      body: data,
      mode: "cors",
      headers: headers
    })
    .then(defaults.res)
    .then(defaults.success)
    .catch(defaults.error);
};

