import React from "react";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import AppContainer from "./components/app-container.js";
import "./App.css";
import Login from "./components/login/login";
import { ForgottenPassword } from "./components/app-forgotten-password.js";
import { connect } from "react-redux";
function App(props) {
  return (
    <Router>
      <div className="App">
        {/* <Route  path="/" component={Login} />{" "} */}
        <Route path="/login" component={Login} />{" "}
        <Route path="/forgotten-password" component={ForgottenPassword} />{" "}
        <Route path="/app" component={AppContainer} />{" "}
        <Route exact={true} path="/" component={AppContainer} />{" "}
      </div>{" "}
    </Router>
  );
}

const mapStateToProps = state => {
  return {
    app: state.app
  };
};
export default connect(
  mapStateToProps,
  null
)(App);
