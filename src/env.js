export const BASEAPIURL =
    "/api/graphql";
export const API_URL =
    "/api/graphql";
// export const BASEAPIURL =
//     "http://retail.patterncraftgroup.com/graphql";
// export const API_URL =
//     "http://retail.patterncraftgroup.com/";

// These are route urls used by react-router-dom
export const LOGINROUTE = `/login`;
export const DASHBOARDROUTE = `/dashboard`;
export const PROFILEROUTE = `/profile`;
export const INVOICEROUTE = `/invoices`;
export const PRODUCTROUTE = `/products`;
export const USERROUTE = `/users`;