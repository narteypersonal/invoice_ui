import React from "react";
import { Chart } from '@antv/g2';
import { fetch_product_sales } from "../../redux/actions/productActions";
import {connect} from "react-redux";

let chart;
class AnalysisChart extends React.Component{

    state = { 
        data : [
            { name: "Binaton iron ", quantity: 5, sale: 250 },
            { name: "Milk", quantity: 2, sale: 6 }
        ],
        chartOptions: {
            width: 600,
            height: 300,
        },
        
    };

    componentDidMount(){
        this.props.fetch_product_sales({});
        //Step 1
        
        // Step 3: Declare the grammar of graphics, draw column chart.
        this.setState({
            chart: new Chart({
                container: 'c1', // Specify chart container ID
                width: 600,
                height: 300,
            })})
        
    }
    render(){
       if(this.state.chart){
           this.state.chart.interval().position('name*quantity').color('name');
           this.state.chart.line().position('name*sale').color("gold")

           this.state.chart.data(this.props.product.productSales.info);
           // Step 4: Render chart.
           this.state.chart.render();
           console.log(this.props.product.productSales.info)
        }

        
        return(
            <div id="c1"></div>
        )
    }
}

const mapPropsToState = state => {
    return {
        product: state.product
    }
}
const mapPropsToDispatch = dispatch => {
    return {
        fetch_product_sales : (values) => dispatch(fetch_product_sales(dispatch, values))
    }
}

// Step 1: Create a Chart instance.



export default connect(mapPropsToState, mapPropsToDispatch)(AnalysisChart);