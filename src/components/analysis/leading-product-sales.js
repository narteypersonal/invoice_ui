import React from "react";
import {
    Button,
    Tabs,
    Card,
    DatePicker,
    Row,
    Col,
    Input,
    List,
    Badge,
    Radio

} from 'antd';
import moment from 'moment';
import ProductAnalysisChart from "./product-analysis-chart";
import { connect } from "react-redux";
import { fetch_product_sales } from "../../redux/actions/productActions";

const { TabPane } = Tabs;
const {RangePicker} = DatePicker;
const { Search } = Input;
const DATE_FORMAT = 'YYYY-MM-DD';


class LeadingProductSalesAnalysis extends React.Component{
    state={
        productChartProposition: 'name*sale',
        dates:[]
    }

    handleAllDay = () => {
        let from = moment().startOf('day');
        let to = moment().endOf('day');
        this.setState({
            dates: [from,
            to]
        });
        this.fetch_product_sale_records({ from: from.format(DATE_FORMAT), to: to.add(1,'day').format(DATE_FORMAT) })
    }

    fetch_product_sale_records = ({from, to}) => {
        if(!from || !to)
            return;
        this.props.fetch_product_sales({from, to})
    }

    handleAllWeek = () => {
        let from = moment().startOf('week');
        let to = moment().endOf('week');
        this.setState({ dates: [from ,
        to ]})
        this.fetch_product_sale_records({ from: from.format(DATE_FORMAT), to: to.add(1, 'day').format(DATE_FORMAT)})
    }
    handleAllMonth = () => {
        let from = moment().startOf('month');
        let to = moment().endOf('month');
        this.setState({
            dates: [from,
                to]
        })
        this.fetch_product_sale_records({ from: from.format(DATE_FORMAT), to: to.add(1, 'day').format(DATE_FORMAT) })
    }
    handleAllYear = () => {
        let from = moment().startOf('year');
        let to = moment().endOf('year');
        this.setState({
            dates: [from,
                to]
        })
        this.fetch_product_sale_records({ from: from.format(DATE_FORMAT), to: to.add(1, 'day').format(DATE_FORMAT) })
    }
    render(){
        const operations = () => (<>
            <Button onClick={this.handleAllDay}>All Day</Button>
            <Button onClick={this.handleAllWeek}>All Week</Button>
            <Button onClick={this.handleAllMonth}>All Month</Button>
            <Button onClick={this.handleAllYear}>All Year</Button>
            <div><RangePicker picker="week" value={this.state.dates} onChange={(val) => console.log(val)} /></div>
        </>);
        return (
            <Card style={{textAlign: 'left'}}>
                <Tabs tabBarExtraContent={operations(this.props)}>
                    <TabPane tab="Products" key="1">
                        <Row >
                            <Col xs={24} sm={24} md={18}>
                                <span>Product Trend  </span>
                                <Radio.Group defaultValue="name*sale" buttonStyle="solid"
                                 onChange={(e)=> this.setState({productChartProposition: e.target.value})}>
                                    <Radio.Button value="name*quantity">Quantity</Radio.Button>
                                    <Radio.Button value="name*sale" >Sales Volume</Radio.Button>
                                </Radio.Group>
                                <ProductAnalysisChart chartProposition={this.state.productChartProposition}/>
                            </Col>
                            <Col xs={24} sm={24} md={6} style={styles.maxHeight}>
                                <h3>Top Product Sales</h3>
                                
                                <List
                                    header={<Search placeholder="Search Product" />}
                                    bordered
                                    dataSource={this.props.product.productSales.info}
                                    renderItem={item => (
                                        <List.Item >
                                            <Badge count={item.quantity}/> {item.name}
                                        </List.Item>
                                    )}
                                />
                            </Col>
                        </Row>
                    </TabPane>
                    <TabPane tab="Sales" key="2">
                        <Row>
                            <Col xs={24} sm={10} md={18}>
                                <h3>Sales Trend</h3>

                            </Col>
                            <Col xs={24} sm={10} md={6}>
                                <h3>Top Product Installments</h3>
                                <Search placeholder="Search Product" />
                            </Col>
                        </Row>
                    </TabPane>
                </Tabs>
                
            </Card>
            
        )
    }
}

const styles={
    maxHeight:{ height: "50vh"}
}

const mapPropsToState = state => {
    return {
        product: state.product
    }
}

const mapPropsToDispatch = dispatch => {
    return {
        fetch_product_sales: (values) => dispatch(fetch_product_sales(dispatch, values))
    }
}


export default connect(mapPropsToState, mapPropsToDispatch) (LeadingProductSalesAnalysis);