import React, { Component } from "react";
import {
  Table,
  Tag,
  Divider,
  Button,
  Card,
  Row,
  Col,
  message,
  Input,
  Icon
} from "antd";
import { PostDataService } from "../services/post-data-service";
import { BASEAPIURL } from "../env";
import { MakeGraphQLQuery } from "../services/utils";
import CollectionCreateInvoiceForm from "./app-invoice-modal";
import { AppInvoiceDetailModal } from "./app-invoice-detail-modal";
import {
  update_invoice_modal_visible,
  show_invoice_detail,
  update_invoice_validity,
  fetch_invoices,
  toggle_invoice_drawer_visible
} from "../redux/actions/invoiceActions";
import Highlighter from "react-highlight-words";
import { connect } from "react-redux";
import {
  toggle_add_receipt_drawer_visible,
  update_receipt_detail
} from "../redux/actions/receiptActions";
class InvoicesTable extends Component {
  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div
        style={{
          padding: 8
        }}
      >
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{
            width: 188,
            marginBottom: 8,
            display: "block"
          }}
        />{" "}
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{
            width: 90,
            marginRight: 8
          }}
        >
          Search{" "}
        </Button>{" "}
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{
            width: 90
          }}
        >
          Reset{" "}
        </Button>{" "}
      </div>
    ),
    filterIcon: filtered => (
      <Icon
        type="search"
        style={{
          color: filtered ? "#1890ff" : undefined
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{
          backgroundColor: "#ffc069",
          padding: 0
        }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
    )
  });

  state = {
    searchText: "",
    loading: true,
    columns: [
      {
        title: "Number",
        dataIndex: "num",
        key: "num",
        ...this.getColumnSearchProps("num"),
        render: (text, record) => (
          <div>
            {" "}
            {record.is_void ? (
              <Tag color="magenta"> {record.num} </Tag>
            ) : (
              <Tag color="green"> {record.num} </Tag>
            )}{" "}
          </div>
        )
      },
      {
        title: "Customer",
        dataIndex: "customer_name",
        key: "customer_name",
        width: "20%",
        ...this.getColumnSearchProps("customer_name")
      },

      {
        title: "Total",
        dataIndex: "net_total",
        key: "net_total",
        render: (text, record) => <span> {record.net_total.toFixed(2)} </span>
      },
      {
        title: "Recieved",
        dataIndex: "amount_recieved",
        key: "amount_recieved",
        render: (text, record) => (
          <span> {record.amount_recieved.toFixed(2)} </span>
        )
      },
      {
        title: "Balance",
        dataIndex: "balance",
        key: "balance",
        render: (text, record) => (
          <div>
            {" "}
            {record.balance > 0 ? (
              <b
                style={{
                  color: "#f50"
                }}
              >
                {" "}
                {`(${record.balance.toFixed(2)})`}{" "}
              </b>
            ) : (
              <span> {record.balance.toFixed(2)} </span>
            )}{" "}
          </div>
        )
      },
      {
        title: "Validity",
        dataIndex: "is_void",
        key: "is_void",
        render: (text, record) => (
          <div>
            {" "}
            {record.is_void ? (
              <Tag color="magenta"> Invalidated </Tag>
            ) : (
              <Tag color="green"> Valid </Tag>
            )}{" "}
          </div>
        )
      },
      {
        title: "Issued",
        dataIndex: "created_at",
        key: "created_at",
        render: (text, record) =>
          (new Date(record.created_at)).toLocaleDateString()
      },

      {
        title: "Action",
        dataIndex: "action",
        key: "action",
        render: (text, record) => (
          <span>
            <Button
              shape="circle"
              size="small"
              type="primary"
              icon="info-circle"
              loading={
                this.props.invoice[`invoiceDetailButton${record.id}IsLoading`]
              }
              onClick={this.props.show_invoice_detail.bind(this, record)}
            />{" "}
            <Divider type="vertical" />{" "}
            {/* <Button
              shape="circle"
              size="small"
              icon="copy"
              loading={this.state[`invoiceCopyButton${record.id}IsLoading`]}
              onClick={this.handleInvoiceCopy.bind(this, record)}
            />{" "} */}
            {/* <Divider type="vertical" />{" "} */}
            {record.is_void || record.balance < 1 ? (
              ""
            ) : (
              <span>
                <Button
                  shape="circle"
                  size="small"
                  icon="file-done"
                  onClick={() => {
                    this.props.update_receipt_detail(record);
                    this.props.toggle_add_receipt_drawer_visible(true);
                  }}
                />
                <Divider type="vertical" />{" "}
              </span>
            )}
            {record.is_void ? (
              <Button
                style={{
                  marginLeft: "2vw"
                }}
                shape="circle"
                size="small"
                icon="check-square"
                type="success"
                loading={
                  this.props.invoice[
                    `invoiceValidityButton${record.id}IsLoading`
                  ]
                }
                onClick={this.props.update_invoice_validity.bind(this, record)}
              />
            ) : (
              <Button
                style={{
                  marginLeft: "2vw"
                }}
                shape="circle"
                size="small"
                icon="close-square"
                type="danger"
                loading={
                  this.props.invoice[
                    `invoiceValidityButton${record.id}IsLoading`
                  ]
                }
                onClick={this.props.update_invoice_validity.bind(this, record)}
              />
            )}{" "}
          </span>
        )
      }
    ]
  };

  success = data => {
    data = {
      ...data,
      key: data.id
    };

    this.setState({
      datasource: [data, ...this.state.datasource]
    });
    message.success(`Invoice Generated`);
  };

  handleInvoiceCopy = record => {
    this.setState({
      [`invoiceCopyButton${record.id}IsLoading`]: true
    });
    let data = {
      id: record.id
    };
    data = {
      data
    };
    PostDataService(
      BASEAPIURL,
      MakeGraphQLQuery("query", "invoice", data, [
        "id num net_total gross_total is_void amount_recieved discount_total tax_total customer_name customer_phone customer_email created_at balance listProducts{product{ id name},   quantity   sub_total   tax_rate   discount_rate }"
      ]),
      {
        success: data => {
          if (data.data["invoice"]) {
            data = data.data["invoice"];
            this.setState({
              customerName: data["customer_name"],
              customerEmail: data["customer_email"],
              customerPhone: data["customer_phone"],
              products: [...data["listProducts"]]
            });
            this.props.update_invoice_modal_visible(true);
          }
        },
        always: () => {
          this.setState({
            [`invoiceCopyButton${record.id}IsLoading`]: false
          });
        }
      }
    );
  };

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0]
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({
      searchText: ""
    });
  };

  componentWillMount() {
    if (
      !this.props.invoice.datasource ||
      (!this.props.invoice.loading && this.props.invoice.datasource.length < 1)
    ) {
      this.props.fetch_invoices();
    }
  }

  render() {
    const { invoice } = this.props;
    return (
      <Card>
        <Table
          bordered
          size="small"
          loading={invoice.loading}
          title={() => (
            <Row>
              <Col span={16}>
                <h1> Invoice Listing </h1>{" "}
              </Col>{" "}
              <Col span={8}>
                <Button
                  type="dashed"
                  onClick={() => this.props.update_invoice_modal_visible(true)}
                >
                  Create Invoice{" "}
                </Button>
                <Button
                  style={{ marginLeft: "6px" }}
                  onClick={this.props.fetch_invoices}
                  type="primary"
                  icon="reload"
                />
              </Col>{" "}
            </Row>
          )}
          columns={this.state.columns}
          dataSource={invoice.datasource}
        />{" "}
      </Card>
    );
  }
}

const mapStateToProps = state => {
  return {
    invoice: state.invoice
  };
};
const mapDispatchToProps = dispatch => {
  return {
    update_invoice_modal_visible: value =>
      dispatch(update_invoice_modal_visible(value)),
    show_invoice_detail: value =>
      dispatch(show_invoice_detail(dispatch, value)),
    update_invoice_validity: value =>
      dispatch(update_invoice_validity(dispatch, value)),
    fetch_invoices: value => dispatch(fetch_invoices),
    toggle_add_receipt_drawer_visible: value =>
      dispatch(toggle_add_receipt_drawer_visible(value)),
    toggle_invoice_drawer_visible: value =>
      dispatch(toggle_invoice_drawer_visible(value)),
    update_receipt_detail: value => dispatch(update_receipt_detail(value))
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(InvoicesTable);
