import React, { useEffect } from "react";
import { Row, Col, Card } from "antd";
import { connect } from "react-redux";
import AppCreditorsSelectableMenu from "./creditors/app-creditors-selectable-menu";
import {
  fetch_creditors,
  fetch_creditor_credits
} from "../redux/actions/creditorActions";
import AppCreditsTable from "./creditors/app-credits-table";
import {
  update_credits_datasource,
  update_credits_action,
  update_credits_edit_details,
  update_add_credit_visible
} from "../redux/actions/creditsActions";

const AppCredits = props => {
  useEffect(() => {
    if (!props.creditors.loading && !props.creditors.datasource)
      props.fetch_creditors();
  });
  return (
    <Row gutter={8}>
      <Col
        span={4}
        style={{ textAlign: "Left", height: "75vh", backgroundColor: "white" }}
      >
        <AppCreditorsSelectableMenu
          onClick={creditor => {
            props.fetch_creditor_credits(
              creditor,
              props.update_credits_datasource
            );
          }}
        />
      </Col>
      <Col span={20}>
        <Card>
          <AppCreditsTable />
        </Card>
      </Col>
    </Row>
  );
};

const mapStateToProps = state => {
  return {
    creditors: state.creditors,
    credits: state.credits
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetch_creditors: () => dispatch(fetch_creditors),
    fetch_creditor_credits: (creditorName, callback) =>
      dispatch(fetch_creditor_credits(dispatch, creditorName, callback)),
    update_credits_datasource: value =>
      dispatch(update_credits_datasource(value)),
    update_credit_action: value => dispatch(update_credits_action(value)),
    update_credit_edit_detail: value =>
      dispatch(update_credits_edit_details(value)),
    update_add_credit_visible: value =>
      dispatch(update_add_credit_visible(value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppCredits);
