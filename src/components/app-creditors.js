import React from "react";
import { Card } from "antd";
import { connect } from "react-redux";
import AppCreditorsTable from "./creditors/app-creditors-table";

const AppCreditors = props => {
  return (
    <Card title="Suppliers">
      <AppCreditorsTable />
    </Card>
  );
};

export default connect()(AppCreditors);
