import React from "react";
import { Card, Table, Row, Col, Button, Tag, Divider, Icon, Input } from "antd";
import { connect } from "react-redux";
import Highlighter from "react-highlight-words";
import { fetch_purchase_invoices, update_purchase_invoice_drawer_visible } from "../redux/actions/purchaseActions";
import Text from "antd/lib/typography/Text";

class AppPurcharseInvoice extends React.Component {
  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div
        style={{
          padding: 8
        }}
      >
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{
            width: 188,
            marginBottom: 8,
            display: "block"
          }}
        />{" "}
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{
            width: 90,
            marginRight: 8
          }}
        >
          Search{" "}
        </Button>{" "}
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{
            width: 90
          }}
        >
          Reset{" "}
        </Button>{" "}
      </div>
    ),
    filterIcon: filtered => (
      <Icon
        type="search"
        style={{
          color: filtered ? "#1890ff" : undefined
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{
          backgroundColor: "#ffc069",
          padding: 0
        }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
    )
  });

  state = {
    searchText: "",
    loading: true,
    columns: [
      {
        title: "Number",
        dataIndex: "number",
        key: "number",
        ...this.getColumnSearchProps("number"),
        render: (text, record) => (
          <div>
            {" "}
            {record.is_void ? (
              <Tag color="magenta"> {record.number} </Tag>
            ) : (
              <Tag color="green"> {record.number} </Tag>
            )}{" "}
          </div>
        )
      },
      {
        title: "Supplier",
        dataIndex: "creditor",
        key: "creditor",
        width: "20%",
        ...this.getColumnSearchProps("customer_name"),
        render: (text, record) => (
          <div>
            <Text>record.creditor.name</Text>
          </div>
        )
      },
      {
        title: "Total",
        dataIndex: "net_total",
        key: "net_total",
        render: (text, record) => <span> {record.net_total.toFixed(2)} </span>
      },
      {
        title: "Recieved",
        dataIndex: "amount_recieved",
        key: "amount_recieved",
        render: (text, record) => (
          <span> {record.amount_recieved.toFixed(2)} </span>
        )
      },
      {
        title: "Balance",
        dataIndex: "balance",
        key: "balance",
        render: (text, record) => (
          <div>
            {" "}
            {record.balance > 0 ? (
              <b
                style={{
                  color: "#f50"
                }}
              >
                {" "}
                {`(${record.balance.toFixed(2)})`}{" "}
              </b>
            ) : (
              <span> {record.balance.toFixed(2)} </span>
            )}{" "}
          </div>
        )
      },
      {
        title: "Validity",
        dataIndex: "is_void",
        key: "is_void",
        render: (text, record) => (
          <div>
            {" "}
            {record.is_void ? (
              <Tag color="magenta"> Invalidated </Tag>
            ) : (
              <Tag color="green"> Valid </Tag>
            )}{" "}
          </div>
        )
      },
      {
        title: "Date Issued",
        dataIndex: "created_at",
        key: "created_at",
        width: "10px",
        render: (text, record) =>
          new Date(record.created_at).toLocaleDateString()
      },

      {
        title: "Action",
        dataIndex: "action",
        key: "action",
        render: (text, record) => (
          <span>
            <Button
              shape="circle"
              size="small"
              type="primary"
              icon="info-circle"
              loading={
                this.props.purchase[`purchaseDetailButton${record.id}IsLoading`]
              }
              onClick={this.props.show_purchase_detail.bind(this, record)}
            />{" "}
            <Divider type="vertical" />{" "}
            {record.is_void || record.balance < 1 ? (
              ""
            ) : (
              <span>
                <Button
                  shape="circle"
                  size="small"
                  icon="file-done"
                  onClick={() => {
                    this.props.update_receipt_detail(record);
                    this.props.toggle_add_receipt_drawer_visible(true);
                  }}
                />
                <Divider type="vertical" />{" "}
              </span>
            )}
            {record.is_void ? (
              <Button
                style={{
                  marginLeft: "2vw"
                }}
                shape="circle"
                size="small"
                icon="check-square"
                type="success"
                loading={
                  this.props.purchase[
                    `purchaseValidityButton${record.id}IsLoading`
                  ]
                }
                onClick={this.props.update_purchase_validity.bind(this, record)}
              />
            ) : (
              <Button
                style={{
                  marginLeft: "2vw"
                }}
                shape="circle"
                size="small"
                icon="close-square"
                type="danger"
                loading={
                  this.props.purchase[
                    `purchaseValidityButton${record.id}IsLoading`
                  ]
                }
                onClick={this.props.update_purchase_validity.bind(this, record)}
              />
            )}{" "}
          </span>
        )
      }
    ]
  };

  render() {
    const { purchases } = this.props;
    return (
      <Card>
        <Table
          bordered
          size="small"
          loading={purchases.loading}
          title={() => (
            <Row>
              <Col span={18}>
                <h1> Purchase Invoices</h1>{" "}
              </Col>{" "}
              <Col span={6}>
                <Button
                  type="dashed"
                  onClick={this.props.update_purchase_invoice_drawer_visible.bind(this,true)}
                >
                  Create Purchase Invoice
                </Button>
                <Button
                  style={{ marginLeft: "6px" }}
                  onClick={this.props.fetch_purchase_invoices}
                  type="primary"
                  icon="reload"
                />
              </Col>{" "}
            </Row>
          )}
          columns={this.state.columns}
          dataSource={purchases.datasource}
        />
      </Card>
    );
  }
}

const mapStateToProps = state => {
  return {
    purchases: state.purchases
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetch_purchase_invoices: () => dispatch(fetch_purchase_invoices),
    update_purchase_invoice_drawer_visible: (value) => dispatch(update_purchase_invoice_drawer_visible(value))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AppPurcharseInvoice);
