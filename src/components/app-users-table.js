import React, { Component } from "react";
import {
  Table,
  Tag,
  Divider,
  Button,
  Card,
  Row,
  Col,
  message,
  Input,
  Icon
} from "antd";
import { PostDataService } from "../services/post-data-service";
import { BASEAPIURL } from "../env";
import { MakeGraphQLQuery } from "../services/utils";
import CollectionCreateUserForm from "./app-user-modal";
import Highlighter from "react-highlight-words";
import { connect } from "react-redux";
import {
  fetch_users,
  update_user_modal_visible,
  update_user_edit_details,
  update_user_add_action,
  update_users_datasource
} from "../redux/actions/userActions";

class UsersTable extends Component {
  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
    )
  });

  state = {
    datasource: [],
    loading: true,
    editProfileVisible: false,
    profileUpdateAction: false,
    columns: [
      {
        title: "Name",
        dataIndex: "name",
        key: "name",
        width: "20%",
        ...this.getColumnSearchProps("name")
      },
      {
        title: "Email",
        dataIndex: "email",
        key: "email",
        ...this.getColumnSearchProps("email")
      },
      {
        title: "Phone",
        dataIndex: "phone",
        key: "phone",
        ...this.getColumnSearchProps("phone")
      },
      {
        title: "Status",
        dataIndex: "is_active",
        key: "is_active",
        render: (text, record) => (
          <div>
            {" "}
            {record.is_active && <Tag color="green"> Active </Tag>}{" "}
            {!record.is_active && <Tag color="volcano"> Inactivate </Tag>}{" "}
          </div>
        )
      },
      {
        title: "Roles",
        dataIndex: "roles",
        key: "roles",
        render: roles =>
          roles && (
            <div>
              {" "}
              {roles.map(role => {
                let color = "";
                switch (role.name) {
                  case "admin":
                    color = "green";
                    break;
                  case "vendor":
                    color = "purple";
                    break;
                  default:
                    color = "gold";
                }
                return (
                  <Tag color={color} key={role.id}>
                    {" "}
                    {role.name.toUpperCase()}{" "}
                  </Tag>
                );
              })}{" "}
            </div>
          )
      },
      {
        title: "Action",
        dataIndex: "action",
        key: "action",
        render: (text, record) => (
          <span>
            <Button
              shape="circle"
              size="small"
              icon="edit"
              onClick={this.showEditProfileModal.bind(this, record)}
            />{" "}
            <Divider type="vertical" />{" "}
            {record.is_active && (
              <Button
                size="small"
                type="danger"
                ghost
                onClick={this.handleDeactivateStaff.bind(this, record.id)}
                loading={this.state[`iconLoading${record.id}`]}
              >
                Deactivate{" "}
              </Button>
            )}{" "}
            {!record.is_active && (
              <Button
                size="small"
                type="primary"
                ghost
                onClick={this.handleActivateStaff.bind(this, record.id)}
                loading={this.state[`iconLoading${record.id}`]}
              >
                Activate{" "}
              </Button>
            )}
          </span>
        )
      }
    ]
  };

  success = data => {
    data = {
      ...data,
      key: data.id
    };
    // console.log(data);

    if (this.state.profileUpdateAction) {
      this.setState({
        datasource: [
          ...this.state.datasource.map(dtsrc => {
            if (dtsrc.id === data.id && dtsrc.email === data.email) {
              return data;
            }
            return dtsrc;
          })
        ]
      });
      message.success(`Staff Updated`);
    } else {
      this.setState({
        datasource: [...this.state.datasource, data]
      });
      message.success(`Staff Added`);
    }
  };

  handleActivateStaff = id => {
    let data = {
      data: {
        id: id
      }
    };
    this.setState({
      [`iconLoading${id}`]: true
    });
    if (this.state[`iconLoading${id}`]) return;
    PostDataService(
      BASEAPIURL,
      MakeGraphQLQuery("mutation", "activateStaff", data, [
        "id",
        "name",
        "email",
        "phone",
        "is_active",
        "roles{id name}"
      ]),
      {
        success: data => {
          // console.log(data);
          if (data.data) {
            data = data.data["activateStaff"];
            data["key"] = data.id;
            this.props.update_user_datasource(
              this.props.user.datasource.map(user => {
                if (user.id === data.id) return data;

                return user;
              })
            );

            message.success("Staff activated");
          } else if (data.errors)
            message.errors(
              data.errors.reduce((accumulator, error) => {
                if (accumulator) return (accumulator += error.message);
                else return (accumulator += `\n${error.message}`);
              }, 0)
            );
        },
        always: () => {
          this.setState({
            [`iconLoading${id}`]: false
          });
        }
      }
    );
  };

  /**
   * This function is currently not being used
   */
  handleRemoveStaff = id => {
    let data = {
      data: {
        id: id
      }
    };
    if (this.state[`removeIconLoading${id}`]) return;
    this.setState({
      [`removeIconLoading${id}`]: true
    });
    PostDataService(
      BASEAPIURL,
      MakeGraphQLQuery("mutation", "removeStaff", data, [
        "id",
        "name",
        "email",
        "phone",
        "is_active",
        "roles{id name}"
      ]),
      {
        success: data => {
          // console.log(data);
          if (data.data) {
            data = data.data["removeStaff"];
            data["key"] = data.id;
            this.props.update_user_datasource(
              this.props.user.datasource.filter(user => {
                if (user.id !== data.id) return true;

                return user;
              })
            );
            // this.setState({
            //   datasource: [
            //     ...this.state.datasource.filter(dt => {
            //       if (dt.id !== id) return true;
            //     })
            //   ]
            // });
            message.success("Staff removed");
          } else if (data.errors)
            message.errors(
              data.errors.reduce((accumulator, error) => {
                if (accumulator) return (accumulator += error.message);
                else return (accumulator += `\n${error.message}`);
              }, 0)
            );
        },
        always: () => {
          this.setState({
            [`removeIconLoading${id}`]: false
          });
        }
      }
    );
  };

  handleDeactivateStaff = id => {
    let data = {
      data: {
        id: id
      }
    };
    if (this.state[`iconLoading${id}`]) return;
    this.setState({
      [`iconLoading${id}`]: true
    });
    PostDataService(
      BASEAPIURL,
      MakeGraphQLQuery("mutation", "deactivateStaff", data, [
        "id",
        "name",
        "email",
        "phone",
        "is_active",
        "roles{id name}"
      ]),
      {
        success: data => {
          // console.log(data);
          if (data.data) {
            data = data.data["deactivateStaff"];
            data["key"] = data.id;
            this.props.update_user_datasource(
              this.props.user.datasource.map(user => {
                if (user.id === data.id) return data;

                return user;
              })
            );

            message.success("Staff deactivated");
          } else if (data.errors)
            message.errors(
              data.errors.reduce((accumulator, error) => {
                if (accumulator) return (accumulator += error.message);
                else return (accumulator += `\n${error.message}`);
              }, 0)
            );
        },
        always: () => {
          this.setState({
            [`iconLoading${id}`]: false
          });
        }
      }
    );
  };

  handleStaffAdd = event => {
    this.props.update_user_edit_details({
      username: "",
      useremail: "",
      userphone: "",
      userrole: ""
    });
    this.props.update_user_modal_visible(true);
  };

  showEditProfileModal = data => {
    let role = data.roles[0] ? data.roles[0] : "";

    this.props.update_user_edit_details({
      username: data.name,
      useremail: data.email,
      userphone: data.phone,
      userrole: role.name
    });
    this.props.update_user_add_action(true);
    this.props.update_user_modal_visible(true);
  };

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  cancelEditProfileModal = () => {
    this.setState({
      ...this.state,
      editProfileVisible: false,
      prefillValues: {}
    });
  };

  componentWillMount() {
    if (!this.props.user.loading && this.props.user.datasource === null) {
      this.props.fetch_users();
    }
  }

  render() {
    const { user } = this.props;
    return (
      <Card>
        
        <Table
          bordered
          size="small"
          loading={user.loading}
          title={() => (
            <Row>
              <Col span={20}>
                <h1> Staff Listing </h1>{" "}
              </Col>{" "}
              <Col span={4}>
                <Button type="dashed" onClick={this.handleStaffAdd}>
                  Add Staff{" "}
                </Button>{" "}
              </Col>{" "}
            </Row>
          )}
          columns={this.state.columns}
          dataSource={user.datasource}
        />{" "}
      </Card>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetch_users: () => dispatch(fetch_users),
    update_user_modal_visible: value =>
      dispatch(update_user_modal_visible(value)),
    update_user_add_action: value => dispatch(update_user_add_action(value)),
    update_user_edit_details: value =>
      dispatch(update_user_edit_details(value)),
    update_user_datasource: value => dispatch(update_users_datasource(value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersTable);
