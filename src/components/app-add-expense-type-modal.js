import React from "react";
import AppAddExpenseType from "./expenses/app-add-expenses-type";
import { connect } from "react-redux";
import { Modal } from "antd";
import { update_add_expenses_type_visible } from "../redux/actions/expensesTypeActions";

const AppAddExpenseTypeModal = props => {
  return (
    <Modal
      title={
        props.expensesType.expensesTypeUpdateAction
          ? "Update Expense Type"
          : "Add Expenses Type"
      }
      visible={props.expensesType.addExpensesTypeVisible}
      onCancel={props.update_add_expenses_type_visible.bind(this, false)}
      footer={false}
    >
      <AppAddExpenseType />
    </Modal>
  );
};

const mapStateToProps = state => {
  return {
    expensesType: state.expensesType
  };
};

const mapDispatchToProps = dispatch => {
  return {
    update_add_expenses_type_visible: value =>
      dispatch(update_add_expenses_type_visible(value))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppAddExpenseTypeModal);
