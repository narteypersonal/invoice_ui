import React, { Component } from "react";
import {
  Form,
  Row,
  Col,
  Layout,
  Input,
  Button,
  Card,
  Icon,
  message
} from "antd";

import { Link } from "react-router-dom";
import { PostDataService } from "../services/post-data-service";
import { MakeGraphQLQuery } from "../services/utils";
import { BASEAPIURL } from "../env";

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class AppForgottenPassword extends Component {
  state = {
    redirect: false
  };
  componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState({
          iconLoading: true
        });

        let data = values;
        data = {
          data
        };
        let queryName = "resetPassword";
        let query = MakeGraphQLQuery("mutation", queryName, data, ["message"]);
        PostDataService(BASEAPIURL, query, {
          success: data => {
            console.log(data);
            if (data.data && data.data[queryName]) {
              if (data.data[queryName] === "Password reset");
              message.success("Password reset");
            } else if (data.errors) {
              message.error(data.errors[0].message, 10);
            }
          },
          always: () => {
            this.setState({
              iconLoading: false
            });
          }
        });
      }
    });
  };

  render() {
    const {
      getFieldDecorator,
      getFieldsError,
      getFieldError,
      isFieldTouched
    } = this.props.form;
    const usernameError =
      isFieldTouched("username") && getFieldError("username");

    return (
      <Layout
        style={{
          height: "100vh"
        }}
      >
        <Row>
          <Col span={24}>
            <Col
              span={12}
              offset={6}
              style={{
                marginTop: "30vh"
              }}
            >
              <Card
                title="Provide Email to Reset Password"
                style={{
                  borderRadius: "15px 15px"
                }}
                extra={
                  <Link className="login-form" to="/login">
                    Back to Login{" "}
                  </Link>
                }
              >
                <Form onSubmit={this.handleSubmit}>
                  <Col span={18}>
                    <Form.Item
                      validateStatus={usernameError ? "error" : ""}
                      help={usernameError || ""}
                    >
                      {" "}
                      {getFieldDecorator("username", {
                        rules: [
                          {
                            required: true,
                            message: "Please input your email!"
                          }
                        ]
                      })(
                        <Input
                          prefix={
                            <Icon
                              type="mail"
                              style={{
                                color: "rgba(0,0,0,.25)"
                              }}
                            />
                          }
                          autoComplete="email"
                          type="email"
                          size="large"
                          placeholder="ex: example@email.com"
                        />
                      )}{" "}
                    </Form.Item>{" "}
                  </Col>{" "}
                  <Col span={2}>
                    <Form.Item>
                      <Button
                        type="primary"
                        htmlType="submit"
                        size="large"
                        disabled={hasErrors(getFieldsError())}
                        loading={this.state.iconLoading}
                      >
                        Reset{" "}
                      </Button>{" "}
                    </Form.Item>{" "}
                  </Col>{" "}
                </Form>{" "}
              </Card>{" "}
            </Col>{" "}
          </Col>{" "}
        </Row>{" "}
      </Layout>
    );
  }
}

export const ForgottenPassword = Form.create({
  name: "horizontal_forgotten_password"
})(AppForgottenPassword);
