import React, { useEffect, useState } from "react";
import { Card, Row, Col, Icon, Tag, Button, Typography } from "antd";
import { connect } from "react-redux";
import {
  update_invoice_modal_visible,
  fetch_invoices,
  update_invoice_validity,
  show_invoice_detail
} from "../../redux/actions/invoiceActions";

const { Paragraph } = Typography;

function AppRecentInvoice(props) {
  const { update_invoice_modal_visible, invoice } = props;
  const makeInvoiceCards = source => {
    return source.map((record, i) => {
      return (
        <Col
          key={record.id}
          xs={{
            span: 24
          }}
          sm={{
            span: 10
          }}
          lg={{
            span: 7
          }}
        >
          <Card
            title={
              record.is_void ? (
                <Tag
                  style={{
                    float: "left"
                  }}
                  color="magenta"
                >
                  {" "}
                  {record.num}{" "}
                </Tag>
              ) : (
                <Tag
                  style={{
                    float: "left"
                  }}
                  color="green"
                >
                  {" "}
                  {record.num}{" "}
                </Tag>
              )
            }
            style={{
              textAlign: "left"
            }}
            actions={[
              <Button
                shape="circle"
                size="small"
                type="primary"
                icon="info-circle"
                loading={invoice[`invoiceDetailButton${record.id}IsLoading`]}
                onClick={() => props.show_invoice_detail(record)}
              />,
              record.is_void ? (
                <Button
                  style={{
                    marginLeft: "2vw"
                  }}
                  shape="circle"
                  size="small"
                  icon="check-square"
                  type="success"
                  loading={
                    invoice[`invoiceValidityButton${record.id}IsLoading`]
                  }
                  onClick={() => props.update_invoice_validity(record)}
                />
              ) : (
                <Button
                  style={{
                    marginLeft: "2vw"
                  }}
                  shape="circle"
                  size="small"
                  icon="close-square"
                  type="danger"
                  loading={
                    invoice[`invoiceValidityButton${record.id}IsLoading`]
                  }
                  onClick={() => props.update_invoice_validity(record)}
                />
              )
            ]}
          >
            <Paragraph ellipsis> Customer: {record.customer_name} </Paragraph>{" "}
            <Paragraph ellipsis>
              Paid: <b> {record.amount_recieved.toFixed(2)} </b>{" "}
            </Paragraph>{" "}
            <Paragraph ellipsis>
              Balance: <b> {record.balance.toFixed(2)} </b>{" "}
            </Paragraph>{" "}
          </Card>{" "}
        </Col>
      );
    });
  };
  useEffect(() => {
    if (!invoice.loading && !invoice.datasource) {
      props.fetch_invoices();
    }
  });

  return (
    <Row gutter={16}>
      <Col
        xs={{
          span: 24
        }}
        sm={{
          span: 10
        }}
        lg={{
          span: 7
        }}
      >
        <Card
          hoverable
          style={{
            paddingTop: "8vh",
            paddingBottom: "8vh",
            borderStyle: "dashed",
            borderWidth: 2,
            backgroundColor: "#fff1",
            textAlign: "center"
          }}
          onClick={() => update_invoice_modal_visible(true)}
        >
          <Icon type="plus" />
          <h3> New Invoice </h3>{" "}
        </Card>{" "}
      </Col>{" "}
      {!invoice.loading && invoice.datasource ? (
        makeInvoiceCards(invoice.datasource)
      ) : (
        <div>
          <Col
            xs={{
              span: 24
            }}
            sm={{
              span: 10
            }}
            lg={{
              span: 7
            }}
          >
            <Card
              loading={invoice.loading}
              actions={[
                <Icon type="info-circle" key="setting" />,

                <Icon type="close-square" key="edit" />
              ]}
            />{" "}
          </Col>{" "}
          <Col
            xs={{
              span: 24
            }}
            sm={{
              span: 10
            }}
            lg={{
              span: 7
            }}
          >
            <Card
              loading={invoice.loading}
              actions={[
                <Icon type="info-circle" key="setting" />,

                <Icon type="close-square" key="edit" />
              ]}
            />{" "}
          </Col>{" "}
          <Col
            xs={{
              span: 24
            }}
            sm={{
              span: 10
            }}
            lg={{
              span: 7
            }}
          >
            <Card
              loading={invoice.loading}
              actions={[
                <Icon type="info-circle" key="setting" />,

                <Icon type="close-square" key="edit" />
              ]}
            />{" "}
          </Col>{" "}
          <Col
            xs={{
              span: 24
            }}
            sm={{
              span: 10
            }}
            lg={{
              span: 7
            }}
          >
            <Card
              loading={invoice.loading}
              actions={[
                <Icon type="info-circle" key="setting" />,

                <Icon type="close-square" key="edit" />
              ]}
            />{" "}
          </Col>{" "}
        </div>
      )}{" "}
    </Row>
  );
}

const mapStateToProps = state => {
  return {
    invoice: state.invoice
  };
};
const mapDispatchToProps = dispatch => {
  return {
    update_invoice_modal_visible: value =>
      dispatch(update_invoice_modal_visible(value)),
    fetch_invoices: () => dispatch(fetch_invoices),
    update_invoice_validity: value =>
      dispatch(update_invoice_validity(dispatch, value)),
    show_invoice_detail: value => dispatch(show_invoice_detail(dispatch, value))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppRecentInvoice);
