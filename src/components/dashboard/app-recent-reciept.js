import React, { useState, useEffect } from "react";
import { Card, Row, Col, Icon, Tag, Button, Typography } from "antd";
import {
  toggle_receipt_modal_visible,
  update_receipt_validity,
  fetch_receipt
} from "../../redux/actions/receiptActions";
import { connect } from "react-redux";

function AppRecentreceipt(props) {
  const { receipt } = props;
  const [recieptList, setRecieptList] = useState([]);
  const { Paragraph } = Typography;
  const makeReceiptCards = source => {
    return source.map((record, i) => {
      return (
        <Col
          key={record.id}
          xs={{
            span: 24
          }}
          sm={{
            span: 10
          }}
          lg={{
            span: 7
          }}
        >
          <Card
            title={
              record.is_void ? (
                <Tag
                  style={{
                    float: "left"
                  }}
                  color="magenta"
                >
                  {" "}
                  {record.invoice.num}{" "}
                </Tag>
              ) : (
                <Tag
                  style={{
                    float: "left"
                  }}
                  color="green"
                >
                  {" "}
                  {record.invoice.num}{" "}
                </Tag>
              )
            }
            style={{
              textAlign: "left"
            }}
            actions={[
              record.is_void ? (
                <Button
                  style={{
                    marginLeft: "2vw"
                  }}
                  shape="circle"
                  size="small"
                  icon="check-square"
                  type="success"
                  loading={
                    receipt[`receiptValidityButton${record.id}IsLoading`]
                  }
                  onClick={() => props.update_receipt_validity(record)}
                />
              ) : (
                <Button
                  style={{
                    marginLeft: "2vw"
                  }}
                  shape="circle"
                  size="small"
                  icon="close-square"
                  type="danger"
                  loading={
                    receipt[`receiptValidityButton${record.id}IsLoading`]
                  }
                  onClick={() => props.update_receipt_validity(record)}
                />
              )
            ]}
          >
            <Paragraph ellipsis> Depositor: {record.depositor} </Paragraph>{" "}
            <Paragraph ellipsis>
              Paid: <b> {record.amount.toFixed(2)} </b>{" "}
            </Paragraph>{" "}
            <Paragraph ellipsis>
              Type: <b> {record.receipttype.name} </b>{" "}
            </Paragraph>{" "}
          </Card>{" "}
        </Col>
      );
    });
  };
  useEffect(() => {
    if (!receipt.loading && receipt.datasource === null) {
      props.fetch_receipt();
    }
  });
  return (
    <Row gutter={16}>
      <Col
        xs={{
          span: 24
        }}
        sm={{
          span: 10
        }}
        lg={{
          span: 7
        }}
      >
        <Card
          hoverable
          style={{
            paddingTop: "8vh",
            paddingBottom: "8vh",
            borderStyle: "dashed",
            borderWidth: 2,
            backgroundColor: "#fff1",
            textAlign: "center"
          }}
          onClick={() => props.toggle_receipt_modal_visible(true)}
        >
          <Icon type="plus" />
          <h3> New receipt </h3>{" "}
        </Card>{" "}
      </Col>{" "}
      {!receipt.loading && receipt.datasource ? (
        makeReceiptCards(receipt.datasource)
      ) : (
        <div>
          <Col
            xs={{
              span: 24
            }}
            sm={{
              span: 10
            }}
            lg={{
              span: 7
            }}
          >
            <Card
              loading={receipt.loading}
              actions={[<Icon type="close-square" key="edit" />]}
            />{" "}
          </Col>{" "}
          <Col
            xs={{
              span: 24
            }}
            sm={{
              span: 10
            }}
            lg={{
              span: 7
            }}
          >
            <Card
              loading={receipt.loading}
              actions={[<Icon type="close-square" key="edit" />]}
            />{" "}
          </Col>{" "}
          <Col
            xs={{
              span: 24
            }}
            sm={{
              span: 10
            }}
            lg={{
              span: 7
            }}
          >
            <Card
              loading={receipt.loading}
              actions={[<Icon type="close-square" key="edit" />]}
            />{" "}
          </Col>{" "}
          <Col
            xs={{
              span: 24
            }}
            sm={{
              span: 10
            }}
            lg={{
              span: 7
            }}
          >
            <Card
              loading={receipt.loading}
              actions={[<Icon type="close-square" key="edit" />]}
            />{" "}
          </Col>{" "}
        </div>
      )}{" "}
    </Row>
  );
}

const mapStateToProps = state => {
  return {
    receipt: state.receipt
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetch_receipt: () => dispatch(fetch_receipt),
    toggle_receipt_modal_visible: value =>
      dispatch(toggle_receipt_modal_visible(value)),
    update_receipt_validity: value =>
      dispatch(update_receipt_validity(dispatch, value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppRecentreceipt);
