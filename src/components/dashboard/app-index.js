import React, { useState } from "react";
import { Card, Row, Col } from "antd";

import AppRecentInvoice from "./app-recent-invoice";
import AppRecentReciept from "./app-recent-reciept";
import { connect } from "react-redux";
import LeadingProductSalesAnalysis from "../analysis/leading-product-sales";

const tabList = [
  {
    key: "invoices",
    tab: "invoices"
  },
  {
    key: "reciepts",
    tab: "receipts"
  }
];

function Dashboard(props) {
  const [tab, onTabChange] = useState("invoices");
  const contentList = {
    invoices: <AppRecentInvoice />,
    reciepts: <AppRecentReciept />
  };

  return(
    <LeadingProductSalesAnalysis />
  )
  // return (
  //   <Row>
  //     <Col
  //       style={{
  //         paddingLeft: "2vw",
  //         paddingTop: "1vh"
  //       }}
  //     >
  //       <Card
  //         style={{
  //           width: "100%",
  //           backgroundColor: "#fff1",
  //           textAlign: "left"
  //         }}
  //         bordered={false}
  //         headStyle={{
  //           backgroundColor: "#fff"
  //         }}
  //         tabList={tabList}
  //         activeTabKey={tab}
  //         onTabChange={key => {
  //           onTabChange(key);
  //         }}
  //       >
  //         {contentList[tab]}{" "}
  //       </Card>{" "}
  //     </Col>{" "}
  //     <Col />
  //   </Row>
  // );
}

export default connect()(Dashboard);
