import React, { Component } from "react";
import { Row, Col, Table } from "antd";

export class PDFGenerator extends Component {
  render() {
    return (
      <div id="invoicePdfContainer">
        <Row>
          <Col span={6}>
            <img src="../images/ghlogo.png" alt="company logo" />
          </Col>
          <Col span={18} style={{ textAlign: "right" }}>
            <h3>www.ghkart.com</h3>
            <h3>P.O.Box KS12483 Accra</h3>
            <h3>+233(0)322064322</h3>
            <h3>+233(0)322064322</h3>
            <h3>Bank: Zenith</h3>
          </Col>
        </Row>
        <Row>
          <Col span={18}>
            <h3>Name: Customer1</h3>
            <h3>City: Kumasi</h3>
            <h3>Country: GHANA</h3>
          </Col>
          <Col span={6} style={{ marginTop: "6vh" }}>
            <h3>Invoice no: GHKA0012019</h3>
          </Col>
        </Row>
        <Row>
          <Table columns={[]} datasource={[]} />
        </Row>
      </div>
    );
  }
}
