import React, {
  Component
} from "react";
import {
  Form,
  Row,
  Col,
  Input,
  Button,
  message
} from "antd";
import {
  MakeGraphQLQuery
} from "../services/utils";
import {
  PostDataService
} from "../services/post-data-service";
import {
  connect
} from "react-redux";
import {
  get_invoice_prefix
} from "../redux/actions/invoiceActions";
import {
  BASEAPIURL
} from "../env";

const FormItem = Form.Item;
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 16
    }
  }
};
const AppInvoiceSettings = Form.create({
  name: "invoice_settings_form",
  mapPropsToFields(props) {
    return {
      prefix: Form.createFormField({
        value: props.invoicePrefix
      })
    };
  }
})(
  class InvoiceSettingsForm extends Component {
    state = {
      disabledInputs: true
    };

    handleEditClicked = e => {
      this.setState({
        disabledInputs: false
      });
    };

    handleResetClicked = () => {
      this.setState({
        disabledInputs: true
      });
      this.props.form.setFields({
        prefix: {
          value: this.props.invoicePrefix
        }
      });
    };

    handleSubmit = e => {
      e.preventDefault();
      if (this.state.disabledInputs) return;
      this.props.form.validateFields((err, values) => {
        if (!err) {
          this.setState({
            disabledInputs: true
          });
          this.props.triggerPrefix(values["prefix"]);
          this.props.triggerLoading(true);
          let data = values;
          data = {
            data
          };
          let queryName = "";
          let query = "";

          queryName = "setInvoicePrefix";
          query = MakeGraphQLQuery("mutation", queryName, data, ["prefix"]);

          PostDataService(BASEAPIURL, query, {
            success: data => {
              if (data.data[queryName]) {
                message.success("Prefix changed");
                // this.props.triggerLoading(false);
              } else if (data.errors) {
                message.error(data.errors[0].message);
              }
            },
            always: () => {
              this.props.triggerLoading(false);
              this.setState({
                disabledInputs: false
              });
            }
          });
        }
      });
    };

    render() {
      var {
        getFieldDecorator
      } = this.props.form;
      return ( <
        Row >
        <
        Form {
          ...formItemLayout
        }
        onSubmit = {
          this.handleSubmit
        } >
        <
        FormItem label = "Invoice Number Prefix" > {
          " "
        } {
          getFieldDecorator("prefix", {
            rules: [{
              required: true,
              message: "Please input number invoice prefix!"
            }]
          })( <
            Input disabled = {
              this.state.disabledInputs
            }
            placeholder = "ex: GHK." /
            >
          )
        } {
          " "
        } <
        /FormItem>{" "} <
        FormItem >
        <
        Row >
        <
        Col span = {
          12
        } >
        <
        Button type = ""
        htmlType = "reset"
        disabled = {
          this.props.isLoading
        }
        onClick = {
          this.handleResetClicked
        } >
        Reset {
          " "
        } <
        /Button>{" "} <
        /Col>{" "} <
        Col span = {
          12
        }
        style = {
          {
            textAlign: "right"
          }
        } >
        <
        Button htmlType = {
          this.state.disabledInputs ? "button" : "submit"
        }
        onClick = {
          e => {
            if (this.state.disabledInputs) {
              e.preventDefault();
              this.handleEditClicked.apply(this);
            }
          }
        }
        icon = {
          this.state.disabledInputs ? "edit" : "upload"
        }
        loading = {
          this.props.isLoading
        } >
        {
          this.state.disabledInputs ? "Edit" : "Submit"
        } {
          " "
        } <
        /Button>{" "} <
        /Col>{" "} <
        /Row>{" "} <
        /FormItem>{" "} <
        /Form>{" "} <
        /Row>
      );
    }
  }
);

export default connect()(AppInvoiceSettings);