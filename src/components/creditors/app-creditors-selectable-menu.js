import React from "react";
import { connect } from "react-redux";
import { Menu, Icon } from "antd";

const AppCreditorsSelectableMenu = props => {
  const generate = creditors => {
    return creditors
      ? creditors.map(creditor => {
          let MenuJSX = null;
          MenuJSX = (
            <Menu.Item
              key={creditor.id}
              onClick={props.onClick.bind(this, creditor)}
              style={{
                textAlign: "justify"
              }}
            >
              <span className="nav-text"> {creditor.name} </span>{" "}
              {props.creditors[`loading${creditor.name}Creditor`] && (
                <Icon type="loading" />
              )}{" "}
            </Menu.Item>
          );
          return MenuJSX;
        })
      : [];
  };

  return (
    <div
      style={{
        textTransform: "capitalize"
      }}
    >
      <h3>
        Creditors {props.creditors.loading ? <Icon type="loading" /> : ""}{" "}
      </h3>{" "}
      <Menu mode="inline">
        <Menu.Item
          key={0}
          onClick={props.onClick.bind(this, "")}
          style={{
            textAlign: "justify"
          }}
        >
          <span className="nav-text"> All </span>{" "}
          {props.creditors[`loading${""}Creditor`] && <Icon type="loading" />}{" "}
        </Menu.Item>{" "}
        {generate(props.creditors.datasource)}{" "}
      </Menu>{" "}
    </div>
  );
};

const mapStateToProps = state => {
  return {
    creditors: state.creditors
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppCreditorsSelectableMenu);
