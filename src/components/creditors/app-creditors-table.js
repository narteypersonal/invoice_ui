import React from "react";
import { Table, Row, Col, Button, Input, Icon, Divider } from "antd";
import { connect } from "react-redux";
import Highlighter from "react-highlight-words";
import {
  update_add_creditor_visible,
  update_creditor_action,
  update_creditor_edit_details,
  fetch_creditors,
  remove_creditor
} from "../../redux/actions/creditorActions";

class AppCreditorsTable extends React.Component {
  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div
        style={{
          padding: 8
        }}
      >
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{
            width: 188,
            marginBottom: 8,
            display: "block"
          }}
        />{" "}
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{
            width: 90,
            marginRight: 8
          }}
        >
          Search{" "}
        </Button>{" "}
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{
            width: 90
          }}
        >
          Reset{" "}
        </Button>{" "}
      </div>
    ),
    filterIcon: filtered => (
      <Icon
        type="search"
        style={{
          color: filtered ? "#1890ff" : undefined
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{
          backgroundColor: "#ffc069",
          padding: 0
        }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
    )
  });

  state = {
    searchText: "",
    columns: [
      {
        title: "Name",
        dataIndex: "name",
        key: "name",
        width: "20%",
        ...this.getColumnSearchProps("name")
      },

      {
        title: "contact",
        dataIndex: "contact",
        key: "contact",
        width: "15%",
        ...this.getColumnSearchProps("contact")
      },
      {
        title: "description",
        dataIndex: "description",
        key: "description",
        ...this.getColumnSearchProps("description")
      },
      {
        title: "Added",
        dataIndex: "created_at",
        key: "created_at",
        render: (text, record) =>
          new Date(record.created_at).toLocaleDateString()
      },
      {
        title: "Action",
        dataIndex: "action",
        key: "action",
        render: (text, record) => (
          <span>
            <Button
              shape="circle"
              size="small"
              icon="edit"
              onClick={this.showEditCreditorDetail.bind(this, record)}
            />{" "}
            <Divider type="vertical" />
            <Button
              shape="circle"
              size="small"
              icon="delete"
              loading={this.props.creditors[`removeIconLoading${record.id}`]}
              onClick={this.props.remove_creditor.bind(this, record.id)}
            />{" "}
          </span>
        )
      }
    ]
  };

  handleCredtiorAdd = event => {
    this.props.update_creditor_edit_details({
      creditorId: "",
      creditorName: "",
      creditorContact: "",
      creditorDescription: ""
    });
    this.props.update_add_creditor_visible(true);
    this.props.update_creditor_action(false);
  };

  showEditCreditorDetail = data => {
    this.props.update_creditor_edit_details({
      creditorId: data.id,
      creditorName: data.name,
      creditorContact: data.contact,
      creditorDescription: data.description
    });
    this.props.update_add_creditor_visible(true);
    this.props.update_creditor_action(true);
  };

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0]
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({
      searchText: ""
    });
  };

  componentWillMount() {
    if (
      !this.props.creditors.datasource ||
      (!this.props.creditors.loading &&
        this.props.creditors.datasource.length < 1)
    ) {
      this.props.fetch_creditors();
    }
  }
  render() {
    const { loading, datasource } = this.props.creditors;
    return (
      <Table
        bordered
        size="small"
        loading={loading}
        title={() => (
          <Row>
            <Col
              span={20}
              style={{
                textAlign: "left"
              }}
            >
              <h1> Suppliers </h1>{" "}
            </Col>{" "}
            <Col span={4}>
              <Button
                type="dashed"
                onClick={() => {
                  this.props.update_add_creditor_visible(true);
                  this.props.update_creditor_action(false);
                  this.props.update_creditor_edit_details({
                    creditorName: "",
                    creditorContact: "",
                    creditorDescription: ""
                  });
                }}
              >
                Add Supplier{" "}
              </Button>{" "}
              <Button
                onClick={this.props.fetch_creditors}
                type="primary"
                icon="reload"
              />
            </Col>{" "}
          </Row>
        )}
        columns={this.state.columns}
        dataSource={datasource}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    creditors: state.creditors
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetch_creditors: () => dispatch(fetch_creditors),
    update_add_creditor_visible: value =>
      dispatch(update_add_creditor_visible(value)),
    update_creditor_action: value => dispatch(update_creditor_action(value)),
    update_creditor_edit_details: value =>
      dispatch(update_creditor_edit_details(value)),
    remove_creditor: creditorId =>
      dispatch(remove_creditor(dispatch, creditorId))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppCreditorsTable);
