import React from "react";
import {
  Form,
  Row,
  Input,
  Col,
  Button,
  Radio,
  InputNumber,
  message,
  Select
} from "antd";
import { connect } from "react-redux";
import { update_add_credit_visible } from "../../redux/actions/creditsActions";
import { MakeGraphQLQuery } from "../../services/utils";
import { PostDataService } from "../../services/post-data-service";
import { BASEAPIURL } from "../../env";

const AppAddCredit = Form.create({ name: "app_add_credit" })(
  class extends React.Component {
    state = { submitting: false };

    handleSubmit = e => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          this.setState({ submitting: true });

          let data = {};
          data["creditor_id"] = values.creditor_id;
          data["description"] = values.description;

          if (values.type) data[`${values.type}`] = values.amount;
          data = {
            data
          };
          let queryName = "";
          let query = "";

          queryName = "createCredit";
          query = MakeGraphQLQuery("mutation", queryName, data, [
            "id",
            "number",
            "debit",
            "credit",
            "description",
            "created_at",
            "updated_at"
          ]);

          PostDataService(BASEAPIURL, query, {
            success: data => {
              if (data.data[queryName]) {
                data = data.data[queryName];
                data["key"] = data.id;

                message.success(`Creditor credited`);
                this.props.form.resetFields();
              } else if (data.errors) {
                message.error(data.errors[0].message);
              }
            },
            always: () => {
              this.setState({ submitting: false });
            }
          });
        }
      });
    };
    render() {
      const { getFieldDecorator } = this.props.form;
      return (
        <Form layout="vertical" onSubmit={this.handleSubmit}>
          <Row>
            <Col>
              <Form.Item label="Creditor/Supplier">
                {" "}
                {getFieldDecorator("creditor_id", {
                  rules: [
                    {
                      required: true,
                      message: "Select a creditor"
                    }
                  ]
                })(
                  <Select
                    placeholder="creditor name"
                    type="text"
                    style={{
                      width: "100%",
                      marginRight: 8
                    }}
                    // onChange={this.handleProductsChange.bind(this)}
                    name="creditor_id"
                  >
                    {this.props.creditors.datasource &&
                      this.props.creditors.datasource.map((creditor, index) => {
                        return (
                          <Select.Option value={creditor.id} key={creditor.id}>
                            {creditor.name}
                          </Select.Option>
                        );
                      })}
                  </Select>
                )}{" "}
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={8}>
            <Col xs={24} sm={12}>
              <Form.Item label="Amount">
                {" "}
                {getFieldDecorator("amount", {
                  rules: [
                    {
                      required: true,
                      message: "Invalid amount!"
                    },
                    {
                      validator: (rule, value, cb) =>
                        value > 0
                          ? cb()
                          : cb("Value must be greater than zero(0)")
                    }
                  ]
                })(<InputNumber min={0} name="amount" type="number" />)}{" "}
              </Form.Item>
            </Col>

            <Col xs={24} sm={12}>
              <Form.Item label="Type">
                {" "}
                {getFieldDecorator("type", {
                  rules: [
                    {
                      required: true,
                      message: "Select either credit or debit"
                    }
                  ]
                })(
                  <Radio.Group
                    name="type"
                    defaultValue="credit"
                    buttonStyle="solid"
                  >
                    <Radio.Button value="credit">Refund</Radio.Button>
                    <Radio.Button value="debit">Payment</Radio.Button>
                  </Radio.Group>
                )}{" "}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Item label="description">
                {" "}
                {getFieldDecorator("description", {
                  rules: [{ required: true }]
                })(<Input name="description" type="textarea" />)}{" "}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <Form.Item>
                <Button
                  type="default"
                  htmlType="reset"
                  onClick={this.props.update_add_credit_visible.bind(
                    this,
                    false
                  )}
                >
                  Cancel
                </Button>
              </Form.Item>
            </Col>
            <Col span={12} style={{ textAlign: "right" }}>
              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={this.state.submitting}
                >
                  Submit
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      );
    }
  }
);

const mapStateToProps = state => {
  return {
    credits: state.credits,
    creditors: state.creditors
  };
};

const mapDispatchToProps = dispatch => {
  return {
    update_add_credit_visible: value =>
      dispatch(update_add_credit_visible(value))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppAddCredit);
