import React from "react";
import { Row, Col, Table, Input, Button, Icon, Tag } from "antd";
import Highlighter from "react-highlight-words";
import { connect } from "react-redux";
import {
  update_add_credit_visible,
  update_credits_action,
  update_credits_edit_details,
  remove_credit,
  update_credits_datasource
} from "../../redux/actions/creditsActions";
import { fetch_creditor_credits } from "../../redux/actions/creditorActions";

let searchInput = "";
let searchText = "";
const getColumnSearchProps = dataIndex => ({
  filterDropdown: ({
    setSelectedKeys,
    selectedKeys,
    confirm,
    clearFilters
  }) => (
    <div
      style={{
        padding: 8
      }}
    >
      <Input
        ref={node => {
          searchInput = node;
        }}
        placeholder={`Search ${dataIndex}`}
        value={selectedKeys[0]}
        onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
        onPressEnter={() => handleSearch(selectedKeys, confirm)}
        style={{
          width: 188,
          marginBottom: 8,
          display: "block"
        }}
      />{" "}
      <Button
        type="primary"
        onClick={() => handleSearch(selectedKeys, confirm)}
        icon="search"
        size="small"
        style={{
          width: 90,
          marginRight: 8
        }}
      >
        Search{" "}
      </Button>{" "}
      <Button
        onClick={() => handleReset(clearFilters)}
        size="small"
        style={{
          width: 90
        }}
      >
        Reset{" "}
      </Button>{" "}
    </div>
  ),
  filterIcon: filtered => (
    <Icon
      type="search"
      style={{
        color: filtered ? "#1890ff" : undefined
      }}
    />
  ),
  onFilter: (value, record) =>
    record[dataIndex]
      .toString()
      .toLowerCase()
      .includes(value.toLowerCase()),
  onFilterDropdownVisibleChange: visible => {
    if (visible) {
      setTimeout(() => searchInput.select());
    }
  },
  render: text => (
    <Highlighter
      highlightStyle={{
        backgroundColor: "#ffc069",
        padding: 0
      }}
      searchWords={[searchText]}
      autoEscape
      textToHighlight={text.toString()}
    />
  )
});

const generateColumns = props => [
  {
    title: "Credit Number",
    dataIndex: "number",
    key: "number",
    width: "15%",
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div
        style={{
          padding: 8
        }}
      >
        <Input
          ref={node => {
            searchInput = node;
          }}
          placeholder={`Search credits number`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm)}
          style={{
            width: 188,
            marginBottom: 8,
            display: "block"
          }}
        />{" "}
        <Button
          type="primary"
          onClick={() => handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{
            width: 90,
            marginRight: 8
          }}
        >
          Search{" "}
        </Button>{" "}
        <Button
          onClick={() => handleReset(clearFilters)}
          size="small"
          style={{
            width: 90
          }}
        >
          Reset{" "}
        </Button>{" "}
      </div>
    ),
    filterIcon: filtered => (
      <Icon
        type="search"
        style={{
          color: filtered ? "#1890ff" : undefined
        }}
      />
    ),
    onFilter: (value, record) =>
      record["number"]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.select());
      }
    },
    render: (text, record) => (
      <div>
        <Tag color="green"> {record.number} </Tag>
      </div>
    )
  },
  {
    title: "Description",
    dataIndex: "description",
    key: "description",
    ...getColumnSearchProps("description")
  },
  {
    title: "Payments",
    dataIndex: "debit",
    key: "debit",
    width: "15%",
    style: { textAlign: "center" },
    render: (text, record) => (
      <span> {record.debit ? record.debit.toFixed(2) : "-"} </span>
    )
  },

  {
    title: "Refund",
    dataIndex: "credit",
    key: "credit",
    width: "15%",
    render: (text, record) => (
      <span style={{ textAlign: "center" }}>
        {record.credit ? record.credit.toFixed(2) : "-"}
      </span>
    )
  },

  {
    title: "Data Issued",
    dataIndex: "created_at",
    key: "created_at",
    width: "15%",
    render: (text, record) => (
      <span> {new Date(record.created_at).toLocaleDateString()} </span>
    )
  },

  {
    title: "Action",
    dataIndex: "action",
    key: "action",
    render: (text, record) => (
      <span>
        <Button
          shape="circle"
          size="small"
          icon="delete"
          loading={props.credits[`removeIconLoading${record.id}`]}
          onClick={props.remove_credit.bind(this, record.id)}
        />{" "}
      </span>
    )
  }
];

const handleSearch = (selectedKeys, confirm) => {
  confirm();

  searchText = selectedKeys[0];
};

const handleReset = clearFilters => {
  clearFilters();
  searchText = "";
};

const AppCreditsTable = props => {
  const { credits } = props;

  const { loading, datasource } = props.credits;
  let columns = generateColumns(props);
  return (
    <Table
      bordered
      size="small"
      loading={loading}
      title={() => (
        <Row>
          <Col
            span={20}
            style={{
              textAlign: "left"
            }}
          >
            <h1> Transactions </h1>{" "}
          </Col>{" "}
          <Col span={4}>
            <Button
              type="dashed"
              onClick={() => {
                props.update_add_credit_visible(true);
                props.update_credit_action(false);
                props.update_credit_edit_details({
                  creditNum: "",
                  creditDebit: "",
                  creditCredit: "",
                  creditDescription: ""
                });
              }}
            >
              Make Transaction{" "}
            </Button>{" "}
          </Col>{" "}
        </Row>
      )}
      columns={columns}
      dataSource={datasource}
    />
  );
};
const mapStateToProps = state => {
  return {
    credits: state.credits
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetch_credits: () => dispatch(fetch_creditor_credits),
    update_credits_datasource: value => dispatch(update_credits_datasource),
    update_add_credit_visible: value =>
      dispatch(update_add_credit_visible(value)),
    update_credit_action: value => dispatch(update_credits_action(value)),
    update_credit_edit_details: value =>
      dispatch(update_credits_edit_details(value)),
    remove_credit: creditID => dispatch(remove_credit(dispatch, creditID))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppCreditsTable);
