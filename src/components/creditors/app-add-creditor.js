import React from "react";
import { Row, Form, Input, Button, Col, Icon, message } from "antd";
import { connect } from "react-redux";
import { MakeGraphQLQuery } from "../../services/utils";
import { PostDataService } from "../../services/post-data-service";
import { BASEAPIURL } from "../../env";
import {
  update_add_creditor_visible,
  update_creditors_datasource
} from "../../redux/actions/creditorActions";

const AppAddCreditor = Form.create({
  name: "app_add_creditor_form",
  mapPropsToFields(props) {
    return {
      id: Form.createFormField({
        value: props.creditors.creditorId
      }),
      name: Form.createFormField({
        value: props.creditors.creditorName
      }),
      contact: Form.createFormField({
        value: props.creditors.creditorContact
      }),
      description: Form.createFormField({
        value: props.creditors.creditorDescription
      })
    };
  }
})(
  class extends React.Component {
    state = {
      submitting: false
    };

    handleSubmit = e => {
      this.setState({ submitting: true });
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          this.setState({
            iconLoading: true
          });
          let data = values;

          data = {
            data
          };
          let queryName = "";
          let query = "";
          if (this.props.creditors.creditorUpdateAction) {
            data.data["id"] = Number.parseInt(data.data["id"]);
            queryName = "updateCreditor";

            query = MakeGraphQLQuery("mutation", queryName, data, [
              "id",
              "name",
              "contact",
              "description",
              "created_at",
              "updated_at"
            ]);
          } else {
            queryName = "createCreditor";
            query = MakeGraphQLQuery("mutation", queryName, data, [
              "id",
              "name",
              "contact",
              "description",
              "created_at",
              "updated_at"
            ]);
          }

          PostDataService(BASEAPIURL, query, {
            success: data => {
              if (data.data[queryName]) {
                data = data.data[queryName];
                data["key"] = data.id;
                if (this.props.creditors.creditorUpdateAction) {
                  this.props.update_creditors_datasource(
                    this.props.creditors.datasource.map(dtsrc => {
                      if (dtsrc.id === data.id) {
                        return data;
                      }
                      return dtsrc;
                    })
                  );
                  message.success(`Creditor Updated`);
                  this.props.update_add_creditor_visible(false);
                } else {
                  if (this.props.creditors.datasource)
                    this.props.update_creditors_datasource([
                      data,
                      ...this.props.creditors.datasource
                    ]);
                  else this.props.update_creditors_datasource([data]);
                  message.success(`Creditor Added`);
                  this.props.form.resetFields();
                }
              } else if (data.errors) {
                message.error(data.errors[0].message);
              }
            },
            always: () => {
              this.setState({ submitting: false });
            }
          });
        }
      });
    };

    render() {
      const {
        getFieldDecorator,
        getFieldsError,
        getFieldError,
        isFieldTouched
      } = this.props.form;

      return (
        <Form layout="vertical" onSubmit={this.handleSubmit}>
          {this.props.creditors.creditorUpdateAction && (
            <Form.Item style={{ display: "none" }}>
              {" "}
              {getFieldDecorator("id", {})(
                <Input type="hidden" name="id" />
              )}{" "}
            </Form.Item>
          )}
          <Form.Item label="Business/Creditor Name">
            {" "}
            {getFieldDecorator("name", {
              rules: [
                {
                  required: true,
                  message: "Please input the creditor!"
                }
              ]
            })(<Input name="name" placeholder="Business/creditor name" />)}{" "}
          </Form.Item>{" "}
          <Form.Item label="Business/Creditor Contact">
            {getFieldDecorator("contact", {
              rules: [
                {
                  required: true
                }
              ]
            })(
              <Input
                prefix={
                  <Icon
                    type="contacts"
                    style={{
                      color: "rgba(0,0,0,.25)"
                    }}
                  />
                }
                onChange={this.handleValueChange}
                name="contact"
                type="tel"
              />
            )}
          </Form.Item>
          <Form.Item label="Description">
            {" "}
            {getFieldDecorator("description")(
              <Input name="description" type="textarea" />
            )}{" "}
          </Form.Item>
          <Row>
            <Col span={12}>
              <Form.Item>
                <Button
                  type="default"
                  htmlType="reset"
                  onClick={this.props.update_add_creditor_visible.bind(
                    this,
                    false
                  )}
                >
                  Cancel
                </Button>
              </Form.Item>
            </Col>
            <Col span={12} style={{ textAlign: "right" }}>
              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={this.state.submitting}
                >
                  Submit
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      );
    }
  }
);

const mapStateToProps = state => {
  return {
    creditors: state.creditors
  };
};

const mapDispatchToProps = dispatch => {
  return {
    update_add_creditor_visible: value =>
      dispatch(update_add_creditor_visible(value)),
    update_creditors_datasource: value =>
      dispatch(update_creditors_datasource(value))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppAddCreditor);
