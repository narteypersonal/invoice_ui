import React from "react";
import {
  Row,
  Col,
  Form,
  Input,
  InputNumber,
  Icon,
  Modal,
  Steps,
  Button,
  message,
  Select,
  Checkbox
} from "antd";

import { PostDataService } from "../services/post-data-service";
import { BASEAPIURL } from "../env";
import { MakeGraphQLQuery } from "../services/utils";
import {
  update_invoice_modal_visible,
  update_invoice_datasource
} from "../redux/actions/invoiceActions";
import { connect } from "react-redux";
import {
  update_product_modal_visible,
  fetch_products
} from "../redux/actions/productActions";
import {
  update_receipt_detail,
  toggle_add_receipt_drawer_visible
} from "../redux/actions/receiptActions";

const Step = Steps.Step;
var id = 0;

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 4
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 20
    }
  }
};

const formItemLayoutWithOutLabel = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0
    },
    sm: {
      span: 20,
      offset: 4
    }
  }
};
function hasErrors(fieldsError, fieldsKeys = []) {
  // console.log(fieldsError);
  return fieldsKeys.some(field => fieldsError[field]);
}
function calculateNewTotal(form) {
  form.setFields({
    total: {
      value: form
        .getFieldValue("prices")
        .reduce(
          (accumulator, price) => accumulator + Number.parseFloat(price),
          0
        )
    }
  });
}

function calculateTaxDiscountGrossTotalBalance(form) {
  //
  let tax_rate = form.getFieldValue("tax_rate");
  let total = form.getFieldValue("total");
  let discount = form.getFieldValue("discount_rate");

  let taxValue = tax_rate > 0 ? total * (tax_rate / 100) : 0;
  let discountValue = discount > 0 ? (total + taxValue) * (discount / 100) : 0;
  let gross_value = Math.ceil(total + taxValue - discountValue);

  form.setFieldsValue({
    total: total,
    gross_total: gross_value,
    balance: gross_value.toFixed(2)
  });
}

var productsListing;
const CollectionCreateInvoiceForm = Form.create({
  name: "form_in_modal",

  onValuesChange(_, fieldObject, allValues) {
    _.form.validateFields();
    if (fieldObject.products) {
      let index = fieldObject.products.length - 1;
      let product = fieldObject.products[index];
      let price;
      for (let prod of productsListing) {
        if (prod.name === product) {
          price = fieldObject.quantity
            ? prod.price
            : prod.price * allValues.quantity[index];
        }
      }
      _.form.setFields({
        [`prices[${index}]`]: {
          value: price
        }
      });

      calculateNewTotal(_.form);
      calculateTaxDiscountGrossTotalBalance(_.form);
    } else if (fieldObject.quantity) {
      let index = fieldObject.quantity.length - 1;
      let quantity = fieldObject.quantity[index];
      let price = 0;
      for (let prod of productsListing) {
        if (prod.name === allValues.products[index]) {
          price = prod.price * quantity;
        }
      }
      _.form.setFields({
        [`prices[${index}]`]: {
          value: price
        }
      });
      calculateNewTotal(_.form);
      calculateTaxDiscountGrossTotalBalance(_.form);
    } else if (fieldObject.prices) {
      let index = fieldObject.prices.length - 1;
      _.form.setFields({
        [`prices[${index}]`]: {
          value: fieldObject.prices[index]
        }
      });
      calculateNewTotal(_.form);
      calculateTaxDiscountGrossTotalBalance(_.form);
    } else if (fieldObject.tax_rate || fieldObject.discount_rate) {
      calculateTaxDiscountGrossTotalBalance(_.form);
    } else if (allValues.products && allValues.products.length > 0) {
      calculateNewTotal(_.form);
    }
  }
})(
  class DynamicFieldSet extends React.Component {
    state = {
      nextButtonDisabled: true,
      visitCustomer: true,
      isLoading: false,
      current: 0,
      products: [],
      steps: [
        {
          title: "Customer Details",
          hidden: false,
          fieldsKey: ["customerEmail", "customerName", "customerPhone"]
        },
        {
          title: "Products Details",
          hidden: true,
          fieldsKey: []
        },
        {
          title: "Discount|Tax",
          hidden: true,
          fieldsKey: []
        }
      ]
    };

    componentDidMount() {
      this.props.form.validateFields();

      if (
        !this.props.product.loading &&
        this.props.product.datasource === null
      ) {
        this.props.fetch_products();
      }
    }

    /**
     * Removes a product,quantity,price row 
     */
    remove = k => {
      const { form } = this.props;
      // can use data-binding to get
      const keys = form.getFieldValue("keys");
      // We need at least one passenger
      if (keys.length === 1) {
        return;
      }
      let newStep = this.state.steps[1];
      newStep.fieldsKey = newStep.fieldsKey.filter((key, index) => {
        // return key !== `products[${k}]` && key !== `quantity[${k}]`;
        // console.log(key);
        return key !== `productsRow${key}`;
      });
      this.setState({
        steps: [
          ...this.state.steps.map((step, index) => {
            if (index === 1) return newStep;
            return step;
          })
        ]
      });

      // can use data-binding to set
      form.setFieldsValue(
        {
          keys: keys.filter(key => key !== k)
        },
        form.validateFields
      );

      // console.log(this.state.steps[1]);
    };

    makeDefaultProductField = currentTab => {
      //if current tab is 1 and the number of product rows are none
      //then add a product row
      if (currentTab === 1) {
        if (this.props.form.getFieldValue("keys").length < 1) this.add();
      }
    };

    autoGenerateProductField = () => {};
    next(fieldsError) {
      // console.log(fieldsError);
      const current = this.state.current + 1;
      this.setState({
        step: this.state.steps.map((step, i) => {
          step.hidden = i === current ? false : true;
          return step;
        })
      });
      this.setState(
        { current },
        this.makeDefaultProductField.bind(this, current)
      );
    }

    prev(fieldsError) {
      const current = this.state.current - 1;
      this.setState({
        step: this.state.steps.map((step, i) => {
          step.hidden = i === current ? false : true;
          return step;
        })
      });
      this.setState({ current });
      this.validateStepRows(fieldsError);
    }

    /**
     * Adds a new product,quantity,price row dynamically
     */
    add = () => {
      let { form } = this.props;
      // can use data-binding to get
      let keys = form.getFieldValue("keys");
      let nextKeys = keys.concat(id);
      // can use data-binding to set
      let newStep = this.state.steps[1];
      // important! notify form to detect changes

      let promise = new Promise((resolve, reject) => {
        resolve(
          (() => {
            newStep.fieldsKey.push(id++);
            this.setState({
              steps: [
                ...this.state.steps.map((step, index) => {
                  if (index === 1) return newStep;
                  return step;
                })
              ]
            });
            form.setFieldsValue(
              {
                keys: nextKeys
              },
              form.validateFields
            );
          })()
        );
      });
    };

    validateStepRows = fieldsError => {
      // this.props.form.validateFields();
      if (this.state.current === 1) {
        let productsFieldsError = fieldsError["products"];
        let quantityFieldsError = fieldsError["quantity"];
        if (productsFieldsError && productsFieldsError.length > 0) {
          return (
            hasErrors(
              productsFieldsError,
              this.state.steps[this.state.current].fieldsKey
            ) ||
            hasErrors(
              quantityFieldsError,
              this.state.steps[this.state.current].fieldsKey
            )
          );
        } else {
          return false;
        }
      }
      let res =
        hasErrors(
          fieldsError,
          this.state.steps[this.state.current].fieldsKey
        ) && this.state.nextButtonDisabled;

      return res;
    };

    handleSubmit = e => {
      e.preventDefault();
      e.stopPropagation();
      message.info("Processing invoice...");
      this.setState({ isLoading: true });
      this.props.form.validateFields((err, values) => {
        // console.log(err);
        if (!err) {
          let data = {};
          let products = [];

          for (let i = 0; i < values.products.length; i++) {
            for (let prod of productsListing) {
              if (prod.name === values.products[i]) {
                products.push({
                  product_id: Number.parseInt(prod.id),
                  quantity: Number.parseInt(values.quantity[i])
                });
              }
            }
          }
          data["products"] = products;
          data["tax_rate"] = Number.parseFloat(values.tax_rate);
          data["discount_rate"] = Number.parseFloat(values.discount_rate);

          data["customer_email"] = values.customerEmail;
          data["customer_phone"] = values.customerPhone;
          data["customer_name"] = values.customerName;
          data["customer_no"] = values.customerNo ? values.customerNo : "";
          data = {
            data
          };
          let queryData = MakeGraphQLQuery("mutation", "createInvoice", data, [
            "id",
            "num",
            "customer_name",
            "customer_no",
            "net_total",
            "amount_recieved",
            "balance",
            "is_void",
            "created_at"
          ]);

          PostDataService(BASEAPIURL, queryData, {
            success: data => {
              if (data.data && data.data["createInvoice"]) {
                data = data.data["createInvoice"];
                this.props.update_invoice_datasource([
                  data,
                  ...this.props.invoice.datasource
                ]);
                message.success("Invoice generated");
                this.props.update_receipt_detail(data);
                this.props.toggle_add_receipt_drawer_visible(true);
                this.props.form.resetFields();
              }
            },
            always: () => {
              this.setState({ isLoading: false });
            }
          });
          // console.log(queryData);
        } else {
          this.setState({ isLoading: false });
        }
      });
    };

    handleProductsChange = value => {
      console.log(value);
    };

    handleQuantityValueChange = event => {
      console.log(event);
    };
    onCancel = () => {
      this.props.form.resetFields();
      const current = 0;
      this.setState({
        step: this.state.steps.map((step, i) => {
          step.hidden = i === current ? false : true;
          return step;
        })
      });
      this.setState({ current });
      this.props.onCancel();
    };
    render() {
      productsListing = this.props.product.datasource;
      const formItemLayout = {
        labelCol: {
          xs: { span: 24 },
          sm: { span: 8 }
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 16 }
        }
      };
      const {
        getFieldDecorator,
        getFieldValue,
        getFieldsError,
        getFieldError,
        isFieldTouched
      } = this.props.form;
      const nameError =
        isFieldTouched("customerName") && getFieldError("customerName");
      const emailError =
        isFieldTouched("customerEmail") && getFieldError("customerEmail");
      const phoneError =
        isFieldTouched("customerPhone") && getFieldError("customerPhone");
      getFieldDecorator("keys", {
        initialValue: []
      });
      getFieldDecorator("taxKeys", {
        initialValue: []
      });
      const keys = getFieldValue("keys");

      const formItems = keys.map((k, index) => {
        let productNameError =
          isFieldTouched(`products[${k}]`) && getFieldError(`products[${k}]`);
        let qtyNameError =
          isFieldTouched(`quantity[${k}]`) && getFieldError(`quantity[${k}]`);
        let priceError =
          isFieldTouched(`prices[${k}]`) && getFieldError(`prices[${k}]`);

        return (
          <Row key={`productsRow${k}`}>
            {index === 0 && (
              <Col span={24}>
                <h1>Products</h1>
              </Col>
            )}

            <Col span={12}>
              <Form.Item
                {...formItemLayoutWithOutLabel}
                validateStatus={productNameError ? "error" : ""}
                help={productNameError || ""}
                required={false}
                key={k}
              >
                {getFieldDecorator(`products[${k}]`, {
                  rules: [
                    {
                      required: true,
                      message: "Please select product!"
                    }
                  ]
                })(
                  <Select
                    placeholder="product name"
                    type="text"
                    style={{
                      width: "100%",
                      marginRight: 8
                    }}
                    // onChange={this.handleProductsChange.bind(this)}
                    name="products[]"
                  >
                    {this.props.product.datasource.map((product, index) => {
                      return (
                        <Select.Option
                          value={`${product.name}`}
                          key={product.id}
                        >
                          {product.name}
                        </Select.Option>
                      );
                    })}
                  </Select>
                )}
              </Form.Item>
            </Col>

            <Col span={6}>
              <Form.Item
                key={`qty${k}`}
                validateStatus={qtyNameError ? "error" : ""}
                help={qtyNameError || ""}
                required={false}
              >
                {getFieldDecorator(`quantity[${k}]`, {
                  rules: [
                    {
                      required: true,
                      message: "Please input product quantity!"
                    }
                  ],
                  initialValue: 1
                })(
                  <Input
                    placeholder="quantity"
                    type="number"
                    min={1}
                    style={{
                      width: "100%",
                      marginRight: 8
                    }}
                    name="quantity[]"
                  />
                )}
              </Form.Item>
            </Col>

            <Col span={4}>
              <Form.Item
                key={`prices[${k}]`}
                validateStatus={priceError ? "error" : ""}
                help={priceError || ""}
                required={false}
              >
                {getFieldDecorator(`prices[${k}]`, {
                  rules: [
                    {
                      message: "Invalid product price!",
                      type: "number"
                    }
                  ]
                })(
                  <InputNumber
                    placeholder="unit_price"
                    min={1}
                    style={{
                      width: "100%",
                      marginRight: 8
                    }}
                    name={`prices[]`}
                  />
                )}
              </Form.Item>
            </Col>

            <Col span={2} style={{ padding: 10 }}>
              {keys.length > 1 ? (
                <Icon
                  className="dynamic-delete-button"
                  type="minus-circle-o"
                  onClick={() => this.remove(k)}
                />
              ) : null}
            </Col>
          </Row>
        );
      });

      const { current } = this.state;
      const indicator = (
        <Steps current={current}>
          {this.state.steps.map(item => (
            <Step key={item.title} title={item.title} />
          ))}
        </Steps>
      );

      return (
        <Modal
          visible={this.props.invoice.invoiceModalVisible}
          onCancel={() => this.props.update_invoice_modal_visible(false)}
          footer={null}
          title={
            <Row>
              <Col span={23}>{indicator}</Col>
            </Row>
          }
          width={900}
          maskClosable={false}
        >
          <Form onSubmit={this.handleSubmit}>
            <div className="steps-content">
              {/* first step begins */}
              <Row gutter={2} hidden={this.state.steps[0].hidden}>
                <Col span={12}>
                  <Form.Item
                    label="Customer/Organization Fullname"
                    validateStatus={nameError ? "error" : ""}
                    help={nameError || ""}
                  >
                    {getFieldDecorator("customerName", {
                      rules: [
                        {
                          required: true,
                          message:
                            "Please input customer/organization full name!"
                        }
                      ]
                    })(
                      <Input
                        prefix={
                          <Icon
                            type="user"
                            style={{
                              color: "rgba(0,0,0,.25)"
                            }}
                          />
                        }
                        onChange={this.handleValueChange}
                        name="customerName"
                        type="text"
                      />
                    )}
                  </Form.Item>{" "}
                  <Form.Item
                    label="Customer/Organization Email"
                    validateStatus={emailError ? "error" : ""}
                    help={emailError || ""}
                  >
                    {getFieldDecorator("customerEmail", {
                      rules: [
                        {
                          message: "Please input customer email!"
                        }
                      ],
                      initialValue: ""
                    })(
                      <Input
                        prefix={
                          <Icon
                            type="mail"
                            style={{
                              color: "rgba(0,0,0,.25)"
                            }}
                          />
                        }
                        onChange={this.handleValueChange}
                        name="customerEmail"
                        type="email"
                      />
                    )}
                  </Form.Item>{" "}
                  <Form.Item
                    label="Customer/Organization Phone"
                    validateStatus={phoneError ? "error" : ""}
                    help={phoneError || ""}
                  >
                    {getFieldDecorator("customerPhone", {
                      rules: [
                        {
                          required: true,
                          message: "Please input customer phone number!",
                          pattern: /^[0-9]+$/
                        }
                      ]
                    })(
                      <Input
                        prefix={
                          <Icon
                            type="phone"
                            rotate={100}
                            style={{
                              color: "rgba(0,0,0,.25)"
                            }}
                          />
                        }
                        onChange={this.handleValueChange}
                        name="customerPhone"
                        type="tel"
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Row>
                    <Col span={10}>
                      <Form.Item label="Customer ID">
                        {getFieldDecorator("customerNo", {})(
                          <Input
                            disabled={this.state.autoGenerateUUID}
                            onChange={this.handleValueChange}
                            name="customerNo"
                            type="text"
                          />
                        )}
                      </Form.Item>
                    </Col>
                    <Col span={14}>
                      <Form.Item label="Auto-generate UUID">
                        {getFieldDecorator("autoGenUUID", {
                          defaultChecked: true
                        })(
                          <Checkbox
                            checked={this.state.autoGenerateUUID}
                            id="autoGenUUID"
                            style={{ marginLeft: "20px" }}
                            name="autoGenUUID"
                            size="small"
                            onChange={e =>
                              this.setState({
                                autoGenerateUUID: e.target.checked
                              })
                            }
                          />
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Col span={14}>
                    Visiting customer:{" "}
                    <Checkbox
                      name="visitor"
                      size="small"
                      onChange={e => {
                        this.setState({
                          visitCustomer: e.target.checked,
                          autoGenerateUUID: e.target.checked
                        });

                        if (e.target.checked) {
                          this.props.form.setFields({
                            customerName: {
                              value: "visitor"
                            },
                            customerPhone: {
                              value: "0000000000"
                            }
                          });
                        } else {
                          this.props.form.setFields({
                            customerName: {
                              value: ""
                            },
                            customerPhone: {
                              value: ""
                            }
                          });
                        }
                      }}
                    />
                  </Col>
                </Col>
              </Row>
              {/* first step ends */}
              {/* second step begings */}
              <Row hidden={this.state.steps[1].hidden}>
                {formItems}

                <Col span={6} offset={16}>
                  <Form.Item {...formItemLayout} label="Total">
                    {getFieldDecorator(`total`, {
                      initialValue: 0
                    })(<Input disabled={true} type="number" />)}
                  </Form.Item>
                </Col>

                <Form.Item {...formItemLayoutWithOutLabel}>
                  <Button
                    type="dashed"
                    onClick={this.add}
                    style={{
                      width: "60%"
                    }}
                  >
                    <Icon type="plus" /> Add field{" "}
                  </Button>
                </Form.Item>
              </Row>
              {/* second step ends */}
              {/* third step begings */}
              <Row hidden={this.state.steps[2].hidden}>
                <Col span={20}>
                  <Form.Item {...formItemLayout} label="Tax Rate">
                    {getFieldDecorator(`tax_rate`, {
                      initialValue: 0
                    })(<Input type="number" />)}
                  </Form.Item>
                  <Form.Item {...formItemLayout} label="Discount rate">
                    {getFieldDecorator(`discount_rate`, {
                      initialValue: 0
                    })(
                      <Input
                        name="discount_rate"
                        type="number"
                        step={0.1}
                        min={0}
                      />
                    )}
                  </Form.Item>
                  <Form.Item {...formItemLayout} label="Gross Total">
                    {getFieldDecorator(`gross_total`, {
                      initialValue: 0
                    })(<Input disabled={true} type="number" />)}
                  </Form.Item>

                  <Form.Item {...formItemLayout} label="Balance">
                    {getFieldDecorator(`balance`, {
                      initialValue: 0
                    })(<Input disabled={true} type="number" />)}
                  </Form.Item>
                </Col>
                <Col span={4} />
              </Row>
              {/* third step ends */}
            </div>
            <div className="steps-action" style={{ textAlign: "right" }}>
              {current > 0 && (
                <Button
                  style={{ marginLeft: 8 }}
                  onClick={() => this.prev(getFieldsError())}
                >
                  Previous
                </Button>
              )}
              {current < this.state.steps.length - 1 && (
                <Button
                  type="primary"
                  disabled={this.validateStepRows(getFieldsError())}
                  onClick={() => this.next(getFieldsError())}
                >
                  Next
                </Button>
              )}
              {current === this.state.steps.length - 1 && (
                <Button
                  type="primary"
                  loading={this.state.isLoading}
                  htmlType="submit"
                >
                  Done
                </Button>
              )}
            </div>
          </Form>
        </Modal>
      );
    }
  }
);

const mapStateToProps = state => {
  return {
    invoice: state.invoice,
    product: state.product
  };
};

const mapDispatchToProps = dispatch => {
  return {
    update_invoice_modal_visible: value =>
      dispatch(update_invoice_modal_visible(value)),
    update_invoice_datasource: value =>
      dispatch(update_invoice_datasource(value)),
    update_product_modal_visible: value =>
      dispatch(update_product_modal_visible(value)),
    fetch_products: () => dispatch(fetch_products),
    update_receipt_detail: value => dispatch(update_receipt_detail(value)),
    toggle_add_receipt_drawer_visible: value =>
      dispatch(toggle_add_receipt_drawer_visible(value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CollectionCreateInvoiceForm);
