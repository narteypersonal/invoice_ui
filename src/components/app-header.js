import React, { Component } from "react";
import { Redirect, Link } from "react-router-dom";
import { Layout, Avatar, Menu, Dropdown, Icon, message } from "antd";
import { connect } from "react-redux";
import { MakeGraphQLQuery } from "../services/utils";
import { PostDataService } from "../services/post-data-service";
import { BASEAPIURL } from "../env";
import { update_profile_datasource } from "../redux/actions/profileActions";
import { toggle_app_login } from "../redux/actions/appAction";
const { Header } = Layout;

class AppHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      affixTop: 0,
      menus: [
        {
          title: "profile",
          link: "/app/profile",
          icon: null
        },
        {
          title: "logout",
          action: this.handleLogout,
          icon: null
        }
      ]
    };
  }

  handleLogout = () => {
    // console.log(MakeGraphQLQuery("mutation", "logout", [], []));
    let data = [];
    data["customProvider"] = "staff";
    data = {
      data
    };
    PostDataService(
      BASEAPIURL,
      MakeGraphQLQuery("mutation", "logout", data, ["status", "message"]),
      {
        success: data => {
          if (data.data["logout"]) {
            sessionStorage.setItem("userData", "");
            sessionStorage.setItem("unitData", "");
            sessionStorage.setItem("roleData", "");
            sessionStorage.clear();
            this.props.update_profile_datasource({
              name: "",
              email: "",
              id: "",
              phone: "",
              is_admin: null,
              loading: null,
              passwordMismatch: null,
              passwordResetable: null
            });
            this.setState({
              redirect: true
            });
            this.props.toggle_app_login();
          } else if (data.errors) {
            message.error(data.errors[0].message);
          }
        }
      }
    );
  };

  dropdownMenu = (
    <Menu style={{ minWidth: "10vw" }}>
      {[
        {
          title: "profile",
          link: "/app/profile",
          icon: null
        },
        {
          title: "logout",
          action: this.handleLogout,
          icon: null
        }
      ].map((menu, i) => {
        return (
          <Menu.Item key={i} onClick={menu.action}>
            {menu.link ? (
              <Link to={`${menu.link}`}>
                {" "}
                {menu.icon && <Icon type={menu.icon} />}{" "}
                <span className="nav-text"> {menu.title} </span>{" "}
              </Link>
            ) : (
              <div>
                {" "}
                {menu.icon && <Icon type={menu.icon} />}{" "}
                <span className="nav-text"> {menu.title} </span>{" "}
              </div>
            )}
          </Menu.Item>
        );
      })}
    </Menu>
  );
  render() {
    return (
      <Header
        style={{
          background: "#fff",
          padding: "0 5",
          textAlign: "right"
        }}
      >
        <Dropdown overlay={this.dropdownMenu} placement="bottomCenter">
          <span style={{ textTransform: "capitalize", cursor: "pointer" }}>
            <Avatar
              style={{
                backgroundColor: "#7265e6",
                verticalAlign: "middle",
                cursor: "pointer"
              }}
              size="large"
              icon="user"
            />
            {this.props.profile.name}
          </span>
        </Dropdown>
      </Header>
    );
  }
}

const mapStateToProps = state => {
  return {
    profile: state.profile
  };
};

const mapDispatchToProps = dispatch => {
  return {
    update_profile_datasource: value =>
      dispatch(update_profile_datasource(value)),
    toggle_app_login: () => dispatch(toggle_app_login)
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppHeader);
