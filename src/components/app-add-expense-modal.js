import React from "react";
import { Modal } from "antd";
import AppAddExpenses from "./expenses/app-add-expenses";
import { connect } from "react-redux";
import { update_add_expenses_visible } from "../redux/actions/expensesActions";
const AppAddExpenseModal = props => {
  return (
    <Modal
      title={
        props.expenses.expensesUpdateAction ? "Edit Expenses" : "Add Expenses"
      }
      visible={props.expenses.addExpensesVisible}
      onCancel={props.update_add_expenses_visible.bind(this, false)}
      footer={false}
    >
      <AppAddExpenses />
    </Modal>
  );
};
const mapStateToProps = state => {
  return {
    expenses: state.expenses
  };
};

const mapDispatchToProps = dispatch => {
  return {
    update_add_expenses_visible: value =>
      dispatch(update_add_expenses_visible(value))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppAddExpenseModal);
