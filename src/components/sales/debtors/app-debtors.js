import React, { Component } from "react";
import {
  Table,
  Tag,
  Button,
  Card,
  Row,
  Col,
  Input,
  Icon,
  Switch,
  Divider
} from "antd";
import Highlighter from "react-highlight-words";
import { connect } from "react-redux";
import {
  fetch_debtorsJournal,
  show_debtor_detail
} from "../../../redux/actions/debtorsJournalActions";
import {
  update_receipt_detail,
  toggle_add_receipt_drawer_visible
} from "../../../redux/actions/receiptActions";

class AppDebtors extends Component {
  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div
        style={{
          padding: 8
        }}
      >
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{
            width: 188,
            marginBottom: 8,
            display: "block"
          }}
        />{" "}
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{
            width: 90,
            marginRight: 8
          }}
        >
          Search{" "}
        </Button>{" "}
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{
            width: 90
          }}
        >
          Reset{" "}
        </Button>{" "}
      </div>
    ),
    filterIcon: filtered => (
      <Icon
        type="search"
        style={{
          color: filtered ? "#1890ff" : undefined
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{
          backgroundColor: "#ffc069",
          padding: 0
        }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
    )
  });

  state = {
    searchText: "",
    useFilter: false,
    loading: false,
    columns: [
      {
        title: "Customer Number",
        dataIndex: "customer_no",
        key: "customer_no",
        filterDropdown: ({
          setSelectedKeys,
          selectedKeys,
          confirm,
          clearFilters
        }) => (
          <div
            style={{
              padding: 8
            }}
          >
            <Input
              ref={node => {
                this.searchInput = node;
              }}
              placeholder={`Search Customer Number`}
              value={selectedKeys[0]}
              onChange={e =>
                setSelectedKeys(e.target.value ? [e.target.value] : [])
              }
              onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
              style={{
                width: 188,
                marginBottom: 8,
                display: "block"
              }}
            />{" "}
            <Button
              type="primary"
              onClick={() => this.handleSearch(selectedKeys, confirm)}
              icon="search"
              size="small"
              style={{
                width: 90,
                marginRight: 8
              }}
            >
              Search{" "}
            </Button>{" "}
            <Button
              onClick={() => this.handleReset(clearFilters)}
              size="small"
              style={{
                width: 90
              }}
            >
              Reset{" "}
            </Button>{" "}
          </div>
        ),
        filterIcon: filtered => (
          <Icon
            type="search"
            style={{
              color: filtered ? "#1890ff" : undefined
            }}
          />
        ),
        onFilter: (value, record) =>
          record["customer_no"]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
          if (visible) {
            setTimeout(() => this.searchInput.select());
          }
        },
        render: (text, record) => (
          <Tag color="green"> {record.customer_no} </Tag>
        )
      },

      {
        title: "Customer Name",
        dataIndex: "customer_name",
        key: "customer_name",
        render: (text, record) => (
          <span> {record.invoices[0].customer_name} </span>
        )
      },
      {
        title: "Total Sales",
        dataIndex: "total_sale",
        key: "total_sale",
        render: (text, record) => <span> {record.total_sale.toFixed(2)} </span>
      },
      {
        title: "Amount Paid",
        dataIndex: "amount_paid",
        key: "amount_paid",
        render: (text, record) => <span> {record.amount_paid.toFixed(2)} </span>
      },
      {
        title: "Balance b/d",
        dataIndex: "balance_bd",
        key: "balance_bd",
        render: (text, record) => (
          <div>
            {" "}
            {record.balance_bd > 0 ? (
              <b
                style={{
                  color: "#f50"
                }}
              >
                {" "}
                {`(${record.balance_bd.toFixed(2)})`}{" "}
              </b>
            ) : (
              <span> {record.balance_bd.toFixed(2)} </span>
            )}{" "}
          </div>
        )
      },
      {
        title: "Data Issued",
        dataIndex: "created_at",
        key: "created_at",
        render: (text, record) => (
          <span> {new Date(record.created_at).toLocaleDateString()} </span>
        )
      },
      {
        title: "Action",
        dataIndex: "action",
        key: "action",
        render: (text, record) => (
          <span>
            <Button
              shape="circle"
              size="small"
              type="primary"
              icon="info-circle"
              loading={
                this.props.debtorsJournals[
                  `debtorDetailButton${record.id}IsLoading`
                ]
              }
              onClick={this.props.show_debtor_detail.bind(this, record)}
            />

            {record.is_void || record.balance_bd < 1 ? (
              ""
            ) : (
              <span>
                <Divider type="vertical" />{" "}
                <Button
                  shape="circle"
                  size="small"
                  icon="file-done"
                  onClick={() => {
                    let recordData = {
                      customer_name: record.invoices[0].customer_name,
                      num: record.invoices[0].num,
                      customer_no: record.customer_no,
                      net_total: record.total_sale,
                      balance: record.balance_bd,
                      amount_recieved: record.amount_paid
                    };
                    this.props.update_receipt_detail(recordData);
                    this.props.toggle_add_receipt_drawer_visible(true);
                  }}
                />
              </span>
            )}
          </span>
        )
      }
    ]
  };

  componentWillMount() {
    if (
      !this.props.debtorsJournals.datasource ||
      (!this.props.debtorsJournals.loading &&
        this.props.debtorsJournals.datasource.length < 1)
    ) {
      this.props.fetch_debtorsJournal();
    }
  }

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0]
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({
      searchText: ""
    });
  };

  render() {
    const { datasource, loading } = this.props.debtorsJournals;
    return (
      <Card>
        <Table
          bordered
          size="small"
          loading={loading}
          title={() => (
            <Row>
              <Col span={9}>
                <h1> Debtors Journal</h1>{" "}
              </Col>{" "}
              <Col span={15} id="dropdown-container">
                <Input.Search
                  disabled={this.state.loading}
                  suffix={this.state.loading ? <Icon type="loading" /> : ""}
                  placeholder="search customer number"
                  onSearch={value => this.handleSearch(value)}
                  style={{ width: 500 }}
                />
                <Button
                  type={this.state.useFilter ? "primary" : "dashed"}
                  onClick={e => {
                    // if (e.target.id !== "switch")
                    //   toggle_sales_account_filter_drawer_visible(
                    //     !salesAccountFilterDrawerVisible
                    //   );
                  }}
                >
                  Filter{" "}
                  <Switch
                    id="switch"
                    onChange={value => this.setState({ useFilter: value })}
                    size="small"
                    style={{ marginLeft: "2px" }}
                    defaultChecked={false}
                  />
                </Button>
                <Button
                  style={{ marginLeft: "6px" }}
                  onClick={this.props.fetch_debtorsJournal}
                  type="primary"
                  icon="reload"
                />
              </Col>
            </Row>
          )}
          columns={this.state.columns}
          dataSource={datasource}
        />
      </Card>
    );
  }
}

const mapStateToProps = state => {
  return {
    debtorsJournals: state.debtorsJournals
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetch_debtorsJournal: () => dispatch(fetch_debtorsJournal),
    show_debtor_detail: value => dispatch(show_debtor_detail(dispatch, value)),
    update_receipt_detail: value => dispatch(update_receipt_detail(value)),
    toggle_add_receipt_drawer_visible: value =>
      dispatch(toggle_add_receipt_drawer_visible(value))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppDebtors);
