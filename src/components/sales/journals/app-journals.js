import React, {
    Component
} from "react";
import {
    Table,
    Tag,
    Button,
    Card,
    Row,
    Col,
    Input,
    Icon,
    Switch
} from "antd";
import Highlighter from "react-highlight-words";
import {
    connect
} from "react-redux";
import {
    fetch_salesJournal,
    toggle_sales_journal_filter_drawer_visible
} from "../../../redux/actions/salesJournalAction"
import {
    show_invoice_detail
} from "../../../redux/actions/invoiceActions";


const ButtonGroup = Button.Group;
class AppSalesJournal extends Component {
    getColumnSearchProps = dataIndex => ({
        filterDropdown: ({
            setSelectedKeys,
            selectedKeys,
            confirm,
            clearFilters
        }) => ( <
            div style = {
                {
                    padding: 8
                }
            } >
            <
            Input ref = {
                node => {
                    this.searchInput = node;
                }
            }
            placeholder = {
                `Search ${dataIndex}`
            }
            value = {
                selectedKeys[0]
            }
            onChange = {
                e =>
                setSelectedKeys(e.target.value ? [e.target.value] : [])
            }
            onPressEnter = {
                () => this.handleSearch(selectedKeys, confirm)
            }
            style = {
                {
                    width: 188,
                    marginBottom: 8,
                    display: "block"
                }
            }
            />{" "} <
            Button type = "primary"
            onClick = {
                () => this.handleSearch(selectedKeys, confirm)
            }
            icon = "search"
            size = "small"
            style = {
                {
                    width: 90,
                    marginRight: 8
                }
            } >
            Search {
                " "
            } <
            /Button>{" "} <
            Button onClick = {
                () => this.handleReset(clearFilters)
            }
            size = "small"
            style = {
                {
                    width: 90
                }
            } >
            Reset {
                " "
            } <
            /Button>{" "} < /
            div >
        ),
        filterIcon: filtered => ( <
            Icon type = "search"
            style = {
                {
                    color: filtered ? "#1890ff" : undefined
                }
            }
            />
        ),
        onFilter: (value, record) =>
            record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select());
            }
        },
        render: text => ( <
            Highlighter highlightStyle = {
                {
                    backgroundColor: "#ffc069",
                    padding: 0
                }
            }
            searchWords = {
                [this.state.searchText]
            }
            autoEscape textToHighlight = {
                text.toString()
            }
            />
        )
    });
    state = {
        searchText: "",
        useFilter: false,
        loading: false,
        columns: [{
                title: "Invoice Number",
                dataIndex: "num",
                key: "num",
                filterDropdown: ({
                    setSelectedKeys,
                    selectedKeys,
                    confirm,
                    clearFilters
                }) => ( <
                    div style = {
                        {
                            padding: 8
                        }
                    } >
                    <
                    Input ref = {
                        node => {
                            this.searchInput = node;
                        }
                    }
                    placeholder = {
                        `Search Invoice Number`
                    }
                    value = {
                        selectedKeys[0]
                    }
                    onChange = {
                        e =>
                        setSelectedKeys(e.target.value ? [e.target.value] : [])
                    }
                    onPressEnter = {
                        () => this.handleSearch(selectedKeys, confirm)
                    }
                    style = {
                        {
                            width: 188,
                            marginBottom: 8,
                            display: "block"
                        }
                    }
                    />{" "} <
                    Button type = "primary"
                    onClick = {
                        () => this.handleSearch(selectedKeys, confirm)
                    }
                    icon = "search"
                    size = "small"
                    style = {
                        {
                            width: 90,
                            marginRight: 8
                        }
                    } >
                    Search {
                        " "
                    } <
                    /Button>{" "} <
                    Button onClick = {
                        () => this.handleReset(clearFilters)
                    }
                    size = "small"
                    style = {
                        {
                            width: 90
                        }
                    } >
                    Reset {
                        " "
                    } <
                    /Button>{" "} < /
                    div >
                ),
                filterIcon: filtered => ( <
                    Icon type = "search"
                    style = {
                        {
                            color: filtered ? "#1890ff" : undefined
                        }
                    }
                    />
                ),
                onFilter: (value, record) =>
                    record["invoice"]["num"]
                    .toString()
                    .toLowerCase()
                    .includes(value.toLowerCase()),
                onFilterDropdownVisibleChange: visible => {
                    if (visible) {
                        setTimeout(() => this.searchInput.select());
                    }
                },
                render: (text, record) => ( <
                    Tag color = "green" > {
                        record.invoice.num
                    } < /Tag>
                )
            },
            {
                title: "Arears",
                dataIndex: "arears",
                key: "arears",
                render: (text, record) => < span > {
                    record.arears.toFixed(2)
                } < /span>
            },
            {
                title: "Data Issued",
                dataIndex: "created_at",
                key: "created_at",
                render: (text, record) => ( < span > {
                        new Date(record.created_at).toLocaleDateString()
                    } < /span>)
                },
                {
                    title: "Action",
                    dataIndex: "action",
                    key: "action",
                    render: (text, record) => ( <
                        span >
                        <
                        Button shape = "circle"
                        size = "small"
                        type = "primary"
                        icon = "info-circle"
                        loading = {
                            this.props.invoice[
                                `invoiceDetailButton${record.invoice.id}IsLoading`
                            ]
                        }
                        onClick = {
                            this.props.show_invoice_detail.bind(
                                this,
                                record.invoice
                            )
                        }
                        />{" "} < /
                        span >
                    )
                }
            ]
        };

        componentWillMount() {
            if (
                !this.props.salesJournals.datasource ||
                (!this.props.salesJournals.loading &&
                    this.props.salesJournals.datasource.length < 1)
            ) {
                this.props.fetch_salesJournal();
            }
        }

        handleSearch(value) {
            this.setState({
                loading: true
            });
            console.log(value);
            setTimeout(() => {
                this.setState({
                    loading: false
                });
            }, 1000);
        }

        render() {
            const {
                loading,
                datasource,
                salesJournalFilterDrawerVisible
            } = this.props.salesJournals;

            const {
                toggle_sales_journal_filter_drawer_visible
            } = this.props;

            return ( <
                Card >
                <
                Table bordered size = "small"
                loading = {
                    loading
                }
                title = {
                    () => ( <
                        Row >
                        <
                        Col span = {
                            9
                        } >
                        <
                        h1 > Credit Sales < /h1>{" "} < /
                        Col > {
                            " "
                        } <
                        Col span = {
                            15
                        }
                        id = "dropdown-container" >
                        <
                        Input.Search disabled = {
                            this.state.loading
                        }
                        suffix = {
                            this.state.loading ? < Icon type = "loading" / > : ""
                        }
                        placeholder = "search invoice number"
                        onSearch = {
                            value => this.handleSearch(value)
                        }
                        style = {
                            {
                                width: 500
                            }
                        }
                        /> <
                        Button type = {
                            this.state.useFilter ? "primary" : "dashed"
                        }
                        onClick = {
                            e => {
                                if (e.target.id !== "switch")
                                    toggle_sales_journal_filter_drawer_visible(
                                        !salesJournalFilterDrawerVisible
                                    );
                            }
                        } >
                        Filter {
                            " "
                        } <
                        Switch id = "switch"
                        onChange = {
                            value => this.setState({
                                useFilter: value
                            })
                        }
                        size = "small"
                        style = {
                            {
                                marginLeft: "2px"
                            }
                        }
                        defaultChecked = {
                            false
                        }
                        /> < /
                        Button > <
                        /Col> < /
                        Row >
                    )
                }
                columns = {
                    this.state.columns
                }
                dataSource = {
                    datasource
                }
                /> < /
                Card >
            );
        }
    }

    const mapStateToProps = state => {
        return {
            salesJournals: state.salesJournals,
            invoice: state.invoice
        };
    };

    const mapDispatchToProps = dispatch => {
        return {
            fetch_salesJournal: () => dispatch(fetch_salesJournal),
            show_invoice_detail: (value) => dispatch(show_invoice_detail(dispatch, value)),
            toggle_sales_journal_filter_drawer_visible: (value) => dispatch(toggle_sales_journal_filter_drawer_visible(value))
        };
    };
    export default connect(
        mapStateToProps,
        mapDispatchToProps
    )(AppSalesJournal);