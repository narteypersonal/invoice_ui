import React from "react";
import { connect } from "react-redux";
import { Menu, Icon, Avatar } from "antd";

import { fetch_salesStaff } from "../../redux/actions/salesStaffAction";

const AppStaffSelectableMenu = props => {
  const generate = staffs => {
    let i = 0;

    return staffs
      ? staffs.map(staff => {
          let staffJSX = null;
          staffJSX = (
            <Menu.Item key={staff.id} onClick={props.onClick.bind(this, staff)}>
              {
                <div>
                  {staff.profileurl ? (
                    <Avatar size="small" icon={staff.profileurl} />
                  ) : (
                    <Avatar size="small" icon="user" />
                  )}
                  <span className="nav-text"> {staff.name} </span>
                </div>
              }
            </Menu.Item>
          );
          return staffJSX;
        })
      : [];
  };
  return (
    <div>
      <h3>
        Sales persons {props.salesStaff.loading ? <Icon type="loading" /> : ""}
      </h3>
      <Menu mode="inline">{generate(props.salesStaff.datasource)}</Menu>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    salesStaff: state.salesStaff
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetch_salesStaff: dispatch(fetch_salesStaff)
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppStaffSelectableMenu);
