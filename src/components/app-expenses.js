import React, { useEffect } from "react";
import { Row, Col, Card } from "antd";
import { connect } from "react-redux";
import AppExpensesTable from "./expenses/app-expenses-table";
import AppExpensesTypeSelectableMenu from "./expenses/app-expenses-type-selectable-menu";
import {
  fetch_expenses,
  update_expenses_datasource
} from "../redux/actions/expensesActions";
import { fetch_expenseType_expenses } from "../redux/actions/expensesTypeActions";

const AppExpenses = props => {
  // useEffect(() => {
  //   if (!props.expenses.loading && !props.expenses.datasource)
  //     props.fetch_expenses();
  // });
  return (
    <Row gutter={8}>
      <Col
        span={4}
        style={{ textAlign: "Left", height: "75vh", backgroundColor: "white" }}
      >
        <AppExpensesTypeSelectableMenu
          onClick={expenseType => {
            props.fetch_expenseType_expenses(
              expenseType.name,
              props.update_expenses_datasource
            );
          }}
        />
      </Col>
      <Col span={20}>
        <Card>
          <AppExpensesTable />
        </Card>
      </Col>
    </Row>
  );
};
const mapStateToProps = state => {
  return {
    expenses: state.expenses,
    expensesType: state.expensesType
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetch_expenses: () => dispatch(fetch_expenses),
    fetch_expenseType_expenses: (value, callback) =>
      dispatch(fetch_expenseType_expenses(dispatch, value, callback)),
    update_expenses_datasource: value =>
      dispatch(update_expenses_datasource(value))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppExpenses);
