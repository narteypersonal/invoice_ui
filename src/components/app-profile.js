import React, { Component } from "react";
import {
  Row,
  Col,
  Avatar,
  Button,
  Form,
  Input,
  Icon,
  Upload,
  Modal
} from "antd";
import AppProfileForm from "./app-profile-form";
import AppPasswordResetForm from "./app-reset-password-form";

export class AppProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      validateConfirmPasswordStatus: ""
    };
  }
  showModal = () => {
    this.setState({ visible: true });
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  confirmPassword = event => {
    let confpass = event.target.value;
  };
  render() {
    return (
      <div>
        <Row type="flex" justify="space-around">
          <Col
            xs={{ span: 23 }}
            lg={{ span: 11 }}
            className="bg-white"
            style={{ padding: "10vh 10px" }}
          >
            <AppProfileForm />
          </Col>

          <Col
            xs={{ span: 23 }}
            lg={{ span: 11 }}
            className="bg-white"
            style={{ padding: "19.23vh .5vw" }}
          >
            <AppPasswordResetForm />
          </Col>
        </Row>
      </div>
    );
  }
}
