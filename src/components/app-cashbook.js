import React from "react";
import AppCashbookTable from "./cashbooks/app-cashbook-table";
import { Card } from "antd";
import { connect } from "react-redux";
const AppCashbook = props => {
  return (
    <Card>
      <AppCashbookTable />;
    </Card>
  );
};

export default connect()(AppCashbook);
