import React from "react";
import { Row, Form, Input, Button, Col, message } from "antd";
import { connect } from "react-redux";
import {
  update_add_expenses_type_visible,
  update_expenses_type_datasource
} from "../../redux/actions/expensesTypeActions";
import { MakeGraphQLQuery } from "../../services/utils";
import { PostDataService } from "../../services/post-data-service";
import { BASEAPIURL } from "../../env";

const AppAddExpensesType = Form.create({
  name: "app_add_expenses_type_form",
  mapPropsToFields(props) {
    return {
      id: Form.createFormField({
        value: props.expensesType.expenseTypeId
      }),
      name: Form.createFormField({
        value: props.expensesType.expenseTypeName
      }),
      description: Form.createFormField({
        value: props.expensesType.expenseTypeDescription
      })
    };
  }
})(
  // eslint-disable-next-line
  class extends React.Component {
    state = {
      submitting: false
    };
    handleSubmit = e => {
      this.setState({ submitting: true });
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          this.setState({
            submitting: true
          });
          values['description'] = values['description'] || "";
          let data = values;
          data = {
            data
          };
          let queryName = "";
          let query = "";
          if (this.props.expensesType.expensesTypeUpdateAction) {
            data.data["id"] = Number.parseInt(data.data["id"]);
            queryName = "updateExpenseType";

            query = MakeGraphQLQuery("mutation", queryName, data, [
              "id",
              "name",
              "description",
              "created_at"
            ]);
          } else {
            queryName = "createExpenseType";
            query = MakeGraphQLQuery("mutation", queryName, data, [
              "id",
              "name",
              "description",
              "created_at"
            ]);
          }

          PostDataService(BASEAPIURL, query, {
            success: data => {
              if (data.data[queryName]) {
                data = data.data[queryName];
                data["key"] = data.id;
                if (this.props.expensesType.expensesTypeUpdateAction) {
                  this.props.update_expenses_type_datasource(
                    this.props.expensesType.datasource.map(dtsrc => {
                      if (dtsrc.id === data.id) {
                        return data;
                      }
                      return dtsrc;
                    })
                  );
                  message.success(`Expenses Type Updated`);
                  this.props.update_add_expenses_type_visible(false);
                } else {
                  if (this.props.expensesType.datasource)
                    this.props.update_expenses_type_datasource([
                      data,
                      ...this.props.expensesType.datasource
                    ]);
                  else this.props.update_expenses_type_datasource([data]);
                  message.success(`Expenses Type Added`);
                }

                this.props.form.resetFields();
              } else if (data.errors) {
                message.error(data.errors[0].message);
              }
            },
            always: () => {
              this.setState({ submitting: false });
            }
          });
        }
      });
    };

    render() {
      const { form } = this.props;
      const { getFieldDecorator } = form;
      return (
        <Form layout="vertical" onSubmit={this.handleSubmit}>
          {this.props.expensesType.expensesTypeUpdateAction && (
            <Form.Item style={{ display: "none" }}>
              {" "}
              {getFieldDecorator("id", {})(
                <Input type="hidden" name="id" />
              )}{" "}
            </Form.Item>
          )}
          <Form.Item label="Name">
            {" "}
            {getFieldDecorator("name", {
              rules: [
                {
                  required: true,
                  message: "Please input the expense type!"
                }
              ]
            })(
              <Input name="name" placeholder="egs:utility,salary,e.t.c" />
            )}{" "}
          </Form.Item>{" "}
          <Form.Item label="Description">
            {" "}
            {getFieldDecorator("description")(
              <Input name="description" type="textarea" />
            )}{" "}
          </Form.Item>
          <Row>
            <Col span={12}>
              <Form.Item>
                <Button
                  type="default"
                  htmlType="reset"
                  onClick={this.props.update_add_expenses_type_visible.bind(
                    this,
                    false
                  )}
                >
                  Cancel
                </Button>
              </Form.Item>
            </Col>
            <Col span={12} style={{ textAlign: "right" }}>
              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={this.state.submitting}
                >
                  Submit
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      );
    }
  }
);

const mapStateToProps = state => {
  return {
    expensesType: state.expensesType
  };
};

const mapDispatchToProps = dispatch => {
  return {
    update_add_expenses_type_visible: value =>
      dispatch(update_add_expenses_type_visible(value)),
    update_expenses_type_datasource: value =>
      dispatch(update_expenses_type_datasource(value))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppAddExpensesType);
