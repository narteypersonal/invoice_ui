import React from "react";
import {
  Row,
  Form,
  Input,
  Button,
  Col,
  message,
  InputNumber,
  Select
} from "antd";
import { connect } from "react-redux";

import { MakeGraphQLQuery } from "../../services/utils";
import { PostDataService } from "../../services/post-data-service";
import { BASEAPIURL } from "../../env";
import {
  update_add_expenses_visible,
  update_expenses_datasource
} from "../../redux/actions/expensesActions";

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}
const AppAddExpenses = Form.create({
  name: "app_add_expenses_type_form",
  mapPropsToFields(props) {
    return {
      id: Form.createFormField({
        value: props.expenses.expenseId
      }),
      num: Form.createFormField({
        value: props.expenses.expenseNum
      }),
      amount: Form.createFormField({
        value: props.expenses.expenseAmount
      }),
      description: Form.createFormField({
        value: props.expenses.expenseDescription
      }),
      expensetype_id: Form.createFormField({
        value: props.expenses.expenseType
      })
    };
  }
})(
  // eslint-disable-next-line
  class extends React.Component {
    state = {
      submitting: false
    };
    handleSubmit = e => {
      this.setState({ submitting: true });
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          this.setState({
            iconLoading: true
          });
          let data = values;

          for (let expensestype of this.props.expensesType.datasource) {
            if (expensestype.name === values["expensetype_id"]) {
              data["expensetype_id"] = expensestype.id;
            }
          }
          data = {
            data
          };
          let queryName = "";
          let query = "";
          if (this.props.expenses.expensesUpdateAction) {
            data.data["id"] = Number.parseInt(data.data["id"]);
            queryName = "updateExpenses";

            query = MakeGraphQLQuery("mutation", queryName, data, [
              "id",
              "num",
              "amount",
              "description",
              "expenseType{id name}",
              "created_at",
              "updated_at"
            ]);
          } else {
            queryName = "createExpenses";
            query = MakeGraphQLQuery("mutation", queryName, data, [
              "id",
              "num",
              "amount",
              "description",
              "expenseType{id name}",
              "created_at",
              "updated_at"
            ]);
          }

          PostDataService(BASEAPIURL, query, {
            success: data => {
              if (data.data[queryName]) {
                data = data.data[queryName];
                data["key"] = data.id;
                if (this.props.expenses.expensesUpdateAction) {
                  this.props.update_expenses_datasource(
                    this.props.expenses.datasource.map(dtsrc => {
                      if (dtsrc.id === data.id) {
                        return data;
                      }
                      return dtsrc;
                    })
                  );
                  message.success(`Expenses Updated`);
                  this.props.update_add_expenses_visible(false);
                } else {
                  if (this.props.expenses.datasource)
                    this.props.update_expenses_datasource([
                      data,
                      ...this.props.expenses.datasource
                    ]);
                  else this.props.update_expenses_datasource([data]);
                  message.success(`Expenses Added`);
                  this.props.form.resetFields();
                }

                
              } else if (data.errors) {
                message.error(data.errors[0].message);
              }
            },
            always: () => {
              this.setState({ submitting: false });
            }
          });
        }
      });
    };

    render() {
      const { Option } = Select;
      const {
        getFieldDecorator,
        getFieldsError,
        getFieldError,
        isFieldTouched
      } = this.props.form;
      const expensesError =
        isFieldTouched("expenses") && getFieldError("expenses");
      return (
        <Form layout="vertical" onSubmit={this.handleSubmit}>
          {this.props.expenses.expensesUpdateAction && (
            <Form.Item>
              {" "}
              {getFieldDecorator("id", {})(
                <Input type="hidden" name="id" />
              )}{" "}
            </Form.Item>
          )}
          <Form.Item label="Description">
            {" "}
            {getFieldDecorator("description")(
              <Input name="description" type="textarea" />
            )}{" "}
          </Form.Item>
          <Form.Item label="Amount">
            {getFieldDecorator("amount", {
              rules: [
                {
                  required: true,
                  message: "Please input the amount spent!"
                },
                {
                  validator: (rule, value, cb) =>
                    value > 0 ? cb() : cb("Value must be greater than zero(0)")
                }
              ],
              initialValue: 0
            })(
              <InputNumber
                min={0}
                formatter={value =>
                  `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                }
                name="amount"
              />
            )}{" "}
          </Form.Item>{" "}
          <Form.Item
            label="Expenses type"
            validateStatus={expensesError ? "error" : ""}
            help={expensesError || ""}
          >
            {getFieldDecorator("expensetype_id", {
              rules: [
                {
                  required: true,
                  message: "Please select a receipt type!"
                }
              ]
            })(
              <Select
                size="large"
                name="expensetype_id"
                loading={this.props.expensesType.loading}
              >
                {this.props.expensesType.datasource &&
                  this.props.expensesType.datasource.map(type => (
                    <Option key={type.name} value={type.name}>
                      {type.name}
                    </Option>
                  ))}
              </Select>
            )}
          </Form.Item>
          <Row>
            <Col span={12}>
              <Form.Item>
                <Button
                  type="default"
                  htmlType="reset"
                  onClick={() => {
                    this.props.update_add_expenses_visible(false);
                    this.props.form.resetFields();
                  }}
                >
                  Cancel
                </Button>
              </Form.Item>
            </Col>
            <Col span={12} style={{ textAlign: "right" }}>
              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={this.state.submitting}
                >
                  {this.props.expenses.expensesUpdateAction ? "Update" : "Add"}
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      );
    }
  }
);

const mapStateToProps = state => {
  return {
    expenses: state.expenses,
    expensesType: state.expensesType
  };
};

const mapDispatchToProps = dispatch => {
  return {
    update_add_expenses_visible: value =>
      dispatch(update_add_expenses_visible(value)),
    update_expenses_datasource: value =>
      dispatch(update_expenses_datasource(value))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppAddExpenses);
