import React, { useEffect } from "react";
import { Row, Col, Table, Input, Button, Icon, Tag, Divider } from "antd";
import Highlighter from "react-highlight-words";
import { connect } from "react-redux";
import { fetch_expensesType } from "../../redux/actions/expensesTypeActions";
import {
  update_add_expenses_visible,
  remove_expenses,
  update_expenses_edit_details,
  update_expenses_action
} from "../../redux/actions/expensesActions";

let searchInput = "";
let searchText = "";
const getColumnSearchProps = dataIndex => ({
  filterDropdown: ({
    setSelectedKeys,
    selectedKeys,
    confirm,
    clearFilters
  }) => (
    <div
      style={{
        padding: 8
      }}
    >
      <Input
        ref={node => {
          searchInput = node;
        }}
        placeholder={`Search ${dataIndex}`}
        value={selectedKeys[0]}
        onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
        onPressEnter={() => handleSearch(selectedKeys, confirm)}
        style={{
          width: 188,
          marginBottom: 8,
          display: "block"
        }}
      />{" "}
      <Button
        type="primary"
        onClick={() => handleSearch(selectedKeys, confirm)}
        icon="search"
        size="small"
        style={{
          width: 90,
          marginRight: 8
        }}
      >
        Search{" "}
      </Button>{" "}
      <Button
        onClick={() => handleReset(clearFilters)}
        size="small"
        style={{
          width: 90
        }}
      >
        Reset{" "}
      </Button>{" "}
    </div>
  ),
  filterIcon: filtered => (
    <Icon
      type="search"
      style={{
        color: filtered ? "#1890ff" : undefined
      }}
    />
  ),
  onFilter: (value, record) =>
    record[dataIndex]
      .toString()
      .toLowerCase()
      .includes(value.toLowerCase()),
  onFilterDropdownVisibleChange: visible => {
    if (visible) {
      setTimeout(() => searchInput.select());
    }
  },
  render: text => (
    <Highlighter
      highlightStyle={{
        backgroundColor: "#ffc069",
        padding: 0
      }}
      searchWords={[searchText]}
      autoEscape
      textToHighlight={text.toString()}
    />
  )
});
const generateColumns = props => [
  {
    title: "Expense Number",
    dataIndex: "num",
    key: "num",
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div
        style={{
          padding: 8
        }}
      >
        <Input
          ref={node => {
            searchInput = node;
          }}
          placeholder={`Search expenses number`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm)}
          style={{
            width: 188,
            marginBottom: 8,
            display: "block"
          }}
        />{" "}
        <Button
          type="primary"
          onClick={() => handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{
            width: 90,
            marginRight: 8
          }}
        >
          Search{" "}
        </Button>{" "}
        <Button
          onClick={() => handleReset(clearFilters)}
          size="small"
          style={{
            width: 90
          }}
        >
          Reset{" "}
        </Button>{" "}
      </div>
    ),
    filterIcon: filtered => (
      <Icon
        type="search"
        style={{
          color: filtered ? "#1890ff" : undefined
        }}
      />
    ),
    onFilter: (value, record) =>
      record["num"]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.select());
      }
    },
    render: (text, record) => (
      <div>
        <Tag color="green"> {record.num} </Tag>
      </div>
    )
  },
  {
    title: "Description",
    dataIndex: "description",
    key: "description",
    ...getColumnSearchProps("description")
  },

  {
    title: "Amount",
    dataIndex: "amount",
    key: "amount",
    render: (text, record) => <span> {record.amount.toFixed(2)} </span>
  },
  {
    title: "Expense Type",
    dataIndex: "expenseType",
    key: "expenseType",
    render: (text, record) => <span> {record.expenseType.name} </span>
  },
  {
    title: "Data Issued",
    dataIndex: "created_at",
    key: "created_at",
    render: (text, record) => (
      <span> {new Date(record.created_at).toLocaleDateString()} </span>
    )
  },

  {
    title: "Action",
    dataIndex: "action",
    key: "action",
    render: (text, record) => (
      <span>
        <Button
          shape="circle"
          size="small"
          icon="edit"
          onClick={showEditExpensesModal.bind(this, props, record)}
        />{" "}
        <Divider type="vertical" />
        <Button
          shape="circle"
          size="small"
          icon="delete"
          loading={props.expenses[`removeIconLoading${record.id}`]}
          onClick={props.remove_expenses.bind(this, record.id)}
        />{" "}
      </span>
    )
  }
];

const handleSearch = (selectedKeys, confirm) => {
  confirm();

  searchText = selectedKeys[0];
};

const handleReset = clearFilters => {
  clearFilters();
  searchText = "";
};

const showEditExpensesModal = (props, data) => {
  props.update_expenses_edit_details({
    expenseId: data.id,
    expenseNum: data.num,
    expenseAmount: data.amount,
    expenseType: data.expenseType.name,
    expenseDescription: data.description
  });
  props.update_add_expenses_visible(true);
  props.update_expenses_action(true);
};

const AppExpensesTable = props => {
  const { expensesType } = props;
  useEffect(() => {
    if (!expensesType.loading && !expensesType.datasource) {
      props.fetch_expensesType();
    }
  });
  const { loading, datasource } = props.expenses;
  let columns = generateColumns(props);
  return (
    <Table
      bordered
      size="small"
      loading={loading}
      title={() => (
        <Row>
          <Col span={20} style={{ textAlign: "left" }}>
            <h1>Expenses</h1>
          </Col>{" "}
          <Col span={4}>
            <Button
              type="dashed"
              onClick={() => {
                props.update_add_expenses_visible(true);
                props.update_expenses_action(false);
                props.update_expenses_edit_details({
                  expenseId: "",
                  expenseNum: "",
                  expenseAmount: "",
                  expenseType: "",
                  expenseDescription: ""
                });
              }}
            >
              Add Expenses
            </Button>
          </Col>
        </Row>
      )}
      columns={columns}
      dataSource={datasource}
    />
  );
};

const mapStateToProps = state => {
  return {
    expenses: state.expenses,
    expensesType: state.expensesType
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetch_expensesType: () => dispatch(fetch_expensesType),
    update_add_expenses_visible: value =>
      dispatch(update_add_expenses_visible(value)),
    remove_expenses: value => dispatch(remove_expenses(dispatch, value)),
    update_expenses_edit_datasource: value =>
      dispatch(update_expenses_edit_details(value)),
    update_expenses_action: value => dispatch(update_expenses_action(value)),
    update_expenses_edit_details: value =>
      dispatch(update_expenses_edit_details(value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppExpensesTable);
