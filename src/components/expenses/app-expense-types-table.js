import React from "react";
import { Table, Row, Col, Button, Input, Icon, Divider } from "antd";
import { connect } from "react-redux";
import Highlighter from "react-highlight-words";
import {
  fetch_expensesType,
  update_add_expenses_type_visible,
  update_expense_type_button_loading,
  update_expense_type_action,
  update_expense_type_edit_details,
  remove_expenseType
} from "../../redux/actions/expensesTypeActions";

class AppExpenseTypeTable extends React.Component {
  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div
        style={{
          padding: 8
        }}
      >
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{
            width: 188,
            marginBottom: 8,
            display: "block"
          }}
        />{" "}
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{
            width: 90,
            marginRight: 8
          }}
        >
          Search{" "}
        </Button>{" "}
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{
            width: 90
          }}
        >
          Reset{" "}
        </Button>{" "}
      </div>
    ),
    filterIcon: filtered => (
      <Icon
        type="search"
        style={{
          color: filtered ? "#1890ff" : undefined
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{
          backgroundColor: "#ffc069",
          padding: 0
        }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
    )
  });

  state = {
    searchText: "",
    columns: [
      {
        title: "Name",
        dataIndex: "name",
        key: "name",
        width: "20%",
        ...this.getColumnSearchProps("name")
      },
      {
        title: "description",
        dataIndex: "description",
        key: "description",
        ...this.getColumnSearchProps("description")
      },
      {
        title: "Added",
        dataIndex: "created_at",
        key: "created_at",
        
        render: (text, record) =>
          new Date(record.created_at).toLocaleDateString()
      },
      {
        title: "Action",
        dataIndex: "action",
        key: "action",
        render: (text, record) => (
          <span>
            <Button
              shape="circle"
              size="small"
              icon="edit"
              onClick={this.showEditExpenseTypeDetail.bind(this, record)}
            />{" "}
            <Divider type="vertical" />
            <Button
              shape="circle"
              size="small"
              icon="delete"
              loading={this.props.expensesType[`removeIconLoading${record.id}`]}
              onClick={this.props.remove_expense_type.bind(this, record.id)}
            />{" "}
          </span>
        )
      }
    ]
  };

  handleExpenseTypeAdd = event => {
    this.props.update_creditor_edit_details({
      creditorId: "",
      creditorName: "",
      creditorContact: "",
      creditorDescription: ""
    });
    this.props.update_add_creditor_visible(true);
    this.props.update_creditor_action(false);
  };

  showEditExpenseTypeDetail = data => {
    this.props.update_expense_type_edit_details({
      expenseTypeId: data.id,
      expenseTypeName: data.name,
      expenseTypeDescription: data.description
    });
    this.props.update_add_expense_type_visible(true);
    this.props.update_expense_type_action(true);
  };

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0]
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({
      searchText: ""
    });
  };

  componentDidMount() {
    if (
      !this.props.expensesType.loading &&
      !this.props.expensesType.datasource
    ) {
      this.props.fetch_expenseType();
    }
  }
  render() {
    const { loading, datasource } = this.props.expensesType;
    return (
      <Table
        bordered
        size="small"
        loading={loading}
        title={() => (
          <Row>
            <Col
              span={20}
              style={{
                textAlign: "left"
              }}
            >
              <h1> Expense Types</h1>{" "}
            </Col>{" "}
            <Col span={4}>
              <Button
                type="dashed"
                onClick={() => {
                  this.props.update_add_expense_type_visible(true);
                  this.props.update_expense_type_action(false);
                  this.props.update_expense_type_edit_details({
                    expenseTypeId: "",
                    expenseTypeName: "",
                    expenseTypeDescription: ""
                  });
                }}
              >
                Add Expense Type{" "}
              </Button>{" "}
            </Col>{" "}
          </Row>
        )}
        columns={this.state.columns}
        dataSource={datasource}
      ></Table>
    );
  }
}

const mapStateToProps = state => {
  return {
    expensesType: state.expensesType
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetch_expenseType: () => dispatch(fetch_expensesType),
    update_add_expense_type_visible: value =>
      dispatch(update_add_expenses_type_visible(value)),
    update_expense_type_button_loading: value =>
      dispatch(update_expense_type_button_loading(value)),
    update_expense_type_action: value =>
      dispatch(update_expense_type_action(value)),
    update_expense_type_edit_details: value =>
      dispatch(update_expense_type_edit_details(value)),
    remove_expense_type: expenseId =>
      dispatch(remove_expenseType(dispatch, expenseId))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppExpenseTypeTable);
