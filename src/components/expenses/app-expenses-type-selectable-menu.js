import React from "react";
import { connect } from "react-redux";
import { Menu, Icon } from "antd";

const AppExpensesTypeSelectableMenu = props => {
  const generate = expensesTypes => {
    return expensesTypes
      ? expensesTypes.map(expensesType => {
          let MenuJSX = null;
          MenuJSX = (
            <Menu.Item
              key={expensesType.id}
              onClick={props.onClick.bind(this, expensesType)}
              style={{ textAlign: "justify" }}
            >
              <span className="nav-text"> {expensesType.name} </span>
              {props.expensesType[`loading${expensesType.name}Expenses`] && (
                <Icon type="loading" />
              )}
            </Menu.Item>
          );
          return MenuJSX;
        })
      : [];
  };

  return (
    <div style={{ textTransform: "capitalize" }}>
      <h3>
        Expense Types{" "}
        {props.expensesType.loading ? <Icon type="loading" /> : ""}{" "}
      </h3>{" "}
      <Menu mode="inline">
        <Menu.Item
          key={0}
          onClick={props.onClick.bind(this, "")}
          style={{ textAlign: "justify" }}
        >
          <span className="nav-text"> All </span>
          {props.expensesType[`loading${""}Expenses`] && (
            <Icon type="loading" />
          )}
        </Menu.Item>
        {generate(props.expensesType.datasource)}
      </Menu>{" "}
    </div>
  );
};

const mapStateToProps = state => {
  return {
    expensesType: state.expensesType
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppExpensesTypeSelectableMenu);
