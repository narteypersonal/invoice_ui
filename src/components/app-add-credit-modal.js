import React from "react";
import { Modal } from "antd";
import { connect } from "react-redux";

import { update_add_credit_visible } from "../redux/actions/creditsActions";
import AppAddCredit from "./creditors/app-add-credit";

const AppAddCreditModal = props => {
  return (
    <Modal
      title="Make Transaction"
      visible={props.credits.addCreditVisible}
      onCancel={props.update_add_credit_visible.bind(this, false)}
      footer={false}
    >
      <AppAddCredit />
    </Modal>
  );
};

const mapStateToProps = state => {
  return {
    credits: state.credits
  };
};

const mapDispatchToProps = dispatch => {
  return {
    update_add_credit_visible: value =>
      dispatch(update_add_credit_visible(value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppAddCreditModal);
