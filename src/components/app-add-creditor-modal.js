import React from "react";
import { Modal } from "antd";
import AppAddExpenses from "./expenses/app-add-expenses";
import { connect } from "react-redux";
import { update_add_creditor_visible } from "../redux/actions/creditorActions";
import AppAddCreditor from "./creditors/app-add-creditor";

const AppAddCreditorModal = props => {
  return (
    <Modal
      title={
        props.creditors.creditorUpdateAction ? "Edit Creditor" : "Add Creditor"
      }
      visible={props.creditors.addCreditorVisible}
      onCancel={props.update_add_creditor_visible.bind(this, false)}
      footer={false}
    >
      <AppAddCreditor />
    </Modal>
  );
};

const mapStateToProps = state => {
  return {
    creditors: state.creditors
  };
};

const mapDispatchToProps = dispatch => {
  return {
    update_add_creditor_visible: value =>
      dispatch(update_add_creditor_visible(value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppAddCreditorModal);
