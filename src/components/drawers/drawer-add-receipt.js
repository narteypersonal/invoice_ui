import React from "react";
import { Drawer } from "antd";
import { connect } from "react-redux";
import AddReceiptForm from "../receipts/app-add-receipt";
import { toggle_add_receipt_drawer_visible } from "../../redux/actions/receiptActions";

const DrawerAddReceipt = props => {
  return (
    <Drawer
      title={`Generate Receipt`}
      placement="right"
      width={600}
      closable={true}
      onClose={props.toggle_add_receipt_drawer_visible.bind(this, false)}
      visible={props.receipt.addReceiptDrawerVisible}
    >
      <AddReceiptForm />
    </Drawer>
  );
};

const mapPropsToState = state => {
  return {
    receipt: state.receipt
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggle_add_receipt_drawer_visible: value =>
      dispatch(toggle_add_receipt_drawer_visible(value))
  };
};
export default connect(
  mapPropsToState,
  mapDispatchToProps
)(DrawerAddReceipt);
