import React from "react";
import { Drawer } from "antd";
import { connect } from "react-redux";
import { update_stock_drawer_visible } from "../../redux/actions/stockActions";
import AppStockForm from "../stocks/app-stock-form";

const DrawerAddStock = props => {
    return (
        <Drawer
            title={`Add stock`}
            placement="right"
            width={600}
            closable={true}
            onClose={props.update_stock_drawer_visible.bind(this, false)}
            visible={props.stock.stockDrawerVisible}
        >
            <AppStockForm />
        </Drawer>
    );
};

const mapPropsToState = state => {
    return {
        stock: state.stock
    };
};

const mapDispatchToProps = dispatch => {
    return {
        update_stock_drawer_visible: value =>
            dispatch(update_stock_drawer_visible(value))
    };
};
export default connect(
    mapPropsToState,
    mapDispatchToProps
)(DrawerAddStock);
