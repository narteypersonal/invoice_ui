import React, { useState } from "react";
import { Drawer, Button } from "antd";
import { toggle_invoice_drawer_visible } from "../../redux/actions/invoiceActions";
import { connect } from "react-redux";
import AppInvoiceDetails from "../invoice/app-invoice-details";
import AppReceipts from "../receipts/app-receipts";

const DrawerInvoiceReceipts = props => {
  const [visible, changeVisibility] = useState(false);
  return (
    <Drawer
      title={`Invoice Details`}
      placement="right"
      width={600}
      closable={true}
      onClose={props.toggle_invoice_drawer_visible.bind(this, false)}
      visible={props.invoice.invoiceDrawerVisible}
    >
      <Button
        type="primary"
        style={{ marginBottom: "3vh" }}
        onClick={changeVisibility.bind(this, true)}
      >
        View invoice reciepts
      </Button>
      <AppInvoiceDetails />
      <Drawer
        title={`Invoice Reciept History `}
        placement="right"
        width={820}
        closable={true}
        onClose={changeVisibility.bind(this, false)}
        visible={visible}
      >
        <AppReceipts
          datasource={props.invoice.invoiceReceipts}
          loading={false}
        />
      </Drawer>
    </Drawer>
  );
};

const mapPropsToState = state => {
  return {
    invoice: state.invoice
  };
};

const maptPropsToDispatch = dispatch => {
  return {
    toggle_invoice_drawer_visible: value =>
      dispatch(toggle_invoice_drawer_visible(value))
  };
};
export default connect(
  mapPropsToState,
  maptPropsToDispatch
)(DrawerInvoiceReceipts);
