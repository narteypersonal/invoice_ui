import React from "react";
import { Drawer } from "antd";
import { connect } from "react-redux";
import { toggle_debtor_detail_drawer_visible } from "../../redux/actions/debtorsJournalActions";
import AppInvoices from "../invoices/app-invoices";

const DrawerDebtorInvoices = props => {
  return (
    <Drawer
      title={`Debtor Invoices`}
      placement="right"
      width={850}
      closable={true}
      onClose={props.toggle_debtor_detail_drawer_visible.bind(this, false)}
      visible={props.debtorsJournals.debtorDrawerVisible}
    >
      <AppInvoices datasource={props.debtorsJournals.debtorDetailDataSource} />
    </Drawer>
  );
};

const mapPropsToState = state => {
  return {
    invoice: state.invoice,
    debtorsJournals: state.debtorsJournals
  };
};

const maptPropsToDispatch = dispatch => {
  return {
    toggle_debtor_detail_drawer_visible: value =>
      dispatch(toggle_debtor_detail_drawer_visible(value))
  };
};
export default connect(
  mapPropsToState,
  maptPropsToDispatch
)(DrawerDebtorInvoices);
