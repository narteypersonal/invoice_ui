import React from "react";
import { Drawer } from "antd";
import { connect } from "react-redux";
import { update_purchase_invoice_drawer_visible } from "../../redux/actions/purchaseActions";
import AppPurchaseInvoiceForm from "../purchases/app-purchase-invoice-form";

const DrawerAddPurchaseInvoice = props => {
  return (
    <Drawer
      title={`Generate Purchase Invoice`}
      placement="right"
      width={600}
      closable={true}
      onClose={props.update_purchase_invoice_drawer_visible.bind(this, false)}
      visible={props.purchases.purchasesDrawerVisible}
    >
      <AppPurchaseInvoiceForm />
    </Drawer>
  );
};

const mapPropsToState = state => {
  return {
    purchases: state.purchases
  };
};

const mapDispatchToProps = dispatch => {
  return {
    update_purchase_invoice_drawer_visible: value =>
      dispatch(update_purchase_invoice_drawer_visible(value))
  };
};
export default connect(
  mapPropsToState,
  mapDispatchToProps
)(DrawerAddPurchaseInvoice);
