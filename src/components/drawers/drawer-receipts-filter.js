import React from "react";
import { Drawer, Col, Slider, Row, Select } from "antd";
import AppDaterangePicker from "../app-daterange-picker";
import { connect } from "react-redux";
import { toggle_receipt_filter_drawer_visible } from "../../redux/actions/receiptActions";

const DrawerReceiptFilter = props => {
  return (
    <Drawer
      title={`Search Filter`}
      placement="right"
      width={600}
      closable={true}
      onClose={props.toggle_receipt_filter_drawer_visible.bind(this, false)}
      visible={props.receipt.receiptFilterDrawerVisible}
    >
      <Row>
        <Col style={{ paddingTop: "15px" }}> Payment Range </Col>{" "}
        <Col>
          <Slider max={1000} range step={10} defaultValue={[20, 50]} />{" "}
        </Col>
      </Row>
      <Row style={{ paddingTop: "15px" }}>
        <Col span={24}>Receipt type:</Col>
        <Col span={24}>
          <Select
            placeholder="Receipt type"
            size="large"
            name="receipttype"
            style={{ width: "100%" }}
            loading={props.receiptType.loading}
          >
            {props.receiptType.datasource &&
              props.receiptType.datasource.map(type => (
                <Select.Option key={type.id} value={type.name}>
                  {type.name}
                </Select.Option>
              ))}
          </Select>
        </Col>
      </Row>
      <Row>
        <Col style={{ paddingTop: "15px" }}> Date Issued </Col>
        <AppDaterangePicker />
      </Row>
    </Drawer>
  );
};

const mapStateToProps = state => {
  return {
    receipt: state.receipt,
    receiptType: state.receiptType
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggle_receipt_filter_drawer_visible: value =>
      dispatch(toggle_receipt_filter_drawer_visible(value))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DrawerReceiptFilter);
