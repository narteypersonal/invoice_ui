import React, { useState } from "react";
import { Drawer } from "antd";
import { connect } from "react-redux";
import AppGenerateInvoiceForm from "../invoice/app-generate-invoice-form";
import {
    update_invoice_modal_visible,
    update_invoice_datasource
} from "../../redux/actions/invoiceActions";
import {
    fetch_products
} from "../../redux/actions/productActions";

const DrawerGenerateInvoice = props => {
    
    return (
        <Drawer
            title={`Generate Invoice`}
            placement="right"
            width={600}
            closable={true}
            onClose={() => props.update_invoice_modal_visible(false)}
            visible={props.invoice.invoiceModalVisible}
        >
           <AppGenerateInvoiceForm />
        </Drawer>
    );
};

const mapPropsToState = state => {
    return {
        invoice: state.invoice
    };
};

const maptPropsToDispatch = dispatch => {
    return {
        update_invoice_modal_visible: value =>
            dispatch(update_invoice_modal_visible(value)),
        update_invoice_datasource: value =>
            dispatch(update_invoice_datasource(value)),
        fetch_products: () => dispatch(fetch_products(dispatch)),
    };
};
export default connect(
    mapPropsToState,
    maptPropsToDispatch
)(DrawerGenerateInvoice);
