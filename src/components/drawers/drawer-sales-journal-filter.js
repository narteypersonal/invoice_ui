import React from "react";
import { Drawer, Col, Slider } from "antd";
import AppDaterangePicker from "../app-daterange-picker";
import { connect } from "react-redux";
import { toggle_sales_journal_filter_drawer_visible } from "../../redux/actions/salesJournalAction";

const DrawerSalesJournalFilter = props => {
  return (
    <Drawer
      title={`Search Filter`}
      placement="right"
      width={600}
      closable={true}
      onClose={props.toggle_sales_journal_filter_drawer_visible.bind(
        this,
        false
      )}
      visible={props.salesJournals.salesJournalFilterDrawerVisible}
    >
      <Col> Sales Range </Col>{" "}
      <Col>
        <Slider max={1000} range step={10} defaultValue={[20, 50]} />{" "}
      </Col>{" "}
      <Col> Date Range </Col> <AppDaterangePicker />
    </Drawer>
  );
};

const mapStateToProps = state => {
  return {
    salesJournals: state.salesJournals
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggle_sales_journal_filter_drawer_visible: value =>
      dispatch(toggle_sales_journal_filter_drawer_visible(value))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DrawerSalesJournalFilter);
