import React from "react";
import {
    Form,
    Row,
    Input,
    Col,
    Button,
    InputNumber,
    message,
    Select,
    Descriptions,
    Icon
  } from "antd";

import { connect } from "react-redux";
import { MakeGraphQLQuery } from "../../services/utils";
import { PostDataService } from "../../services/post-data-service";
import { BASEAPIURL } from "../../env";
import { update_purchase_invoice_drawer_visible, update_purchase_invoice_total_cost, update_purchase_invoice_total_items, update_purchase_invoice_total_quantity } from "../../redux/actions/purchaseActions";

const formItemLayoutWithOutLabel = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0
    }
  }
};

function hasErrors(fieldsError, fieldsKeys = []) {
  // console.log(fieldsError);
  return fieldsKeys.some(field => fieldsError[field]);
}

  /**
     * Adds a new product,quantity,price row dynamically
     */
   function add(props) {
      let { form } = props;
      // can use data-binding to get
      let keys = form.getFieldValue("keys");
      let nextKeys = keys.concat(keys.length);
      
      // important! notify form to detect changes

      form.setFieldsValue(
        {
          keys: nextKeys
        }
      );

      console.log(nextKeys);
    }

function calculateNewTotal(form) {
  return form
        .getFieldValue("prices")
        .reduce(
          (accumulator, price) => accumulator + Number.parseFloat(price),
          0
        );
}

function calculateTotalQuantity(allQuantities){
  let totalQuantity = 0;
  for(let index= 0; index < allQuantities.length; index++){
    totalQuantity += parseFloat(allQuantities[index]);
  }
  return totalQuantity;
}
function calculateTotalCost(allPrices,allQuantities){
  let totalCost = 0;
    for(let index= 0; index < allPrices.length; index++){
      totalCost += (allPrices[index] && parseFloat(allPrices[index]) && allQuantities[index] && parseFloat(allQuantities[index]))? parseFloat(allPrices[index]) * parseFloat(allQuantities[index]) : 0;
    }
  return totalCost;
}

const AppAddPurchaseInvoice = Form.create({ name: "app_add_purchase_invoice",
onValuesChange(_, fieldObject, allValues) {
  _.form.validateFields();
  if(fieldObject.quantity || fieldObject.prices){
  
    for (let key of allValues.keys){
      //check if all fields are filled for each row
      if(allValues.products[key] && allValues.quantity[key] && allValues.prices[key] && (key === (allValues.keys.length - 1))){
        add(_);
      }
    }
  }
  _.update_purchase_invoice_total_quantity(calculateTotalQuantity(allValues.quantity));
  _.update_purchase_invoice_total_cost(calculateTotalCost(allValues.prices,allValues.quantity));
   
}
 })(
    class extends React.Component {
      state = { 
        submitting: false,
        items_count: 0,
        quantity_count: 0,
        total_amount: 0 
      };

      
       
      /**
     * Removes a product,quantity,price row 
     */
    remove = k => {
      const { form } = this.props;
      // can use data-binding to get
      const keys = form.getFieldValue("keys");
      
      // We need at least one passenger
      if (keys.length === 1) {
        return;
      }

      // can use data-binding to set
      form.setFieldsValue(
        {
          keys: keys.filter(key => key !== k)
        },
        ()=>{
          let allQuantities = form.getFieldValue("quantity");
          let allPrices = form.getFieldValue("prices");
          
          this.props.update_purchase_invoice_total_cost(calculateTotalCost(allPrices, allQuantities))
          this.props.update_purchase_invoice_total_quantity(calculateTotalQuantity(allQuantities))
        }
      );
      
      
      // calculateTotalQuantity()
    }
    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            this.setState({ submitting: true });
  
            let data = {};
            let products = [];
            data["creditor_id"] = values.creditor_id;
            data["description"] = values.description;
            data["amount"] = 0;
            for(let key of values.keys){
              let product = values.products[key];
              let price = values.prices[key] ? parseFloat(values.prices[key]) : 0;
              let quantity = values.quantity[key] ? parseFloat(values.quantity[key]):0;
              if( product && price && quantity){
                data["amount"] += quantity * price;
                products.push({
                  product,price,quantity
                })
              }
            }

            data["products"] = products;

            console.log(data);
            return;
            
  
            let queryName = "createPurchaseInvoice";
            let query = MakeGraphQLQuery("mutation", queryName, data, [
              "id",
              "number",
              "debit",
              "credit",
              "description",
              "created_at",
              "updated_at"
            ]);
  
            PostDataService(BASEAPIURL, query, {
              success: data => {
                if (data.data[queryName]) {
                  data = data.data[queryName];
                  data["key"] = data.id;
  
                  message.success(`Purchase invoice generated`);
                  this.props.form.resetFields();
                } else if (data.errors) {
                  message.error(data.errors[0].message);
                }
              },
              always: () => {
                this.setState({ submitting: false });
              }
            });
          }
        });
      };
      render() {
        const {
          getFieldDecorator,
          getFieldValue,
          getFieldsError,
          getFieldError,
          isFieldTouched
        } = this.props.form;
        getFieldDecorator("keys", {
          initialValue: [0]
        });
        getFieldDecorator("total_amount", {
          initialValue: 0,
        });
        const keys = getFieldValue("keys");
        const formItems = keys.map((k, index) => {
          let productNameError =
            isFieldTouched(`products[${k}]`) && getFieldError(`products[${k}]`);
          let qtyNameError =
            isFieldTouched(`quantity[${k}]`) && getFieldError(`quantity[${k}]`);
          let priceError =
            isFieldTouched(`prices[${k}]`) && getFieldError(`prices[${k}]`);
  
          return (
            <Row key={`productsRow${k}`}>
              <Col span={12}>
                <Form.Item
                  {...formItemLayoutWithOutLabel}
                  validateStatus={productNameError ? "error" : ""}
                  help={productNameError || ""}
                  required={false}
                  key={k}
                >
                  {getFieldDecorator(`products[${k}]`, {
                    rules: [
                      {
                        required: true,
                        message: "Please select product!"
                      }
                    ]
                  })(
                    <Input
                      placeholder="product name"
                      type="text"
                      style={{
                        width: "100%",
                        marginRight: 8
                      }}
                      // onChange={this.handleProductsChange.bind(this)}
                      name="products[]"
                    />
                     
                  )}
                </Form.Item>
              </Col>

              <Col span={4}>
                <Form.Item
                  key={`prices[${k}]`}
                  validateStatus={priceError ? "error" : ""}
                  help={priceError || ""}
                  required={false}
                >
                  {getFieldDecorator(`prices[${k}]`, {
                    rules: [
                      {
                        message: "Invalid product price!",
                        type: "number"
                      }
                    ]
                  })(
                    <InputNumber
                      placeholder="unit_price"
                      min={1}
                      style={{
                        width: "100%",
                        marginRight: 8
                      }}
                      name={`prices[]`}
                    />
                  )}
                </Form.Item>
              </Col>

              <Col span={6}>
                <Form.Item
                  key={`qty${k}`}
                  validateStatus={qtyNameError ? "error" : ""}
                  help={qtyNameError || ""}
                  required={false}
                >
                  {getFieldDecorator(`quantity[${k}]`, {
                    rules: [
                      {
                        required: true,
                        message: "Please input product quantity!"
                      }
                    ],
                    initialValue: 1
                  })(
                    <Input
                      placeholder="quantity"
                      type="number"
                      min={1}
                      style={{
                        width: "100%",
                        marginRight: 8
                      }}
                      name="quantity[]"
                    />
                  )}
                </Form.Item>
              </Col>

              <Col span={2} style={{ padding: 10 }}>
                {keys.length > 1 ? (
                  <Icon
                    className="dynamic-delete-button"
                    type="minus-circle-o"
                    onClick={() => this.remove(k)}
                  />
                ) : null}
              </Col>
            </Row>
          );
        });
        return (
          <Form layout="vertical" onSubmit={this.handleSubmit}>
            <Row>
              <Col>
                <Form.Item label="Creditor/Supplier">
                  {" "}
                  {getFieldDecorator("creditor_id", {
                    rules: [
                      {
                        required: true,
                        message: "Select a creditor"
                      }
                    ]
                  })(
                    <Select
                      placeholder="creditor name"
                      type="text"
                      style={{
                        width: "100%",
                        marginRight: 8
                      }}
                      // onChange={this.handleProductsChange.bind(this)}
                      name="creditor_id"
                    >
                      {this.props.creditors.datasource &&
                        this.props.creditors.datasource.map((creditor, index) => {
                          return (
                            <Select.Option value={creditor.id} key={creditor.id}>
                              {creditor.name}
                            </Select.Option>
                          );
                        })}
                    </Select>
                  )}{" "}
                </Form.Item>
              </Col>
              <Col>
                <Form.Item label="description">
                  {" "}
                  {getFieldDecorator("description", {
                    rules: [{ required: true }]
                  })(<Input name="description" type="textarea" />)}{" "}
                </Form.Item>
              </Col>
            </Row>
            
            <Row>
              <Descriptions size="small" column={3}>
                  <Descriptions.Item label="No Items">{this.props.purchases.totalItems}</Descriptions.Item>
                  <Descriptions.Item label="Cost" type="warning" >{this.props.purchases.totalCost}</Descriptions.Item>
                  <Descriptions.Item label="Qty">{this.props.purchases.totalQuantity}</Descriptions.Item>
                  
              </Descriptions>
            </Row>
            <Row style={{maxHeight: "40vh", backgroundColor: "#ccc",overflowY: "scroll", paddingTop:"10px", paddingLeft: "5px"}}>
              {formItems}
              <Form.Item {...formItemLayoutWithOutLabel}>
                  <Button
                    
                    type="dashed"
                    onClick={add.bind(this,this.props)}
                    style={{
                      width: "60%"
                    }}
                  >
                    <Icon type="plus" /> Add field{" "}
                  </Button>
                </Form.Item>
            </Row>
            <Row style={{marginTop: "10px"}}>
              <Col span={12}>
                <Form.Item>
                  <Button
                    type="default"
                    htmlType="reset"
                    onClick={this.props.update_purchase_invoice_drawer_visible.bind(
                      this,
                      false
                    )}
                  >
                    Cancel
                  </Button>
                </Form.Item>
              </Col>
              <Col span={12} style={{ textAlign: "right" }}>
                <Form.Item>
                  <Button
                    
                    type="primary"
                    htmlType="submit"
                    loading={this.state.submitting}
                  >
                    Submit
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        );
      }
    }
  );

const mapStateToProps = state => {
    return {
      creditors: state.creditors,
      purchases: state.purchases
    };
  };
  
  const mapDispatchToProps = dispatch => {
   return {
       update_purchase_invoice_drawer_visible: (value) => dispatch(update_purchase_invoice_drawer_visible(value)),
       update_purchase_invoice_total_cost: (value) => dispatch(update_purchase_invoice_total_cost(value)),
       update_purchase_invoice_total_items: (value) => dispatch(update_purchase_invoice_total_items(value)),
       update_purchase_invoice_total_quantity: (value) => dispatch(update_purchase_invoice_total_quantity(value))
      }
  };
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(AppAddPurchaseInvoice);
