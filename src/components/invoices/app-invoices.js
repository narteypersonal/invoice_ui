import React, { Component } from "react";
import {
    Table,
    Tag,
    Button,
    Row,
    Col,
    Input,
    Icon
} from "antd";

import Highlighter from "react-highlight-words";
import { connect } from "react-redux";
import { show_invoice_detail } from "../../redux/actions/invoiceActions";


class Invoices extends Component {
    getColumnSearchProps = dataIndex => ({
        filterDropdown: ({
            setSelectedKeys,
            selectedKeys,
            confirm,
            clearFilters
        }) => (
                <div
                    style={{
                        padding: 8
                    }}
                >
                    <Input
                        ref={node => {
                            this.searchInput = node;
                        }}
                        placeholder={`Search ${dataIndex}`}
                        value={selectedKeys[0]}
                        onChange={e =>
                            setSelectedKeys(e.target.value ? [e.target.value] : [])
                        }
                        onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
                        style={{
                            width: 188,
                            marginBottom: 8,
                            display: "block"
                        }}
                    />{" "}
                    <Button
                        type="primary"
                        onClick={() => this.handleSearch(selectedKeys, confirm)}
                        icon="search"
                        size="small"
                        style={{
                            width: 90,
                            marginRight: 8
                        }}
                    >
                        Search{" "}
                    </Button>{" "}
                    <Button
                        onClick={() => this.handleReset(clearFilters)}
                        size="small"
                        style={{
                            width: 90
                        }}
                    >
                        Reset{" "}
                    </Button>{" "}
                </div>
            ),
        filterIcon: filtered => (
            <Icon
                type="search"
                style={{
                    color: filtered ? "#1890ff" : undefined
                }}
            />
        ),
        onFilter: (value, record) =>
            record[dataIndex]
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select());
            }
        },
        render: text => (
            <Highlighter
                highlightStyle={{
                    backgroundColor: "#ffc069",
                    padding: 0
                }}
                searchWords={[this.state.searchText]}
                autoEscape
                textToHighlight={text.toString()}
            />
        )
    });

    state = {
        searchText: "",
        columns: [
            {
                title: "Invoice Number",
                dataIndex: "num",
                key: "num",
                filterDropdown: ({
                    setSelectedKeys,
                    selectedKeys,
                    confirm,
                    clearFilters
                }) => (
                        <div
                            style={{
                                padding: 8
                            }}
                        >
                            <Input
                                ref={node => {
                                    this.searchInput = node;
                                }}
                                placeholder={`Search Invoice Number`}
                                value={selectedKeys[0]}
                                onChange={e =>
                                    setSelectedKeys(e.target.value ? [e.target.value] : [])
                                }
                                onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
                                style={{
                                    width: 188,
                                    marginBottom: 8,
                                    display: "block"
                                }}
                            />{" "}
                            <Button
                                type="primary"
                                onClick={() => this.handleSearch(selectedKeys, confirm)}
                                icon="search"
                                size="small"
                                style={{
                                    width: 90,
                                    marginRight: 8
                                }}
                            >
                                Search{" "}
                            </Button>{" "}
                            <Button
                                onClick={() => this.handleReset(clearFilters)}
                                size="small"
                                style={{
                                    width: 90
                                }}
                            >
                                Reset{" "}
                            </Button>{" "}
                        </div>
                    ),
                filterIcon: filtered => (
                    <Icon
                        type="search"
                        style={{
                            color: filtered ? "#1890ff" : undefined
                        }}
                    />
                ),
                onFilter: (value, record) =>
                    record["invoice"]["num"]
                        .toString()
                        .toLowerCase()
                        .includes(value.toLowerCase()),
                onFilterDropdownVisibleChange: visible => {
                    if (visible) {
                        setTimeout(() => this.searchInput.select());
                    }
                },
                render: (text, record) => (
                    <div>
                        {" "}
                        {record.is_void ? (
                            <Tag color="magenta"> {record.num} </Tag>
                        ) : (
                                <Tag color="green"> {record.num} </Tag>
                            )}{" "}
                    </div>
                )
            },
            {
                title: "Total",
                dataIndex: "net_total",
                key: "net_total",
                width: "20%",
                ...this.getColumnSearchProps("net_total")
            },

            {
                title: "Recieved",
                dataIndex: "amount_recieved",
                key: "amount_recieved",
                render: (text, record) => <span> {record.amount_recieved.toFixed(2)} </span>
            },
            {
                title: "Balance",
                dataIndex: "balance",
                key: "balance",
                render: (text, record) => (
                    <div>
                        {" "}
                        {record.balance > 0 ? (
                            <b
                                style={{
                                    color: "#f50"
                                }}
                            >
                                {" "}
                                {record.balance.toFixed(2)}{" "}
                            </b>
                        ) : (
                                <span> {record.balance.toFixed(2)} </span>
                            )}{" "}
                    </div>
                )
            },
            {
                title: "Validity",
                dataIndex: "is_void",
                key: "is_void",
                render: (text, record) => (
                    <div>
                        {" "}
                        {record.is_void ? (
                            <Tag color="magenta"> Invalidated </Tag>
                        ) : (
                                <Tag color="green"> Valid </Tag>
                            )}{" "}
                    </div>
                )
            },
            {
                title: "Data Issued",
                dataIndex: "created_at",
                key: "created_at",
                render: (text, record) => (
                    <span> {new Date(record.created_at).toLocaleDateString()} </span>
                )
            }, {
                title: "Action",
                dataIndex: "action",
                key: "action",
                render: (text, record) => (
                    <span>
                        <Button
                            shape="circle"
                            size="small"
                            type="primary"
                            icon="info-circle"
                            loading={
                                this.props.invoice[`invoiceDetailButton${record.id}IsLoading`]
                            }
                            onClick={this.props.show_invoice_detail.bind(this, record)}
                        />{" "}
                        
                    </span>
                )
            }

            
        ]
    };


    handleSearch = (selectedKeys, confirm) => {
        confirm();
        this.setState({
            searchText: selectedKeys[0]
        });
    };

    handleReset = clearFilters => {
        clearFilters();
        this.setState({
            searchText: ""
        });
    };

    render() {
        let { loading, datasource } = this.props;
        
        return (
                <Table
                    bordered
                    size="small"
                    loading={loading}
                   
                    columns={this.state.columns}
                    dataSource={datasource}
                />
            
        );
    }
}

const mapStateToProps = state =>{
    return {
        invoice: state.invoice
    }
}

const mapDispatchToProps = dispatch => {
    return {
        show_invoice_detail: value => dispatch(show_invoice_detail(dispatch,value))
    }
}

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(Invoices);
