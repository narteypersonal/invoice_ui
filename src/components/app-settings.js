import React, { Component } from "react";
import { Row, Col, Card } from "antd";
import AppCompanyForm from "./app-company-form";
import AppInvoiceSettings from "./app-invoice-settings";
import { PostDataService } from "../services/post-data-service";
import { MakeGraphQLQuery } from "../services/utils";
import { BASEAPIURL } from "../env";
import { connect } from "react-redux";
class AppSettings extends Component {
  state = {
    invoicePrefix: "",
    invoiceIsLoading: true,
    companyIsLoading: true,
    companyDetails: {}
  };
  componentDidMount() {
    PostDataService(
      BASEAPIURL,
      MakeGraphQLQuery("query", "invoiceNumber", [], ["prefix"]),
      {
        success: data => {
          if (data.data["invoiceNumber"]) {
            this.setState({
              invoicePrefix: data.data["invoiceNumber"]["prefix"]
            });
          }
        },
        always: () => {
          this.setState({
            invoiceIsLoading: false
          });
        }
      }
    );
  }
  render() {
    return (
      <Row>
        <Col
          xs={{
            span: 24
          }}
          md={{
            span: 12
          }}
        >
          <Card title="Company Details">
            <AppCompanyForm
              companyDetails={this.state.companyDetails}
              companyIsLoading={this.state.companyIsLoading}
              handleDefaultImage={() => {
                // console.log(this.state.companyDetails);
                if (this.props.settings.logo_url)
                  this.setState({
                    fileList: [
                      {
                        status: "done",
                        url: this.props.settings.details.logo_url
                      }
                    ]
                  });
              }}
              triggerCompanyLoading={(loading = true) =>
                this.setState({
                  companyIsLoading: loading
                })
              }
              triggerCompanyDetails={(data = {}) =>
                this.setState({
                  companyDetails: data
                })
              }
            />{" "}
          </Card>{" "}
        </Col>{" "}
        <Col
          xs={{
            span: 24
          }}
          md={{
            span: 12
          }}
        >
          <Card title="Invoice Settings">
            <AppInvoiceSettings
              invoicePrefix={this.state.invoicePrefix}
              isLoading={this.state.invoiceIsLoading}
              triggerLoading={(loading = true) =>
                this.setState({
                  invoiceIsLoading: loading
                })
              }
              triggerPrefix={(prefix = "") =>
                this.setState({
                  invoicePrefix: prefix
                })
              }
            />{" "}
          </Card>{" "}
        </Col>{" "}
      </Row>
    );
  }
}

const mapStateToProps = state => {
  return {
    settings: state.settings
  };
};
export default connect(mapStateToProps)(AppSettings);
