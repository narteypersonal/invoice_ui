import React, { Component } from "react";
import { Table, Tag, Button, Card, Row, Col, Switch, Input, Icon } from "antd";

import Highlighter from "react-highlight-words";
import { connect } from "react-redux";
import {
  fetch_receipt,
  update_loading,
  update_receipt_datasource,
  update_receipt_validity,
  toggle_add_receipt_drawer_visible,
  toggle_receipt_filter_drawer_visible
} from "../redux/actions/receiptActions";

class ReceiptsTable extends Component {
  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div
        style={{
          padding: 8
        }}
      >
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{
            width: 188,
            marginBottom: 8,
            display: "block"
          }}
        />{" "}
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{
            width: 90,
            marginRight: 8
          }}
        >
          Search{" "}
        </Button>{" "}
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{
            width: 90
          }}
        >
          Reset{" "}
        </Button>{" "}
      </div>
    ),
    filterIcon: filtered => (
      <Icon
        type="search"
        style={{
          color: filtered ? "#1890ff" : undefined
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{
          backgroundColor: "#ffc069",
          padding: 0
        }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
    )
  });

  state = {
    searchText: "",
    columns: [
      {
        title: "Invoice Number",
        dataIndex: "num",
        key: "num",
        filterDropdown: ({
          setSelectedKeys,
          selectedKeys,
          confirm,
          clearFilters
        }) => (
          <div
            style={{
              padding: 8
            }}
          >
            <Input
              ref={node => {
                this.searchInput = node;
              }}
              placeholder={`Search receipt`}
              value={selectedKeys[0]}
              onChange={e =>
                setSelectedKeys(e.target.value ? [e.target.value] : [])
              }
              onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
              style={{
                width: 188,
                marginBottom: 8,
                display: "block"
              }}
            />{" "}
            <Button
              type="primary"
              onClick={() => this.handleSearch(selectedKeys, confirm)}
              icon="search"
              size="small"
              style={{
                width: 90,
                marginRight: 8
              }}
            >
              Search{" "}
            </Button>{" "}
            <Button
              onClick={() => this.handleReset(clearFilters)}
              size="small"
              style={{
                width: 90
              }}
            >
              Reset{" "}
            </Button>{" "}
          </div>
        ),
        filterIcon: filtered => (
          <Icon
            type="search"
            style={{
              color: filtered ? "#1890ff" : undefined
            }}
          />
        ),
        onFilter: (value, record) =>
          record["invoice"]["num"]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
          if (visible) {
            setTimeout(() => this.searchInput.select());
          }
        },
        render: (text, record) => (
          <div>
            {" "}
            {record.is_void ? (
              <Tag color="magenta"> {record.invoice.num} </Tag>
            ) : (
              <Tag color="green"> {record.invoice.num} </Tag>
            )}{" "}
          </div>
        )
      },
      {
        title: "Depositor",
        dataIndex: "depositor",
        key: "depositor",
        width: "20%",
        ...this.getColumnSearchProps("depositor")
      },

      {
        title: "Payment",
        dataIndex: "amount",
        key: "amount",
        render: (text, record) => <span> {record.amount.toFixed(2)} </span>
      },
      {
        title: "Deposit Type",
        dataIndex: "deposit_type",
        key: "deposit_type",
        render: (text, record) => (
          <span> {record.receipttype ? record.receipttype.name : ""} </span>
        )
      },
      {
        title: "Validity",
        dataIndex: "is_void",
        key: "is_void",
        render: (text, record) => (
          <div>
            {" "}
            {record.is_void ? (
              <Tag color="magenta"> Invalidated </Tag>
            ) : (
              <Tag color="green"> Valid </Tag>
            )}{" "}
          </div>
        )
      },
      {
        title: "Data Issued",
        dataIndex: "created_at",
        key: "created_at",
        render: (text, record) => (
          <span> {new Date(record.created_at).toLocaleDateString()} </span>
        )
      },

      {
        title: "Action",
        dataIndex: "action",
        key: "action",
        render: (text, record) => (
          <span>
            {" "}
            {record.is_void ? (
              <Button
                style={{
                  marginLeft: "2vw"
                }}
                shape="circle"
                size="small"
                icon="check-square"
                type="success"
                loading={
                  this.props.receipt[
                    `receiptValidityButton${record.id}IsLoading`
                  ]
                }
                onClick={this.props.update_receipt_validity.bind(this, record)}
              />
            ) : (
              <Button
                style={{
                  marginLeft: "2vw"
                }}
                shape="circle"
                size="small"
                icon="close-square"
                type="danger"
                loading={
                  this.props.receipt[
                    `receiptValidityButton${record.id}IsLoading`
                  ]
                }
                onClick={this.props.update_receipt_validity.bind(this, record)}
              />
            )}{" "}
          </span>
        )
      }
    ]
  };

  componentWillMount() {
    if (!this.props.receipt.loading && !this.props.receipt.datasource) {
      this.props.fetch_receipt();
    }
  }

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0]
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({
      searchText: ""
    });
  };

  render() {
    let {
      loading,
      datasource,
      receiptFilterDrawerVisible
    } = this.props.receipt;
    return (
      <Card>
        <Table
          bordered
          size="small"
          loading={loading}
          title={() => (
            <Row>
              <Col span={20}>
                <h1> Receipt Listing </h1>{" "}
              </Col>{" "}
              <Col span={4} id="dropdown-container">
                <Button
                  style={{ marginLeft: "6px" }}
                  onClick={this.props.fetch_receipt}
                  type="primary"
                  icon="reload"
                />
              </Col>
            </Row>
          )}
          columns={this.state.columns}
          dataSource={datasource}
        />{" "}
      </Card>
    );
  }
}

const mapStateToProps = state => {
  return {
    receipt: state.receipt
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetch_receipt: () => dispatch(fetch_receipt),
    update_loading: () => dispatch(update_loading),
    update_receipt_datasource: value =>
      dispatch(update_receipt_datasource(value)),
    update_receipt_validity: value =>
      dispatch(update_receipt_validity(dispatch, value)),
    toggle_receipt_filter_drawer_visible: value =>
      dispatch(toggle_receipt_filter_drawer_visible(value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReceiptsTable);
