import React, { Component } from "react";
import { Layout } from "antd";
import AppMenuItem from "./app-menu-item";
import { connect } from "react-redux";
import { fetch_settings } from "../redux/actions/settingActions";

const { Sider } = Layout;

class AppSider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false
    };
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };

  componentDidMount() {
    if (
      !this.props.settings.loading &&
      this.props.settings.details.length < 1
    ) {
      this.props.fetch_settings();
    }
  }
  render() {
    return (
      <Sider
        breakpoint="lg"
        collapsedWidth="0"
        onBreakpoint={broken => {
          
        }}
        onCollapse={(collapsed, type) => {
          
        }}
      >
        <div
          className="logo"
          style={{
            height: "5vh",
            background: "rgb(24, 144, 255)",
            margin: "5px 10px",
            color: "white",
            borderRadius: 5,
            lineHeight: 2
          }}
        >
          <h3
            style={{
              color: "white",
              fontWeight: 600
            }}
          >
            {" "}
            IMS
            {this.props.settings.details.company_name
              ? `-${this.props.settings.details.company_name}`
              : ""}{" "}
          </h3>{" "}
        </div>{" "}
        <AppMenuItem
          // showInvoiceListing={this.props.showInvoiceListing}
          // showProductAddModal={this.props.showProductAddModal}
          showUserAddModal={this.props.showUserAddModal}
        />{" "}
      </Sider>
    );
  }
}
const mapStateToProps = state => {
  return {
    settings: state.settings
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetch_settings: () => dispatch(fetch_settings)
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppSider);
