import React, { Component } from "react";
import {
  Form,
  Row,
  Col,
  Layout,
  Input,
  Button,
  Card,
  Icon,
  Checkbox,
  message
} from "antd";

import { Redirect, Link } from "react-router-dom";
import { PostDataService } from "../../services/post-data-service";
import { MakeGraphQLQuery } from "../../services/utils";
import { BASEAPIURL } from "../../env";
import { connect } from "react-redux";
import { toggle_app_login } from "../../redux/actions/appAction";

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class AppLogin extends Component {
  state = {
    redirect: false
  };
  componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState({
          iconLoading: true
        });
        values["customProvider"] = "staff";
        let data = values;
        data = {
          data
        };
        let queryName = "login";
        let query = MakeGraphQLQuery("mutation", queryName, data, [
          "access_token",
          "token_type"
        ]);
        console.log(BASEAPIURL);
        PostDataService(BASEAPIURL, query, {
          success: data => {
            if (data.data && data.data[queryName]) {
              // console.log(data.data[queryName]);
              sessionStorage.setItem(
                "userData",
                JSON.stringify(data.data[queryName])
              );

              this.props.toggle_app_login();
            } else if (data.errors) {
              message.error(data.errors[0].message, 10);
            }
          },
          always: () => {
            this.setState({
              iconLoading: false
            });
          }
        });
      }
    });
  };

  render() {
    if (this.props.app.login) {
      return <Redirect to="/app" />;
    }
    const {
      getFieldDecorator,
      getFieldsError,
      getFieldError,
      isFieldTouched
    } = this.props.form;
    const usernameError =
      isFieldTouched("username") && getFieldError("username");
    const passwordError =
      isFieldTouched("password") && getFieldError("password");
    return (
      <Layout
        style={{
          height: "100vh"
        }}
      >
        <Row>
          <Col span={24}>
            <Col
              xs={{ span: 24 }}
              sm={{ span: 20, offset: 2 }}
              md={{ span: 14, offset: 5 }}
              lg={{ span: 12, offset: 6 }}
              style={{
                marginTop: "30vh"
              }}
            >
              <Card
                title="Login"
                style={{
                  borderRadius: "15px 15px"
                }}
              >
                <Form onSubmit={this.handleSubmit}>
                  <Form.Item
                    validateStatus={usernameError ? "error" : ""}
                    help={usernameError || ""}
                  >
                    {" "}
                    {getFieldDecorator("username", {
                      rules: [
                        {
                          required: true,
                          message: "Please input your username!"
                        }
                      ]
                    })(
                      <Input
                        prefix={
                          <Icon
                            type="user"
                            style={{
                              color: "rgba(0,0,0,.25)"
                            }}
                          />
                        }
                        autoComplete="email"
                        type="email"
                        placeholder="Email"
                      />
                    )}{" "}
                  </Form.Item>{" "}
                  <Form.Item
                    validateStatus={passwordError ? "error" : ""}
                    help={passwordError || ""}
                  >
                    {" "}
                    {getFieldDecorator("password", {
                      rules: [
                        {
                          required: true,
                          message: "Please input your Password!"
                        }
                      ]
                    })(
                      <Input.Password
                        prefix={
                          <Icon
                            type="lock"
                            style={{
                              color: "rgba(0,0,0,.25)"
                            }}
                          />
                        }
                        autoComplete="password"
                        type="password"
                        placeholder="Password"
                      />
                    )}{" "}
                  </Form.Item>{" "}
                  <Form.Item>
                    {" "}
                    {getFieldDecorator("remember", {
                      valuePropName: "checked",
                      initialValue: true
                    })(<Checkbox> Remember me </Checkbox>)}{" "}
                    <Link
                      className="login-form-forgot"
                      to="/forgotten-password"
                    >
                      Forgot password{" "}
                    </Link>{" "}
                  </Form.Item>{" "}
                  <Form.Item>
                    <Button
                      type="primary"
                      htmlType="submit"
                      disabled={hasErrors(getFieldsError())}
                      loading={this.state.iconLoading}
                    >
                      Log in
                    </Button>{" "}
                  </Form.Item>{" "}
                </Form>{" "}
              </Card>{" "}
            </Col>{" "}
          </Col>{" "}
        </Row>{" "}
      </Layout>
    );
  }
}

// const mapDispatchToProps = dispatch => {
//   return {
//     update_profile_datasource: value =>
//       dispatch(update_profile_datasource(value))
//   };
// };
const Login = Form.create({
  name: "horizontal_login"
})(AppLogin);

const mapStateToProps = state => {
  return {
    app: state.app
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggle_app_login: () => dispatch(toggle_app_login)
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
