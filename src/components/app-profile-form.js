import React, { Component } from "react";
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Upload,
  Icon,
  message,
  Card
} from "antd";
import { BASEAPIURL, API_URL } from "../env";
import { MakeGraphQLQuery } from "../services/utils";
import { PostDataService } from "../services/post-data-service";
import { connect } from "react-redux";
import {
  update_profile_loading,
  update_profile_datasource
} from "../redux/actions/profileActions";
const FormItem = Form.Item;
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 16
    }
  }
};

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

const AppProfileForm = Form.create({
  name: "profile_detail_form"
})(
  class ProfileDetailForm extends Component {
    constructor(props) {
      super(props);
      this.state = {
        disabledInputs: true,
        previewVisible: false,
        previewImage: "",
        fileList: []
      };
    }

    handleEditClicked = () => {
      this.setState({
        disabledInputs: false
      });
    };

    handleResetClicked = () => {
      this.setState({
        disabledInputs: true
      });
    };

    handleSubmit = e => {
      e.preventDefault();
      if (this.state.disabledInputs) return;
      this.props.form.validateFields((err, values) => {
        if (!err) {
          let dataObj = {};
          for (let key in values) {
            if (key === "upload") continue;
            dataObj[key] = values[key] ? values[key] : "";
          }
          this.setState({
            disabledInputs: true
          });

          console.log(values);
          let data = dataObj;
          data = {
            data
          };
          let queryName = "";
          let query = "";

          queryName = "updateStaff";
          query = MakeGraphQLQuery("mutation", queryName, data, [
            "name",
            "email",
            "phone"
          ]);
          this.props.update_profile_loading(true);

          PostDataService(BASEAPIURL, query, {
            success: data => {
              if (data.data[queryName]) {
                message.success("Profile updated.");
                this.props.update_profile(data.data[queryName]);
              } else if (data.errors) {
                message.error(data.errors[0].message);
              }
            },
            always: () => {
              this.props.update_profile_loading(false);
              this.setState({
                disabledInputs: false
              });
            }
          });
        }
      });
    };

    render() {
      var { getFieldDecorator } = this.props.form;
      var { profile } = this.props;
      const { previewVisible, previewImage, fileList } = this.state;

      const uploadButton = (
        <div>
          <Icon type="plus" />
          <div className="ant-upload-text"> Upload </div>{" "}
        </div>
      );
      return (
        <Card title="Profiel Details" bordered={false}>
          <Form {...formItemLayout} onSubmit={this.handleSubmit}>
            <Form.Item>
              {getFieldDecorator("id", {
                initialValue: profile.id
              })(<Input type="hidden" />)}
            </Form.Item>
            <FormItem label="Username">
              {" "}
              {getFieldDecorator("name", {
                rules: [
                  {
                    required: true,
                    message: "Please input your username!"
                  }
                ],
                initialValue: profile.name
              })(
                <Input
                  disabled={this.state.disabledInputs}
                  placeholder="ex: mensah kusi."
                  type="text"
                />
              )}{" "}
            </FormItem>{" "}
            <FormItem label="Useremail">
              {" "}
              {getFieldDecorator("email", {
                rules: [
                  {
                    message: "Please input a valid email!"
                  }
                ],
                initialValue: profile.email
              })(
                <Input
                  disabled={this.state.disabledInputs}
                  placeholder="ex: mail@example.inc"
                  type="email"
                />
              )}{" "}
            </FormItem>{" "}
            <FormItem label="phone">
              {" "}
              {getFieldDecorator("phone", {
                rules: [
                  {
                    message: "Please input a valid phone number!"
                  }
                ],
                initialValue: profile.phone
              })(
                <Input
                  disabled={this.state.disabledInputs}
                  placeholder="ex: +233"
                  type="text"
                />
              )}{" "}
            </FormItem>{" "}
            <FormItem>
              <Row>
                <Col span={12}>
                  <Button
                    type=""
                    htmlType="reset"
                    onClick={this.handleResetClicked}
                  >
                    Reset{" "}
                  </Button>{" "}
                </Col>{" "}
                <Col
                  span={12}
                  style={{
                    textAlign: "right"
                  }}
                >
                  <Button
                    type=""
                    htmlType={this.state.disabledInputs ? "button" : "submit"}
                    onClick={e => {
                      if (this.state.disabledInputs) {
                        e.preventDefault();
                        this.handleEditClicked.apply(this);
                      }
                    }}
                    icon={this.state.disabledInputs ? "edit" : "upload"}
                    loading={profile.loading}
                  >
                    {this.state.disabledInputs ? "Edit" : "Submit"}{" "}
                  </Button>{" "}
                </Col>{" "}
              </Row>{" "}
            </FormItem>
          </Form>
        </Card>
      );
    }
  }
);

const mapStateToProps = state => {
  return {
    profile: state.profile
  };
};

const mapDispatchToProps = dispatch => {
  return {
    update_profile_loading: value => dispatch(update_profile_loading(value)),
    update_profile: value => dispatch(update_profile_datasource(value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppProfileForm);
