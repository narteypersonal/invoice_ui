import React, { Component } from "react";
import { Route, Redirect } from "react-router-dom";
import "../App.css";
import AppSider from "./app-sider";
import { AppFooter } from "./app-footer";

import { Layout, Affix } from "antd";
import AppHeader from "./app-header";
import { AppProfile } from "./app-profile";
import CollectionCreateInvoiceForm from "./app-invoice-modal";
import CollectionCreateProductForm from "./app-product-modal";
import CollectionCreateUserForm from "./app-user-modal";
import UsersTable from "./app-users-table";
import { PDFGenerator } from "./pdf/pdf-generator";
import ProductsTable from "./app-products-table";
import InvoicesTable from "./app-invoices-table";
import ReceiptsTable from "./app-receipts-table";
import AppSalesAccount from "./sales/accounts/app-accounts";
import AppIndividualSalesAccount from "./sales/individual/app-sales";
import AppIndividualSalesJournal from "./sales/individual/app-journal";
import AppSalesJournal from "./sales/journals/app-journals";
import DrawerInvoiceReceipts from "./drawers/drawer-invoice-receipts";
import DrawerSalesAccountFilter from "./drawers/drawer-sales-account-filter";
import DrawerSalesJournalFilter from "./drawers/drawer-sales-journal-filter";
import AppSettings from "./app-settings";
import CollectionCreateReceiptForm from "./app-receipt-modal";
import { fetch_receiptType } from "../redux/actions/receiptTypeActions";
import { connect } from "react-redux";
import { fetch_profile } from "../redux/actions/profileActions";
import { update_app_login } from "../redux/actions/appAction";
import Dashboard from "./dashboard/app-index";
import { update_invoice_detail_modal_visible } from "../redux/actions/invoiceActions";
import {
  update_product_modal_visible,
  update_product_add_action,
  update_product_edit_details,
  fetch_products
} from "../redux/actions/productActions";
import {
  fetch_roles,
  update_role_datasource
} from "../redux/actions/roleAction";
import DrawerAddReceipt from "./drawers/drawer-add-receipt";
import DrawerReceiptsFilter from "./drawers/drawer-receipts-filter";
import AppDebtors from "./sales/debtors/app-debtors";
import DrawerDebtorInvoices from "./drawers/drawer-debtor-invoices";
import AppExpenses from "./app-expenses";
import AppAddExpenseTypeModal from "./app-add-expense-type-modal";
import AppAddExpenseModal from "./app-add-expense-modal";
import AppCredits from "./app-credits";
import AppAddCreditorModal from "./app-add-creditor-modal";
import AppAddCreditModal from "./app-add-credit-modal";
import AppCreditors from "./app-creditors";
import AppExpenseType from "./app-expense-type";
import AppCashbook from "./app-cashbook";
import AppPurchaseInvoice from "./app-purchase-invoice";
import DrawerAddPurchaseInvoice from "./drawers/drawer-add-purchase-invoice";
import DrawerAddStock from "./drawers/drawer-add-stock";
import AppStockTable from "./stocks/app-stock-table";
import AppStockBalanceTable from "./stocks/app-stock-balance-table";
import DrawerGenerateInvoice from "./drawers/drawer-generate-invoice";

const { Content } = Layout;
var user = null;
class AppContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      appName: "IMS",

      rolesLoaded: false,
      user: {},
      pages: {
        productModalVisible: false,
        userModalVisible: false
      }
    };
  }

  showProductAddModal = () => {
    this.setState({
      pages: {
        ...this.state.pages,
        productModalVisible: true
      }
    });
  };

  hideProductModal = () => {
    this.setState({
      pages: {
        ...this.state.pages,
        productModalVisible: false
      }
    });
  };

  showUserAddModal = () => {
    this.setState({
      pages: {
        ...this.state.pages,
        userModalVisible: true
      }
    });
  };

  hideUserModal = () => {
    this.setState({
      pages: {
        ...this.state.pages,
        userModalVisible: false
      }
    });
  };

  componentWillMount() {
    let userData = JSON.parse(sessionStorage.getItem("userData"));

    let rolesData = JSON.parse(sessionStorage.getItem("roleData"));

    if (
      userData !== null &&
      Object.keys(userData).length > 0 &&
      !userData.email &&
      !userData.name
    ) {
      this.props.fetch_profile();
      if (!this.props.role.datasource) {
        if (rolesData != null) {
          this.props.update_role_datasource(rolesData);
        } else this.props.fetch_roles();
        if(!this.props.product.datasource){
          this.props.fetch_products();
        }
      }
    }
  }

  componentDidMount() {
    let userData = sessionStorage.getItem("userData");
    if (userData == undefined) {
      sessionStorage.clear();

      this.props.update_app_login(false);
    } else {
      this.setState({
        user: JSON.parse(userData)
      });
      this.props.fetch_receiptType();
      this.props.update_app_login(true);
    }
  }
  content = null;

  render() {
    if (!this.props.app.login) {
      return <Redirect to="/login" />;
    }
    return (
      <Layout
        style={{
          minHeight: "100vh"
        }}
      >
        <AppSider
          appName={this.state.appName}
          showUserAddModal={this.showUserAddModal}
        />{" "}
        <Layout>
          <Affix
            offsetTop={10}
            style={{
              position: "Fixed",
              right: 20,
              zIndex: 999
            }}
          />{" "}
          <AppHeader />{" "}
          <Content
            style={{
              margin: "24px 16px 0"
            }}
          >
            <DrawerGenerateInvoice />
            <DrawerDebtorInvoices />
            <DrawerInvoiceReceipts />
            <DrawerSalesAccountFilter />
            <DrawerSalesJournalFilter />
            <DrawerReceiptsFilter />
            <DrawerAddReceipt />
            <DrawerAddPurchaseInvoice />
            <DrawerAddStock />
            <Route
              path={`/app/profile`}
              render={() => (
                <AppProfile
                  username={this.state.user.name}
                  useremail={this.state.user.email}
                  userphone={this.state.user.phone}
                  userprofile={this.state.user.profile}
                />
              )}
            />{" "}
            <CollectionCreateProductForm />
            <CollectionCreateReceiptForm />{" "}
            <CollectionCreateUserForm
              rolesLoaded={() => sessionStorage.getItem("roleData")}
            />
            <AppAddExpenseTypeModal />
            <AppAddExpenseModal />
            <AppAddCreditorModal />
            <AppAddCreditModal />
            <Route path="/app/users" render={() => <UsersTable />} />
            <Route path="/app/pdf" render={() => <PDFGenerator />} />
            <Route path="/app/products" render={() => <ProductsTable />} />
            <Route path="/app/invoices" render={() => <InvoicesTable />} />
            <Route path="/app/receipts" render={() => <ReceiptsTable />} />
            <Route path="/app/credits" render={() => <AppCredits />} />
            <Route path="/app/creditors" render={() => <AppCreditors />} />
            <Route path="/app/expensetype" render={() => <AppExpenseType />} />
            <Route path="/app/expenses" render={() => <AppExpenses />} />
            <Route exact path="/app/stocks/balance" render={() => <AppStockBalanceTable />} />
            <Route exact path="/app/stocks" render={() => <AppStockTable />} />
            <Route
              path="/app/sales/general/accounts"
              render={() => <AppSalesAccount />}
            />{" "}
            <Route
              path="/app/sales/general/journals"
              render={() => <AppSalesJournal />}
            />{" "}
            <Route
              path="/app/sales/general/debtors-ledger"
              render={() => <AppDebtors />}
            />{" "}
            <Route
              path="/app/sales/individual/accounts"
              render={() => <AppIndividualSalesAccount />}
            />{" "}
            <Route
              path="/app/sales/individual/journals"
              render={() => <AppIndividualSalesJournal />}
            />
            <Route
              path="/app/purchase"
              render={() => <AppPurchaseInvoice />}
            />
            <Route path="/app/cashbooks" render={() => <AppCashbook />} />
            <Route
              exact={true}
              path="/app/settings"
              render={() => <AppSettings />}
            />{" "}
            <Route exact={true} path={`/app`} render={() => <Dashboard />} />{" "}
            {/*  */}{" "}
          </Content>{" "}
          <AppFooter />
        </Layout>{" "}
      </Layout>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetch_receiptType: () => dispatch(fetch_receiptType),
    fetch_profile: () => dispatch(fetch_profile),
    fetch_products: () => dispatch(fetch_products(dispatch)),
    update_app_login: value => dispatch(update_app_login(value)),
    update_invoice_detail_modal_visible: value =>
      dispatch(update_invoice_detail_modal_visible(value)),
    update_product_modal_visible: value =>
      dispatch(update_product_modal_visible(value)),
    update_product_add_action: value =>
      dispatch(update_product_add_action(value)),
    update_product_edit_details: value =>
      dispatch(update_product_edit_details(value)),
    fetch_roles: () => dispatch(fetch_roles),
    update_role_datasource: value => dispatch(update_role_datasource(value))
  };
};

const mapStateToProps = state => {
  return {
    app: state.app,
    invoice: state.invoice,
    role: state.role,
    product: state.product
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppContainer);
