import React from "react";
import {
  Row,
  Col,
  Form,
  Input,
  Icon,
  Modal,
  InputNumber,
  message,
  Button,
  Select,
  AutoComplete
} from "antd";
import { MakeGraphQLQuery } from "../services/utils";
import { PostDataService } from "../services/post-data-service";
import { BASEAPIURL } from "../env";
import { connect } from "react-redux";
import {
  toggle_receipt_modal_visible,
  update_receipt_datasource,
  update_receipt_detail,
  fetch_receipt
} from "../redux/actions/receiptActions";
import debounce from "lodash/debounce";
import { update_invoice_via_receipt } from "../redux/actions/invoiceActions";

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

const CollectionCreateReceiptForm = Form.create({
  name: "form_in_receipt_modal",
  mapPropsToFields(props) {
    return {
      invoice_num: Form.createFormField({
        value: props.receipt.receiptDetail.num
      }),
      invoice_id: Form.createFormField({
        value: props.receipt.receiptDetail.id
      }),
      debt: Form.createFormField({
        value: props.receipt.receiptDetail.balance
          ? props.receipt.receiptDetail.balance.toFixed(2)
          : 0
      }),
      depositor: Form.createFormField({
        value: props.receipt.receiptDetail.customer_name
      })
    };
  },
  onValuesChange(_, fieldObject, allValues) {
    if (fieldObject.hasOwnProperty("amount")) {
      console.log(fieldObject);
      let amount = allValues.amount ? parseFloat(allValues.amount) : 0;
      _.form.setFields({
        balance: {
          value: (parseFloat(allValues.debt) - amount).toFixed(2)
        }
      });
    }
  }
})(
  // eslint-disable-next-line
  class ReceiptModal extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        iconLoading: false,
        result: [],
        fetching: false
      };
      this.lastFetchId = 0;
      this.handleSearch = debounce(this.handleSearch, 800);
    }

    componentDidMount() {
      // To disabled submit button at the beginning.
      this.props.form.validateFields();
    }

    handleSubmit = e => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          this.setState({
            iconLoading: true
          });
          let {
            amount,
            depositor,
            description,
            invoice_num,
            invoice_id
          } = values;
          if (!description) description = "";
          let data = {
            amount,
            depositor,
            description,
            invoice_num,
            invoice_id
          };

          for (let receipttype of this.props.receiptType.datasource) {
            if (receipttype.name === values["receipttype"]) {
              data["receipttype_id"] = receipttype.id;
            }
          }

          let queryName = "createReceipt";
          let query = "mutation";

          PostDataService(
            BASEAPIURL,
            MakeGraphQLQuery(query, queryName, { data: data }, [
              "id",
              "amount",
              "amount_received",
              "amount_changed",
              "depositor",
              "description",
              "receipttype{id, name}",
              "invoice{id, num, amount_recieved,balance}",
              "created_at",
              "is_void"
            ]),
            {
              success: data => {
                try {
                  if (data.data[queryName]) {
                    message.success("Receipt added");
                    data = data.data[queryName];
                    data["key"] = data.id;
                    // console.log(data);
                    this.props.form.resetFields();
                    this.props.update_invoice_via_receipt(data);
                    if (!this.props.receipt.datasource) {
                      this.props.fetch_receipts();
                    } else {
                      this.props.update_receipt_datasource([
                        ...this.props.receipt.datasource,
                        data
                      ]);
                    }
                  } else if (data.errors) {
                    message.error(data.errors[0].message);
                  }
                } catch (ex) {
                  console.log(ex);
                }
              },
              always: () => {
                this.setState({
                  iconLoading: false
                });
                this.handleCancel();
              }
            }
          );
        } else {
          this.props.form.getFieldsError();
        }
      });
    };

    handleSearch = value => {
      let queryname = "searchInvoice";
      PostDataService(
        BASEAPIURL,
        MakeGraphQLQuery(
          "query",
          queryname,
          { data: { invoice_number: value } },
          ["id", "num"]
        ),
        {
          success: data => {
            console.log(data);
            if (data.data) {
              // console.log(data.data[queryname]);
              this.setState({ result: data.data[queryname] });
            }
          }
        }
      );
    };

    handleSelect = (value, option) => {
      this.props.form.setFields({
        invoice_id: {
          value: option.key
        }
      });
    };

    handleCancel = () => {
      this.props.toggle_receipt_modal_visible(
        !this.props.receipt.receiptModalVisible
      );
      this.props.update_receipt_detail([]);
    };

    render() {
      const { receiptModalVisible } = this.props.receipt;

      const { Option } = Select;
      const { result } = this.state;
      const children = result.map(invoice => (
        <Option key={invoice.id} value={invoice.num}>
          {invoice.num}
        </Option>
      ));
      const {
        getFieldDecorator,
        getFieldsError,
        getFieldError,
        isFieldTouched
      } = this.props.form;
      const invoiceNameError =
        isFieldTouched("depositor") && getFieldError("depositor");
      const depositorNameError =
        isFieldTouched("depositor") && getFieldError("depositor");
      const amountError = isFieldTouched("amount") && getFieldError("amount");
      const amountChangedError =
        isFieldTouched("change") && getFieldError("change");
      const amountReceivedError =
        isFieldTouched("received") && getFieldError("received");
      const receiptTypeError =
        isFieldTouched("receipttype") && getFieldError("receipttype");

      return (
        <Modal
          visible={receiptModalVisible}
          title={this.props.update ? "Edit Receipt" : "Add Receipt"}
          okText="Create"
          onCancel={this.handleCancel.bind(this)}
          onOk={() => {}}
          footer={false}
          maskClosable={false}
        >
          <Form layout="vertical" onSubmit={this.handleSubmit}>
            <Row>
              <Col span={18}>
                {" "}
                {this.props.update && (
                  <Form.Item>
                    {" "}
                    {getFieldDecorator("id", {})(
                      <Input type="hidden" name="id" />
                    )}{" "}
                  </Form.Item>
                )}{" "}
                {
                  <Form.Item>
                    {" "}
                    {getFieldDecorator("invoice_id", {})(
                      <Input type="hidden" name="invoice_id" />
                    )}{" "}
                  </Form.Item>
                }{" "}
                <Form.Item
                  label="Invoice Name"
                  validateStatus={invoiceNameError ? "error" : ""}
                  help={invoiceNameError || ""}
                >
                  {getFieldDecorator("invoice_num", {
                    rules: [
                      {
                        required: true,
                        message: "Please input a valid invoice number!"
                      }
                    ]
                  })(
                    <AutoComplete
                      name="invoice_num"
                      size="large"
                      style={{ width: 200 }}
                      onSearch={this.handleSearch}
                      onSelect={this.handleSelect}
                      placeholder="Invoice number"
                    >
                      {children}
                    </AutoComplete>
                  )}
                </Form.Item>
                <Form.Item
                  label="Depositor Name"
                  validateStatus={depositorNameError ? "error" : ""}
                  help={depositorNameError || ""}
                >
                  {getFieldDecorator("depositor", {
                    rules: [
                      {
                        required: true,
                        message: "Please input depositor's name!"
                      }
                    ]
                  })(
                    <Input
                      size="large"
                      placeholder="eg:John Boadi"
                      name="depositor"
                      type="text"
                    />
                  )}{" "}
                </Form.Item>{" "}
                <Row gutter={1}>
                  <Col span={12}>
                    <Form.Item
                      label="Received"
                      validateStatus={amountReceivedError ? "error" : ""}
                      help={amountReceivedError || ""}
                    >
                      {getFieldDecorator("received", {
                        rules: [
                          {
                            required: true
                          },
                          {
                            validator: (rule, value, cb) =>
                              value > 0
                                ? cb()
                                : cb("Value must be greater than zero(0)")
                          }
                        ],
                        initialValue: 0
                      })(
                        <InputNumber
                          size="large"
                          style={{
                            width: 150
                          }}
                          min={0}
                          formatter={value =>
                            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                          name="received"
                        />
                      )}{" "}
                    </Form.Item>{" "}
                  </Col>
                  <Col span={12}>
                    <Form.Item
                      label="Change"
                      validateStatus={amountChangedError ? "error" : ""}
                      help={amountChangedError || ""}
                    >
                      {getFieldDecorator("change", {
                        rules: [
                          {
                            required: true
                          },
                          {
                            validator: (rule, value, cb) =>
                              value > 0
                                ? cb()
                                : cb("Value must be greater than zero(0)")
                          }
                        ],
                        initialValue: 0
                      })(
                        <InputNumber
                          size="large"
                          style={{
                            width: 150
                          }}
                          min={0}
                          formatter={value =>
                            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                          name="change"
                        />
                      )}{" "}
                    </Form.Item>{" "}
                  </Col>
                </Row>
                <Row gutter={1}>
                  {this.props.receipt.receiptDetail.hasOwnProperty(
                    "balance"
                  ) && (
                    <Col span={12}>
                      <Form.Item label="Debt">
                        {getFieldDecorator("debt", {})(
                          <Input
                            size="large"
                            style={{
                              width: 150
                            }}
                            disabled={true}
                            min={1}
                            formatter={value => {
                              `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            }}
                            name="debt"
                          />
                        )}{" "}
                      </Form.Item>{" "}
                    </Col>
                  )}
                  <Col span={12}>
                    <Form.Item
                      label="Amount"
                      validateStatus={amountError ? "error" : ""}
                      help={amountError || ""}
                    >
                      {getFieldDecorator("amount", {
                        rules: [
                          {
                            required: true,
                            message: "Please input deposited amount! "
                          },
                          {
                            validator: (rule, value, cb) =>
                              value > 0
                                ? cb()
                                : cb("Value must be greater than zero(0)")
                          }
                        ],
                        initialValue: 0
                      })(
                        <InputNumber
                          size="large"
                          style={{ width: 150 }}
                          min={0}
                          disabled={true}
                          formatter={value =>
                            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                          prefix={
                            <Icon
                              type="tag"
                              style={{
                                color: "rgba(0,0,0,.25)"
                              }}
                            />
                          }
                          name="amount"
                        />
                      )}{" "}
                    </Form.Item>{" "}
                  </Col>{" "}
                </Row>{" "}
                <Row gutter={1}>
                  {this.props.receipt.receiptDetail.hasOwnProperty(
                    "balance"
                  ) && (
                    <Col span={12}>
                      <Form.Item label="Balance">
                        {getFieldDecorator("balance", {})(
                          <Input
                            size="large"
                            style={{ width: 150 }}
                            min={0}
                            disabled={true}
                            formatter={value =>
                              `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                            }
                            name="balance"
                          />
                        )}{" "}
                      </Form.Item>{" "}
                    </Col>
                  )}{" "}
                  <Col span={12}>
                    <Form.Item
                      label="Receipt type"
                      validateStatus={receiptTypeError ? "error" : ""}
                      help={receiptTypeError || ""}
                    >
                      {getFieldDecorator("receipttype", {
                        rules: [
                          {
                            required: true,
                            message: "Please select a receipt type!"
                          }
                        ]
                      })(
                        <Select
                          size="large"
                          name="receipttype"
                          loading={this.props.receiptType.loading}
                        >
                          {this.props.receiptType.datasource &&
                            this.props.receiptType.datasource.map(type => (
                              <Option key={type.id} value={type.name}>
                                {type.name}
                              </Option>
                            ))}
                        </Select>
                      )}
                    </Form.Item>
                  </Col>
                </Row>{" "}
                <Form.Item label="Description">
                  {getFieldDecorator("description", {})(
                    <Input.TextArea
                      rows={4}
                      name="description"
                      placeholder="Payment description"
                    />
                  )}
                </Form.Item>
              </Col>{" "}
            </Row>{" "}
            <Form.Item
              style={{
                textAlign: "right"
              }}
            >
              <Button onClick={this.handleCancel.bind(this)}> Cancel </Button>{" "}
              <Button
                type="primary"
                htmlType="submit"
                disabled={hasErrors(getFieldsError())}
                loading={this.state.iconLoading}
              >
                {this.props.update ? "Update" : "Generate"}{" "}
              </Button>{" "}
            </Form.Item>{" "}
          </Form>{" "}
        </Modal>
      );
    }
  }
);

const mapStateToProps = state => {
  return {
    receipt: state.receipt,
    receiptType: state.receiptType
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetch_receipts: dispatch(fetch_receipt),
    toggle_receipt_modal_visible: value =>
      dispatch(toggle_receipt_modal_visible(value)),
    update_receipt_datasource: value =>
      dispatch(update_receipt_datasource(value)),
    update_receipt_detail: value => dispatch(update_receipt_detail(value)),
    update_invoice_via_receipt: value =>
      dispatch(update_invoice_via_receipt(value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CollectionCreateReceiptForm);
