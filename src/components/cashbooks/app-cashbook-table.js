import React from "react";
import { connect } from "react-redux";
import { Table, Input, Button, Tag, Icon, Row, Col } from "antd";
import Highlighter from "react-highlight-words";
import { fetch_cashbooks } from "../../redux/actions/cashbookActions";

class AppCashbookTable extends React.Component {
  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div
        style={{
          padding: 8
        }}
      >
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{
            width: 188,
            marginBottom: 8,
            display: "block"
          }}
        />{" "}
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{
            width: 90,
            marginRight: 8
          }}
        >
          Search{" "}
        </Button>{" "}
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{
            width: 90
          }}
        >
          Reset{" "}
        </Button>{" "}
      </div>
    ),
    filterIcon: filtered => (
      <Icon
        type="search"
        style={{
          color: filtered ? "#1890ff" : undefined
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{
          backgroundColor: "#ffc069",
          padding: 0
        }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
    )
  });

  state = {
    searchText: "",
    columns: [
      {
        title: "Date Issued",
        dataIndex: "created_at",
        key: "created_at",
        render: (text, record) => (
          <span> {new Date(record.created_at).toLocaleDateString()} </span>
        )
      },
      {
        title: "Description",
        dataIndex: "description",
        key: "description",
        ...this.getColumnSearchProps("description")
      },
      {
        title: "folio",
        dataIndex: "folio_number",
        key: "folio_number",
        filterDropdown: ({
          setSelectedKeys,
          selectedKeys,
          confirm,
          clearFilters
        }) => (
          <div
            style={{
              padding: 8
            }}
          >
            <Input
              ref={node => {
                this.searchInput = node;
              }}
              placeholder={`Search Folio Number`}
              value={selectedKeys[0]}
              onChange={e =>
                setSelectedKeys(e.target.value ? [e.target.value] : [])
              }
              onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
              style={{
                width: 188,
                marginBottom: 8,
                display: "block"
              }}
            />{" "}
            <Button
              type="primary"
              onClick={() => this.handleSearch(selectedKeys, confirm)}
              icon="search"
              size="small"
              style={{
                width: 90,
                marginRight: 8
              }}
            >
              Search{" "}
            </Button>{" "}
            <Button
              onClick={() => this.handleReset(clearFilters)}
              size="small"
              style={{
                width: 90
              }}
            >
              Reset{" "}
            </Button>{" "}
          </div>
        ),
        filterIcon: filtered => (
          <Icon
            type="search"
            style={{
              color: filtered ? "#1890ff" : undefined
            }}
          />
        ),
        onFilter: (value, record) =>
          record["folio_number"]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
          if (visible) {
            setTimeout(() => this.searchInput.select());
          }
        },
        render: (text, record) => (
          <div>
            <Tag color="green"> {record.folio_number} </Tag>
          </div>
        )
      },
      {
        title: "Debit",
        dataIndex: "debit",
        key: "debit",
        width: "20%",
        render: (text, record) => <span> {record.debit.toFixed(2)} </span>
      },

      {
        title: "Credit",
        dataIndex: "credit",
        key: "credit",
        render: (text, record) => {
          if(parseFloat(text) > 0){
            return (<span style={{
              color: "#f50"
            }}> ({record.credit.toFixed(2)}) </span>)
          }else{
            return <span > {record.credit.toFixed(2)}</span>
          }
        }
      },
      {
        title: "Balance_BD",
        dataIndex: "balance_bd",
        key: "balance_bd",
        render: (text, record) => (
          <span>
            {
              record.balance_bd < 0 ? (
              <b
                style={{
                  color: "#f50"
                }}
              >
                
                {`(${Math.abs(record.balance_bd.toFixed(2))})`}
              </b>
            ) : (
              <span> {record.balance_bd.toFixed(2)} </span>
            )}
            
          </span>
        )
      }
    ]
  };

  componentWillMount() {
    if (
      !this.props.cashbook.datasource ||
      (!this.props.cashbook.loading &&
        this.props.cashbook.datasource.length < 1)
    ) {
      this.props.fetch_cashbooks();
    }
  }

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0]
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({
      searchText: ""
    });
  };
  render() {
    const { loading, datasource } = this.props.cashbook;
    return (
      <Table
        bordered
        size="small"
        title={() => (
          <Row>
            <Col span={20}>
              <h1>Cashbook</h1>{" "}
            </Col>{" "}
            <Col>
              <Button
                onClick={this.props.fetch_cashbooks}
                type="primary"
                icon="reload"
              />
            </Col>
          </Row>
        )}
        loading={loading}
        columns={this.state.columns}
        dataSource={datasource}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    cashbook: state.cashbook
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetch_cashbooks: () => dispatch(fetch_cashbooks)
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppCashbookTable);
