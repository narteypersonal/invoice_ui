import React, { Component } from "react";
import {
  Table,
  Divider,
  Button,
  Card,
  Row,
  Col,
  message,
  Input,
  Icon
} from "antd";
import { PostDataService } from "../services/post-data-service";
import { BASEAPIURL } from "../env";
import { MakeGraphQLQuery } from "../services/utils";
import CollectionCreateProductForm from "./app-product-modal";
import Highlighter from "react-highlight-words";
import { connect } from "react-redux";
import {
  fetch_products,
  update_product_modal_visible,
  update_product_add_action,
  update_product_edit_details,
  remove_product
} from "../redux/actions/productActions";

class ProductsTable extends Component {
  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div
        style={{
          padding: 8
        }}
      >
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{
            width: 188,
            marginBottom: 8,
            display: "block"
          }}
        />{" "}
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{
            width: 90,
            marginRight: 8
          }}
        >
          Search{" "}
        </Button>{" "}
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{
            width: 90
          }}
        >
          Reset{" "}
        </Button>{" "}
      </div>
    ),
    filterIcon: filtered => (
      <Icon
        type="search"
        style={{
          color: filtered ? "#1890ff" : undefined
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{
          backgroundColor: "#ffc069",
          padding: 0
        }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
    )
  });

  state = {
    datasource: [],
    loading: false,
    filteredDatasource: null,
    editProductVisible: false,
    productUpdateAction: false,
    columns: [
      {
        title: "Product",
        dataIndex: "name",
        key: "name",
        width: "20%",
        ...this.getColumnSearchProps("name")
      },
      ,
      {
        title: "In Stock",
        dataIndex: "quantity",
        key: "quantity",
        defaultSortOrder: 'descend',
        sorter: (a, b) => a.quantity - b.quantity,
      },

      {
        title: "Price",
        dataIndex: "price",
        key: "price",
        sortDirections: ['descend', 'ascend'],
        sorter: (a, b) => a.price - b.price,
      },

      {
        title: "Action",
        dataIndex: "action",
        key: "action",
        render: (text, record) => (
          <span>
            <Button
              shape="circle"
              size="small"
              icon="edit"
              onClick={this.showEditProductModal.bind(this, record)}
            />{" "}
            <Divider type="vertical" />
            <Button
              shape="circle"
              size="small"
              icon="delete"
              loading={this.props.product[`removeIconLoading${record.id}`]}
              onClick={this.props.remove_product.bind(this, record.id)}
            />{" "}
          </span>
        )
      }
    ]
  };

  handleProductAdd = event => {
    this.props.update_product_edit_details({
      productId: "",
      productName: "",
      productPrice: ""
    });
    this.props.update_product_modal_visible(true);
    this.props.update_product_add_action(false);
  };

  handleSearch = (selectedKeys, confirm) => {
    if (!selectedKeys) {

      this.setState({
        searchText: "",
        filteredDatasource: null
      });
      return
    }
    let pattern = new RegExp(`${selectedKeys}`, 'gi');
    let result = this.props.product.datasource.filter((product) =>
      product.name.match(pattern))
    this.setState({
      searchText: selectedKeys[0],
      filteredDatasource: result
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({
      searchText: ""
    });
  };

  showEditProductModal = data => {
    this.props.update_product_edit_details({
      productId: data.id,
      productName: data.name,
      productPrice: data.price
    });
    this.props.update_product_modal_visible(true);
    this.props.update_product_add_action(true);
  };

  componentWillMount() {
    if (
      !this.props.product.loading &&
      this.props.product.datasource === null
    ) {
      this.props.fetch_products();
    }
  }

  render() {
    const { product } = this.props;
    return (
      <Card>
        <Table
          bordered
          size="small"
          loading={product.loading}
          title={() => (
            <Row>
              <Col span={6}>
                <h1> Product Listing </h1>{" "}
              </Col>{" "}
              <Col span={14} id="dropdown-container">
                <Input.Search
                  disabled={this.state.loading}
                  suffix={this.state.loading ? <Icon type="loading" /> : ""}
                  placeholder="search product name"
                  onSearch={value => this.handleSearch(value)}
                  onChange={(e) => this.handleSearch(e.target.value)}
                  style={{ width: 500 }}
                />

                <Button
                  style={{ marginLeft: "6px" }}
                  onClick={this.props.fetch_products}
                  type="primary"
                  icon="reload"
                />
              </Col>
              <Col span={4}>

                <Button type="dashed" onClick={this.handleProductAdd}>
                  Add Product{" "}
                </Button>{" "}
              </Col>{" "}
            </Row>
          )}
          columns={this.state.columns}
          dataSource={this.state.filteredDatasource || product.datasource}
        />{" "}
      </Card>
    );
  }
}

const mapStateToProps = state => {
  return {
    product: state.product
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetch_products: () => dispatch(fetch_products(dispatch)),
    update_product_modal_visible: value =>
      dispatch(update_product_modal_visible(value)),
    update_product_add_action: value =>
      dispatch(update_product_add_action(value)),
    update_product_edit_details: value =>
      dispatch(update_product_edit_details(value)),
    remove_product: value => dispatch(remove_product(dispatch, value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductsTable);
