import React from "react";
import { Card } from "antd";
import AppExpenseTypesTable from "./expenses/app-expense-types-table";

const AppExpenseType = props => {
  return (
    <Card title="Expense Types">
      <AppExpenseTypesTable />
    </Card>
  );
};

export default AppExpenseType;
