import React from "react";
import {
  Row,
  Col,
  Form,
  Input,
  Icon,
  Modal,
  message,
  Button,
  Select
} from "antd";
import { MakeGraphQLQuery } from "../services/utils";
import { BASEAPIURL } from "../env";
import { PostDataService } from "../services/post-data-service";
import { connect } from "react-redux";
import {
  update_user_modal_visible,
  update_users_datasource
} from "../redux/actions/userActions";

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

const CollectionCreateUserForm = Form.create({
  name: "form_in_user_modal",
  mapPropsToFields(props) {
    return {
      name: Form.createFormField({
        value: props.user.username
      }),
      email: Form.createFormField({
        value: props.user.useremail
      }),
      phone: Form.createFormField({
        value: props.user.userphone
      }),
      role: Form.createFormField({
        value: props.user.userrole
      })
    };
  }
})(
  // eslint-disable-next-line
  class extends React.Component {
    state = {
      iconLoading: false
    };
    componentDidMount() {
      // To disabled submit button at the beginning.
      // this.props.form.validateFields();
      // console.log(this.state.prefillValues);
      // console.log(JSON.parse(sessionStorage.getItem("roleData")));
    }

    renderDefaultsFields = () => {
      this.setState({
        iconLoading: true
      });
    };

    handleSubmit = e => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          this.setState({
            iconLoading: true
          });

          let role = this.props.role.datasource.filter(role => {
            if (role.name === values.role) return true;
          });

          values["role"] = parseInt(role[0].id);

          let data = values;
          data = {
            data
          };
          let query = "";
          let queryName = "";

          if (this.props.user.userUpdateAction) {
            queryName = "updateStaff";
            query = MakeGraphQLQuery("mutation", queryName, data, [
              "id",
              "name",
              "email",
              "phone",
              "roles{id name}"
            ]);
          } else {
            queryName = "addStaff";
            query = MakeGraphQLQuery("mutation", queryName, data, [
              "id",
              "name",
              "email",
              "phone",
              "roles{id name}"
            ]);
          }

          PostDataService(BASEAPIURL, query, {
            success: data => {
              if (data.data[queryName]) {
                data = data.data[queryName];
                data["key"] = data.id;
                if (this.props.user.userUpdateAction)
                  this.props.update_user_datasource(
                    this.props.user.datasource.map(user => {
                      if (user.id === data.id) return data;

                      return user;
                    })
                  );
                else {
                  this.props.update_user_datasource([
                    data,
                    ...this.props.user.datasource
                  ]);
                }
                message.success(`Successful, view staff profile`);
                this.props.form.resetFields();
              } else if (data.errors) {
                message.error(data.errors[0].message);
              }
            },
            always: () => {
              if (this.props.user.userUpdateAction)
                this.props.update_user_modal_visible(false);

              this.props.form.resetFields();
              this.setState({
                iconLoading: false
              });
            }
          });
        }
      });
    };
    render() {
      const { user, role } = this.props;

      const {
        getFieldDecorator,
        getFieldsError,
        getFieldError,
        isFieldTouched
      } = this.props.form;

      const nameError = isFieldTouched("name") && getFieldError("name");
      const emailError = isFieldTouched("email") && getFieldError("email");
      const roleError = isFieldTouched("role_id") && getFieldError("role_id");
      const phoneError = isFieldTouched("phone") && getFieldError("phone");
      const passwordError = isFieldTouched("password") && getFieldError("password");

      return (
        <Modal
          visible={user.userModalVisible}
          title={user.userUpdateAction ? "Edit Staff" : "Add New Staff"}
          onCancel={this.props.update_user_modal_visible.bind(this, false)}
          footer={false}
          maskClosable={false}
        >
          <Form
            layout="vertical"
            onSubmit={
              this.props.handleSubmit
                ? this.props.handleSubmit
                : this.handleSubmit
            }
          >
            <Row>
              <Col span={24}>
                <Form.Item
                  label="Staff Fullname"
                  validateStatus={nameError ? "error" : ""}
                  help={nameError || ""}
                >
                  {getFieldDecorator("name", {
                    rules: [
                      {
                        required: true,
                        message: "Please input staff full name!"
                      }
                    ]
                  })(
                    <Input
                      prefix={
                        <Icon
                          type="user"
                          style={{
                            color: "rgba(0,0,0,.25)"
                          }}
                        />
                      }
                      placeholder="eg: John Boadi"
                      name="name"
                      type="text"
                    />
                  )}{" "}
                </Form.Item>{" "}
                <Form.Item
                  label="Email"
                  validateStatus={emailError ? "error" : ""}
                  help={emailError || ""}
                >
                  {getFieldDecorator("email", {
                    rules: [
                      {
                        required: true,
                        message: "Please input staff email!"
                      }
                    ]
                  })(
                    <Input
                      prefix={
                        <Icon
                          type="user"
                          style={{
                            color: "rgba(0,0,0,.25)"
                          }}
                        />
                      }
                      placeholder="eg: johnboadi@mail.com"
                      name="name"
                      type="email"
                    />
                  )}{" "}
                </Form.Item>{" "}
                <Form.Item
                  label="phone"
                  validateStatus={phoneError ? "error" : ""}
                  help={phoneError || ""}
                >
                  {getFieldDecorator("phone", {
                    rules: [
                      {
                        required: true,
                        message: "Please input staff phone!"
                      }
                    ]
                  })(
                    <Input
                      prefix={
                        <Icon
                          type="phone"
                          style={{
                            color: "rgba(0,0,0,.25)"
                          }}
                        />
                      }
                      placeholder="eg: +233 56 998 5666"
                      name="phone"
                      type="tel"
                    />
                  )}{" "}
                </Form.Item>{" "}
                <Form.Item
                  label="password"
                  validateStatus={passwordError ? "error" : ""}
                  help={passwordError || ""}
                >
                  {getFieldDecorator("password", {
                    rules: [
                      {
                        required: true,
                        message: "Please provide a password!"
                      }
                    ]
                  })(
                    <Input.Password
                      prefix={
                        <Icon
                          type="password"
                          style={{
                            color: "rgba(0,0,0,.25)"
                          }}
                        />
                      }
                      placeholder="****"
                      name="password"
                      type="password"
                    />
                  )}{" "}
                </Form.Item>{" "}
                <Row gutter={2}>
                  <Col span={24}>
                    <Form.Item
                      label="Role"
                      validateStatus={roleError ? "error" : ""}
                      help={roleError || ""}
                    >
                      {getFieldDecorator("role", {
                        rules: [
                          {
                            required: true,
                            message: "Please select staff role"
                          }
                        ]
                      })(
                        <Select name="role_id">
                          {role.datasource &&
                            role.datasource.map(role => {
                              return (
                                <Select.Option value={role.name} key={role.id}>
                                  {" "}
                                  {role.name}{" "}
                                </Select.Option>
                              );
                            })}
                        </Select>
                      )}{" "}
                    </Form.Item>{" "}
                  </Col>{" "}
                </Row>{" "}
              </Col>{" "}
            </Row>{" "}
            <Form.Item
              style={{
                textAlign: "right"
              }}
            >
              <Button
                onClick={this.props.update_user_modal_visible.bind(this, false)}
              >
                Cancel
              </Button>
              <Button
                type="primary"
                htmlType="submit"
                disabled={hasErrors(getFieldsError())}
                loading={this.state.iconLoading}
              >
                {user.userUpdateAction ? "Update" : "Add"}
              </Button>
            </Form.Item>
          </Form>
        </Modal>
      );
    }
  }
);

const mapStateToProps = state => {
  return {
    user: state.user,
    role: state.role
  };
};

const mapDispatchToProps = dispatch => {
  return {
    update_user_modal_visible: value =>
      dispatch(update_user_modal_visible(value)),
    update_user_datasource: value => dispatch(update_users_datasource(value))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CollectionCreateUserForm);
