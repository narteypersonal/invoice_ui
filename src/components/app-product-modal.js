import React from "react";
import {
  Row,
  Col,
  Form,
  Input,
  Icon,
  Modal,
  InputNumber,
  message,
  Button
} from "antd";
import { MakeGraphQLQuery } from "../services/utils";
import { PostDataService } from "../services/post-data-service";
import { connect } from "react-redux";
import {
  update_product_modal_visible,
  update_product_datasource
} from "../redux/actions/productActions";
import { BASEAPIURL } from "../env";

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}
const CollectionCreateProductForm = Form.create({
  name: "form_in_product_modal",
  mapPropsToFields(props) {
    return {
      id: Form.createFormField({
        value: props.product.productId
      }),
      name: Form.createFormField({
        value: props.product.productName
      }),
      price: Form.createFormField({
        value: props.product.productPrice
      })
    };
  }
})(
  // eslint-disable-next-line
  class extends React.Component {
    state = {
      iconLoading: false
    };
    componentDidMount() {
      // To disabled submit button at the beginning.
      this.props.form.validateFields();
    }

    handleSubmit = e => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          this.setState({
            iconLoading: true
          });
          let data = values;
          data = {
            data
          };
          let queryName = "";
          let query = "";
          if (this.props.product.productUpdateAction) {
            data.data["id"] = Number.parseInt(data.data["id"]);
            queryName = "updateProduct";
            query = MakeGraphQLQuery("mutation", queryName, data, [
              "id",
              "name",
              "price",
              "quantity",
              "quantity_sold",
            ]);
          } else {
            queryName = "addProduct";
            query = MakeGraphQLQuery("mutation", queryName, data, [
              "id",
              "name",
              "price",
              "quantity",
              "quantity_sold",
            ]);
          }

          PostDataService(BASEAPIURL, query, {
            success: data => {
              if (data.data[queryName]) {
                data = data.data[queryName];
                data["key"] = data.id;
                if (this.props.product.productUpdateAction) {
                  this.props.update_product_datasource(
                    this.props.product.datasource.map(dtsrc => {
                      if (dtsrc.id === data.id) {
                        return data;
                      }
                      return dtsrc;
                    })
                  );
                  message.success(`Product Updated`);
                  this.props.update_product_modal_visible(false);
                } else {
                  this.props.update_product_datasource([
                    ...this.props.product.datasource,
                    data
                  ]);
                  message.success(`Product Added`);
                }

                this.props.form.resetFields();
              } else if (data.errors) {
                message.error(data.errors[0].message);
              }
            },
            always: () => {
              this.setState({
                iconLoading: false
              });
            }
          });
        }
      });
    };
    render() {
      const { product } = this.props;
      const {
        getFieldDecorator,
        getFieldsError,
        getFieldError,
        isFieldTouched
      } = this.props.form;
      const productnameError = isFieldTouched("name") && getFieldError("name");
      const priceError = isFieldTouched("price") && getFieldError("price");

      return (
        <Modal
          visible={product.productModalVisible}
          title={
            product.productUpdateAction ? "Edit Product" : "Add New Product"
          }
          okText="Create"
          onCancel={this.props.update_product_modal_visible.bind(this, false)}
          footer={false}
          maskClosable={false}
        >
          <Form layout="vertical" onSubmit={this.handleSubmit}>
            <Row>
              <Col span={18}>
                {" "}
                {product.productUpdateAction && (
                  <Form.Item>
                    {" "}
                    {getFieldDecorator("id", {})(
                      <Input type="hidden" name="id" />
                    )}{" "}
                  </Form.Item>
                )}{" "}
                <Form.Item
                  label="Product Name"
                  validateStatus={productnameError ? "error" : ""}
                  help={productnameError || ""}
                >
                  {getFieldDecorator("name", {
                    rules: [
                      {
                        required: true,
                        message: "Please input product name!"
                      }
                    ]
                  })(
                    <Input
                      prefix={
                        <Icon
                          type="shopping-cart"
                          style={{
                            color: "rgba(0,0,0,.25)"
                          }}
                        />
                      }
                      placeholder="egs: Tv, Laptop, Pendrive"
                      name="name"
                      type="text"
                    />
                  )}{" "}
                </Form.Item>{" "}
                <Row gutter={2}>
                  <Col span={12}>
                    <Form.Item
                      label="Unit Price"
                      validateStatus={priceError ? "error" : ""}
                      help={priceError || ""}
                    >
                      {getFieldDecorator("price", {
                        rules: [
                          {
                            required: true,
                            message: "Please input product unit price!",
                            pattern: /^[0-9]+(\.[0-9]+)?$/
                          }
                        ]
                      })(
                        <InputNumber
                          min={1}
                          formatter={value =>
                            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          }
                          prefix={
                            <Icon
                              type="tag"
                              style={{
                                color: "rgba(0,0,0,.25)"
                              }}
                            />
                          }
                          name="price"
                        />
                      )}{" "}
                    </Form.Item>{" "}
                  </Col>{" "}
                </Row>{" "}
              </Col>{" "}
            </Row>{" "}
            <Form.Item
              style={{
                textAlign: "right"
              }}
            >
              <Button
                onClick={this.props.update_product_modal_visible.bind(
                  this,
                  false
                )}
              >
                {" "}
                Cancel{" "}
              </Button>{" "}
              <Button
                type="primary"
                htmlType="submit"
                disabled={hasErrors(getFieldsError())}
                loading={this.state.iconLoading}
              >
                {product.productUpdateAction ? "Update" : "Add"}{" "}
              </Button>{" "}
            </Form.Item>{" "}
          </Form>{" "}
        </Modal>
      );
    }
  }
);

const mapStateToProps = state => {
  return {
    product: state.product
  };
};

const mapDispatchToProps = dispatch => {
  return {
    update_product_modal_visible: value =>
      dispatch(update_product_modal_visible(value)),
    update_product_datasource: value =>
      dispatch(update_product_datasource(value))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CollectionCreateProductForm);
