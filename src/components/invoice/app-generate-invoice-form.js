import React from "react";
import {
    Form,
    Input,
    InputNumber,
    Icon,
    Checkbox,
    Row,
    Col,
    Button,
    message,
    Collapse, 
    Tag,
    Select,
} from "antd";

import { connect } from "react-redux";
import { MakeGraphQLQuery } from "../../services/utils";
import { PostDataService } from "../../services/post-data-service";
import { BASEAPIURL } from "../../env";
import {
    update_invoice_modal_visible,
    update_invoice_datasource
} from "../../redux/actions/invoiceActions";
import { fetch_products } from "../../redux/actions/productActions";

var id = 0;
var maximums = [];
const { Panel } = Collapse;
const formItemLayoutWithOutLabel = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0
        }
    }
};

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 8
        }
    },
    wrapperCol: {
        xs: {
            span: 24
        },
        sm: {
            span: 16
        }
    }
};

function hasErrors(fieldsError, fieldsKeys = []) {
    // console.log(fieldsError);
    return fieldsKeys.some(field => fieldsError[field]);
}
function calculateNewTotal(form) {
    form.setFields({
        total: {
            value: form
                .getFieldValue("prices")
                .reduce(
                    (accumulator, price) => accumulator + Number.parseFloat(price),
                    0
                )
        }
    });
}

function calculateTaxDiscountGrossTotalBalance(form, tax_rate, discount) {
    //
    let total = form.getFieldValue("total");
    tax_rate = tax_rate || form.getFieldValue("tax_rate");
    discount = discount || form.getFieldValue("discount_rate");

    let taxValue = tax_rate > 0 ? total * (tax_rate / 100) : 0;
    let discountValue = discount > 0 ? (total + taxValue) * (discount / 100) : 0;
    let gross_value = (total + taxValue - discountValue);

    form.setFieldsValue({
        total: total,
        gross_total: gross_value,
        balance: gross_value.toFixed(2)
    });
}

var productsListing;

const AppGenerateInvoiceForm = Form.create({
    name: "app_generate_invoice_form",
    onValuesChange(_, fieldObject, allValues) {
        
        if (fieldObject.products) {
            let index = fieldObject.products.length - 1;
            let product = fieldObject.products[index];
            let price;
            for (let prod of productsListing) {
                if (prod.name === product) {
                    price = fieldObject.quantity
                        ? prod.price
                        : prod.price * allValues.quantity[index];

                    
                    maximums[index] = prod.quantity;
                }
            }
            _.form.setFields({
                [`prices[${index}]`]: {
                    value: price
                }
            });

            calculateNewTotal(_.form);
            calculateTaxDiscountGrossTotalBalance(_.form);
        } else if (fieldObject.quantity) {
            let index = fieldObject.quantity.length - 1;
            let quantity = fieldObject.quantity[index];
            let price = 0;
            for (let prod of productsListing) {
                if (prod.name === allValues.products[index]) {
                    price = prod.price * quantity;
                }
            }
            _.form.setFields({
                [`prices[${index}]`]: {
                    value: price
                }
            });
            calculateNewTotal(_.form);
            calculateTaxDiscountGrossTotalBalance(_.form);
        } else if (fieldObject.prices) {
            let index = fieldObject.prices.length - 1;
            _.form.setFields({
                [`prices[${index}]`]: {
                    value: fieldObject.prices[index]
                }
            });
            calculateNewTotal(_.form);
            calculateTaxDiscountGrossTotalBalance(_.form);
        } else if (fieldObject.tax_rate || fieldObject.discount_rate) {
            calculateNewTotal(_.form);
            calculateTaxDiscountGrossTotalBalance(_.form, allValues.tax_rate, allValues.discount_rate);
        } else if (
            allValues.amount_received || allValues.amount_changed
        ) {
            
            let total = parseFloat(allValues.balance);
            let amount = parseFloat(
                allValues.amount_received - allValues.amount_changed
            ).toFixed(2);
            let diff = amount - total
            
            _.form.setFields({
                amount: {
                    value: diff >= 0 ? total + diff : diff,
                    errors: diff < 0 ? [new Error('Not enought')] : null,
                }
            });

        } else if (allValues.products && allValues.products.length > 0) {
            calculateNewTotal(_.form);
        } 
    }
})(
    class extends React.Component {
        state = {
            submitting: false,
            error: null,
            productKeys: [],
            fieldStatus: {
                amountReceived:{
                    status: "error",
                    help: "This field is required"
                },
                amount: {
                    status: "error",
                    help: "Not enough"
                }
            }
        };

        /**
       * Removes a product,quantity,price row 
       */
        remove = k => {
            const { form } = this.props;
            // can use data-binding to get
            const keys = form.getFieldValue("keys");

            // We need at least one passenger
            if (keys.length === 1) {
                return;
            }

            // can use data-binding to set
            form.setFieldsValue(
                {
                    keys: keys.filter(key => key !== k)
                },
                () => {
                    let allQuantities = form.getFieldValue("quantity");
                    let allProducts = form.getFieldValue("products");
                    calculateNewTotal(form);
                    calculateTaxDiscountGrossTotalBalance(form);

                }
            );


            // calculateTotalQuantity()
        }

        handleSubmit = e => {
            e.preventDefault();
            e.stopPropagation();
            message.info("Processing invoice...");
            this.setState({ submitting: true });
            const values = this.props.form.getFieldsValue();
            const errObj = this.props.form.getFieldsError();
            const {invoice} = this.props;
            let err = false;
            // console.log(errObj)
            for (let key in errObj){
                if (errObj[key] && errObj[key].length > 0){
                    for(let arrVal of errObj[key]){
                        if(arrVal){
                            err = true;
                            break;
                        }
                    }
                }
                else if(errObj[key]){
                    err = true;
                    break;
                }
            }
            
            if (!err) {

                let data = {};
                let products = [];

                for (let i = 0; i < values.products.length; i++) {
                    for (let prod of productsListing) {
                        if (prod.name === values.products[i]) {
                            products.push({
                                product_id: Number.parseInt(prod.id),
                                quantity: Number.parseInt(values.quantity[i])
                            });
                        }
                    }
                }
                // console.log(products)
                if (products.length < 1) {
                    message.error("No purchase details cannot be empty");
                    this.setState({ submitting: false });
                }
                if (values.amount_received - values.amount_changed < values.balance){
                    message.error("Effective amount paid is not enough.");
                    this.setState({ submitting: false });
                }
                data["products"] = products;
                data["tax_rate"] = Number.parseFloat(values.tax_rate);
                data["discount_rate"] = Number.parseFloat(values.discount_rate);
                data['amount_paid'] = parseFloat(values.amount_received);
                data['amount_changed'] = parseFloat(values.amount_changed);
                data['amount_recieved'] = parseFloat(values.amount);
                data["customer_email"] = values.customerEmail;
                data["customer_phone"] = values.customerPhone;
                data["customer_name"] = values.customerName;
                data["customer_no"] = values.customerNo ? values.customerNo : "";
                
                let queryData = MakeGraphQLQuery("mutation", "createInvoice", { data }, [
                    "id",
                    "num",
                    "customer_name",
                    "customer_no",
                    "net_total",
                    "amount_recieved",
                    "balance",
                    "is_void",
                    "created_at"
                ]);

                PostDataService(BASEAPIURL, queryData, {
                    success: data => {
                        if (data.data && data.data["createInvoice"]) {
                            data = data.data["createInvoice"];
                            this.props.update_invoice_datasource([
                                data,
                                invoice.datasource
                            ]);
                            this.props.fetch_products();
                            message.success("Invoice generated");
                            this.props.form.resetFields();
                        }
                    },
                    always: () => {
                        this.setState({ submitting: false });
                    }
                });
            } else {
                this.setState({ submitting: false });
                message.error("Invoiced invalid...");
            }
        };

        handleSetVisitingCustomer = checked => {
            this.setState({
                visitCustomer: checked,
                autoGenerateUUID: checked
            });

            if (checked) {
                this.props.form.setFields({
                    customerName: {
                        value: "visitor"
                    },
                    customerPhone: {
                        value: "0000000000"
                    }
                });
            } else {
                this.props.form.setFields({
                    customerName: {
                        value: ""
                    },
                    customerPhone: {
                        value: ""
                    }
                });
            }
        }

        componentDidMount(){
            if (!this.props.product.loading &&
                    (!this.props.product.datasource || this.props.product.datasource.length === 0)
                ) {
                this.props.fetch_products();
            }
                    
            this.handleSetVisitingCustomer(true);
            if (this.props.form.getFieldValue("keys").length < 1) this.add();
            this.props.form.validateFields();
        }

        /**
         * Adds a new product,quantity,price row dynamically
         */
        add = () => {
            let { form } = this.props;
            // can use data-binding to get
            let keys = form.getFieldValue("keys");
            let nextKeys = keys.concat(id);
            form.setFieldsValue(
                {
                    keys: nextKeys
                },
                () => id++
            );
        };

        amount_recieved_validator = (value) => {
            let total = this.props.form.getFieldValue("total");

            if (value < total) {
                this.setState({
                    fieldStatus: {
                        ...this.state.fieldStatus, amountReceived: {
                            status: "error",
                            help: "Amount recieved is less than total cost"
                        }
                    }
                })
            } else {
                this.setState({
                    fieldStatus: {
                        ...this.state.fieldStatus, amountReceived: {
                            status: "success",
                            help: ""
                        }
                    }
                });
            }
        }

        amount_validator = (value) => {
            let balance = this.props.form.getFieldValue("balance");
            let amount_recieved = this.props.form.getFieldValue("amount_recieved");
            let amount_changed = this.props.form.getFieldValue("amount_changed");
            
            let diff = (amount_recieved - amount_changed) - balance; 
            
            if (diff < 0) {
                this.setState({
                    fieldStatus: {
                        ...this.state.fieldStatus, amount: {
                            status: "error",
                            help: "Not enough"
                        }
                    }
                })
            } else {
                this.setState({
                    fieldStatus: {
                        ...this.state.fieldStatus, amount: {
                            status: "success",
                            help: ""
                        }
                    }
                });
            }
        }

        render() {
            productsListing = this.props.product.datasource;

            const {
                getFieldDecorator,
                getFieldValue,
                getFieldsError,
                getFieldError,
                isFieldTouched
            } = this.props.form;

            const nameError =
                isFieldTouched("customerName") && getFieldError("customerName");
            const emailError =
                isFieldTouched("customerEmail") && getFieldError("customerEmail");
            const phoneError =
                isFieldTouched("customerPhone") && getFieldError("customerPhone");

            getFieldDecorator("keys", {
                initialValue: []
            });

            getFieldDecorator("taxKeys", {
                initialValue: []
            });

            const keys = getFieldValue("keys");

            const productSelectItems = keys.map((k, index) => {
                return (
                    <Row key={`productsRow${k}`}>
                        <Col span={12}>
                            <Form.Item
                                {...formItemLayoutWithOutLabel}
                                key={k}
                            >
                                {getFieldDecorator(`products[${k}]`, {
                                    rules: [
                                        {
                                            required: true,
                                            message: "Please select product!"
                                        }
                                    ]
                                })(
                                    <Select
                                        placeholder="product name"
                                        type="text"
                                        style={{
                                            width: "100%",
                                            marginRight: 8
                                        }}
                                        // onChange={this.handleProductsChange.bind(this)}
                                        name="products[]"
                                    >
                                        {this.props.product.datasource && this.props.product.datasource.map((product, index) => {
                                            return (
                                                <Select.Option
                                                    value={`${product.name}`}
                                                    key={product.id}
                                                >
                                                    {product.name}
                                                </Select.Option>
                                            );
                                        })}
                                    </Select>
                                )}
                            </Form.Item>
                        </Col>

                        <Col span={6}>
                            <Form.Item
                                key={`qty${k}`}
                                
                            >
                                {getFieldDecorator(`quantity[${k}]`, {
                                    rules: [
                                        {
                                            required: true,
                                            message:'Quantity is invalid'
                                        
                                        },{
                                            validator: (rule, value, callback) => {
                                                if (value > maximums[k]) {
                                                    callback(new Error("Quantity is more than in stock"));
                                                }
                                            }
                                        }
                                    ],
                                    initialValue: 1,
                                    validateTrigger: ["onChange"]
                                    
                                })(
                                    <Input
                                        placeholder="quantity"
                                        type="number"
                                        min={1}
                                        max={maximums[k]}
                                        style={{
                                            width: "100%",
                                            marginRight: 8
                                        }}
                                        
                                        name="quantity[]"
                                    />
                                )}
                            </Form.Item>
                        </Col>

                        <Col span={4}>
                            <Form.Item
                                key={`prices[${k}]`}
                            >
                                {getFieldDecorator(`prices[${k}]`, {
                                    rules: [
                                        {
                                            required: true,
                                            message: "Invalid product price!",
                                            type: "number"
                                        }
                                    ]
                                })(
                                    <InputNumber
                                        placeholder="unit_price"
                                        min={1}
                                        style={{
                                            width: "100%",
                                            marginRight: 8
                                        }}
                                        name={`prices[]`}
                                    />
                                )}
                            </Form.Item>
                        </Col>

                        <Col span={2} style={{ padding: 10 }}>
                            {keys.length > 1 ? (
                                <Icon
                                    className="dynamic-delete-button"
                                    type="minus-circle-o"
                                    onClick={() => this.remove(k)}
                                />
                            ) : null}
                        </Col>
                    </Row>
                );
            });

            return (
                <Form layout="vertical" onSubmit={this.handleSubmit}>
                    <Row>
                        <Collapse bordered={false} defaultActiveKey={['2','3']}>
                            <Panel header={<Row><Col span={20}><span>Customer details</span></Col> { this.state.visitCustomer && <Col span={4}> <Tag color="geekblue">Visitor</Tag></Col>}</Row> } key="1">
                                <Row gutter={2}>
                                    <Col span={12}>
                                        <Form.Item
                                            label="Customer/Organization Fullname"
                                            validateStatus={nameError ? "error" : ""}
                                            help={nameError || ""}
                                        >
                                            {getFieldDecorator("customerName", {
                                                rules: [
                                                    {
                                                        required: true,
                                                        message:
                                                            "Please input customer/organization full name!"
                                                    }
                                                ]
                                            })(
                                                <Input
                                                    prefix={
                                                        <Icon
                                                            type="user"
                                                            style={{
                                                                color: "rgba(0,0,0,.25)"
                                                            }}
                                                        />
                                                    }
                                                    onChange={this.handleValueChange}
                                                    name="customerName"
                                                    type="text"
                                                />
                                            )}
                                        </Form.Item>{" "}
                                        <Form.Item
                                            label="Customer/Organization Email"
                                            validateStatus={emailError ? "error" : ""}
                                            help={emailError || ""}
                                        >
                                            {getFieldDecorator("customerEmail", {
                                                rules: [
                                                    {
                                                        message: "Please input customer email!"
                                                    }
                                                ],
                                                initialValue: ""
                                            })(
                                                <Input
                                                    prefix={
                                                        <Icon
                                                            type="mail"
                                                            style={{
                                                                color: "rgba(0,0,0,.25)"
                                                            }}
                                                        />
                                                    }
                                                    onChange={this.handleValueChange}
                                                    name="customerEmail"
                                                    type="email"
                                                />
                                            )}
                                        </Form.Item>{" "}
                                        <Form.Item
                                            label="Customer/Organization Phone"
                                            validateStatus={phoneError ? "error" : ""}
                                            help={phoneError || ""}
                                        >
                                            {getFieldDecorator("customerPhone", {
                                                rules: [
                                                    {
                                                        required: true,
                                                        message: "Please input customer phone number!",
                                                        pattern: /^[0-9]+$/
                                                    }
                                                ]
                                            })(
                                                <Input
                                                    prefix={
                                                        <Icon
                                                            type="phone"
                                                            rotate={100}
                                                            style={{
                                                                color: "rgba(0,0,0,.25)"
                                                            }}
                                                        />
                                                    }
                                                    onChange={this.handleValueChange}
                                                    name="customerPhone"
                                                    type="tel"
                                                />
                                            )}
                                        </Form.Item>
                                    </Col>
                                    <Col span={12}>
                                        <Row>
                                            <Col span={10}>
                                                <Form.Item label="Customer ID">
                                                    {getFieldDecorator("customerNo", {})(
                                                        <Input
                                                            disabled={this.state.autoGenerateUUID}
                                                            onChange={this.handleValueChange}
                                                            name="customerNo"
                                                            type="text"
                                                        />
                                                    )}
                                                </Form.Item>
                                            </Col>
                                            <Col span={14}>
                                                <Form.Item label="Auto-generate UUID">
                                                    {getFieldDecorator("autoGenUUID", {
                                                        defaultChecked: true
                                                    })(
                                                        <Checkbox
                                                            checked={this.state.autoGenerateUUID}
                                                            id="autoGenUUID"
                                                            style={{ marginLeft: "20px" }}
                                                            name="autoGenUUID"
                                                            size="small"
                                                            onChange={e =>
                                                                this.setState({
                                                                    autoGenerateUUID: e.target.checked
                                                                })
                                                            }
                                                        />
                                                    )}
                                                </Form.Item>
                                            </Col>
                                        </Row>
                                        <Col span={14}>
                                            Visiting customer:{" "}
                                            <Checkbox
                                                name="visitor"
                                                checked={this.state.visitCustomer}
                                                size="small"
                                                onChange={e => {
                                                    this.handleSetVisitingCustomer(e.target.checked);
                                                }}
                                            />
                                        </Col>
                                    </Col>
                                </Row>
                            </Panel>
                            <Panel header="Purchase Details" key="2">
                                <Row>
                                    {productSelectItems}

                                    <Col span={6} offset={16}>
                                        <Form.Item {...formItemLayout} label="Total">
                                            {getFieldDecorator(`total`, {
                                                initialValue: 0
                                            })(<Input disabled={true} type="number" />)}
                                        </Form.Item>
                                    </Col>

                                    <Form.Item {...formItemLayoutWithOutLabel}>
                                        <Button
                                            type="dashed"
                                            onClick={this.add}
                                            style={{
                                                width: "60%"
                                            }}
                                        >
                                            <Icon type="plus" /> Add field{" "}
                                        </Button>
                                    </Form.Item>
                                </Row>
                            </Panel>
                            <Panel header="Transaction" key="3">
                                <Row gutter={1} >
                                    <Col span={8}>
                                        <Form.Item
                                            label="Received"
                                        >
                                            {getFieldDecorator('amount_received', {
                                                initialValue: 0,
                                                rules : [{
                                                    required: true,
                                                }]
                                            })(
                                            <InputNumber
                                                size="large"
                                                style={{
                                                    width: 150
                                                }}
                                                required={true}
                                                min={0}
                                                formatter={value =>
                                                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                                                }
                                                
                                                name="amount_received"
                                            />)}
                                        </Form.Item>{" "}
                                    </Col>
                                    <Col span={8}>
                                        <Form.Item
                                            label="Change"
                                        >
                                            {getFieldDecorator('amount_changed', {
                                                initialValue: 0
                                            })(
                                            <InputNumber
                                                size="large"
                                                style={{
                                                    width: 150
                                                }}
                                                min={0}
                                                formatter={value =>
                                                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                                                }
                                                onChange={this.amount_validator}
                                                name="amount_changed"
                                            />)}
                                        
                                        </Form.Item>{" "}
                                    </Col>
                                    <Col span={8}>
                                        <Form.Item 
                                            label="Amount"
                                            >
                                            {getFieldDecorator('amount', {
                                                initialValue: 0
                                            })(
                                            <InputNumber
                                                size="large"
                                                style={{ width: 150 }}
                                                disabled={true}
                                                formatter={value =>
                                                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                                                }
                                                name="amount"
                                            />)}
                                        </Form.Item>{" "}
                                    </Col>
                                </Row>
                            </Panel>
                            <Panel header="Taxation" key="4">
                                <Row >
                                    <Col span={20}>
                                        <Form.Item {...formItemLayout} label="Tax Rate">
                                            {getFieldDecorator(`tax_rate`, {
                                                initialValue: 0
                                            })(<Input type="number" />)}
                                        </Form.Item>
                                        <Form.Item {...formItemLayout} label="Discount rate">
                                            {getFieldDecorator(`discount_rate`, {
                                                initialValue: 0
                                            })(
                                                <Input
                                                    name="discount_rate"
                                                    type="number"
                                                    step={0.1}
                                                    min={0}
                                                />
                                            )}
                                        </Form.Item>
                                        <Form.Item {...formItemLayout} label="Gross Total">
                                            {getFieldDecorator(`gross_total`, {
                                                initialValue: 0
                                            })(<Input disabled={true} type="number" />)}
                                        </Form.Item>

                                        <Form.Item {...formItemLayout} label="Balance">
                                            {getFieldDecorator(`balance`, {
                                                initialValue: 0
                                            })(<Input disabled={true} type="number" />)}
                                        </Form.Item>
                                    </Col>
                                    <Col span={4} />
                                </Row>
                            </Panel>
                        </Collapse>
                    </Row>
                    <Row style={{ marginTop: "10px" }}>
                        <Col span={12}>
                            <Form.Item>
                                <Button
                                    type="default"
                                    htmlType="reset"
                                    onClick={this.props.update_invoice_modal_visible.bind(
                                        this,
                                        false
                                    )}
                                >
                                    Cancel
                                </Button>
                            </Form.Item>
                        </Col>
                        <Col span={12} style={{ textAlign: "right" }}>
                            <Form.Item>
                                <Button
                                    type="primary"
                                    htmlType="submit"
                                    loading={this.state.submitting}
                                >
                                    Submit
                                </Button>
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            );
        }
    }
);

const mapStateToProps = state => {
    return {
        stock: state.stock,
        product: state.product,
        invoice: state.invoice
    };
};

const mapDispatchToProps = dispatch => {
    return {
        update_invoice_datasource: (values) => dispatch(update_invoice_datasource(values)),
        update_invoice_modal_visible: (value) => dispatch(update_invoice_modal_visible(value)),
        fetch_products: () => dispatch(fetch_products(dispatch))
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AppGenerateInvoiceForm);
