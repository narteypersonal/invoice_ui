import React, { Component } from "react";
import { Row, Col, Table, Divider, Tag } from "antd";
import { connect } from "react-redux";

class AppInvoiceDetail extends Component {
  state = {
    loading: false,
    columns: [
      {
        title: "Product",
        dataIndex: "product.name",
        key: "product.name"
      },
      {
        title: "Quantity",
        dataIndex: "quantity",
        key: "quantity"
      },
      {
        title: "Sub Total",
        dataIndex: "sub_total",
        key: "sub_total",
        render: (text, record) => <span>{record.sub_total.toFixed(2)}</span>
      }
    ],
    steps: [
      {
        title: "Invoice Detail",
        hidden: true
      },
      {
        title: "Customer Detail",
        hidden: false
      }
    ]
  };
  render = () => {
    const datasource = this.props.invoice.invoiceDetailDataSource;

    return (
      <div>
        <Row>
          <Col span={12}>
            <span>#: {datasource.num}</span>
          </Col>
          <Col span={12} style={{ textAlign: "right" }}>
            <span>
              Issued: {new Date(datasource.created_at).toLocaleDateString()}
            </span>
          </Col>
        </Row>
        <Row>
          <Col span={8}>
            Validity:{" "}
            {datasource.is_void ? (
              <Tag color="red">Void</Tag>
            ) : (
              <Tag color="green">Valid</Tag>
            )}
          </Col>
          <Col span={8}>
            Recieved:{" "}
            {datasource.amount_recieved &&
              datasource.amount_recieved.toFixed(2)}
          </Col>
          <Col span={8}>
            Balance:{" "}
            {datasource.balance > 0 ? (
              <b style={{ color: "red" }}>{datasource.balance.toFixed(2)}</b>
            ) : (
              <span>{datasource.balance && datasource.balance.toFixed(2)}</span>
            )}
          </Col>
        </Row>
        <Row>
          <Table
            bordered
            size="small"
            loading={this.state.loading}
            columns={this.state.columns}
            dataSource={datasource.listProducts}
          />
          <Col span={11} offset={13}>
            <b>Discount</b>
            <Divider type="vertical" />
            <span>
              {datasource.discount_total &&
                datasource.discount_total.toFixed(2)}
            </span>
          </Col>
          <Col span={10} offset={14}>
            <b>Tax</b>
            <Divider type="vertical" />
            <span>
              {datasource.tax_total && datasource.tax_total.toFixed(2)}
            </span>
          </Col>
          <Col span={10} offset={14}>
            <b>Total</b>
            <Divider type="vertical" />
            <span>
              {datasource.net_total && datasource.net_total.toFixed(2)}
            </span>
          </Col>
        </Row>
      </div>
    );
  };
}

const mapStateToProps = state => {
  return {
    invoice: state.invoice
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppInvoiceDetail);
