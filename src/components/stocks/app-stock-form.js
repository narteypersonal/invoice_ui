import React from "react";
import {
    Form,
    Row,
    Input,
    Col,
    Button,
    Alert,
    message,
    Select,
    Descriptions,
    Icon
  } from "antd";

import { connect } from "react-redux";
import { MakeGraphQLQuery } from "../../services/utils";
import { PostDataService } from "../../services/post-data-service";
import { BASEAPIURL } from "../../env";
import { update_stock_drawer_visible, update_stock_datasource } from "../../redux/actions/stockActions";

const formItemLayoutWithOutLabel = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0
    }
  }
};

function hasErrors(fieldsError, fieldsKeys = []) {
  // console.log(fieldsError);
  return fieldsKeys.some(field => fieldsError[field]);
}

  /**
     * Adds a new product,quantity,price row dynamically
     */
   function add(props) {
      let { form } = props;
      // can use data-binding to get
      let keys = form.getFieldValue("keys");
      let nextKeys = keys.concat(keys.length);
      
      // important! notify form to detect changes

      form.setFieldsValue(
        {
          keys: nextKeys
        }
      );

      // console.log(nextKeys);
    }


const AppAddStock = Form.create({ name: "app_add_stock",
onValuesChange(_, fieldObject, allValues) {
  _.form.validateFields();
  if(fieldObject.quantity || fieldObject.products){
  
    for (let key of allValues.keys){
      //check if all fields are filled for each row
      if(allValues.products[key] && allValues.quantity[key] && (key === (allValues.keys.length - 1))){
        add(_);
      }
    }
  }
   
}
 })(
    class extends React.Component {
      state = { 
        submitting: false,
        error: null
      };
       
      /**
     * Removes a product,quantity,price row 
     */
    remove = k => {
      const { form } = this.props;
      // can use data-binding to get
      const keys = form.getFieldValue("keys");
      
      // We need at least one passenger
      if (keys.length === 1) {
        return;
      }

      // can use data-binding to set
      form.setFieldsValue(
        {
          keys: keys.filter(key => key !== k)
        },
        ()=>{
          let allQuantities = form.getFieldValue("quantity");
          let allProducts = form.getFieldValue("products");
          
        }
      );
      
      
      // calculateTotalQuantity()
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({error: null});
        this.props.form.validateFields((err, values) => {
          if (!err) {
            this.setState({ submitting: true });
  
            let data = {};
            let products = [];
            
            for(let key of values.keys){
              let product = parseInt(values.products[key]);
              let quantity = values.quantity[key] ? parseFloat(values.quantity[key]):0;
              if( product  && quantity){
                products.push({
                  product_id: product,quantity
                })
              }
            }

            if(! products.length ){
              this.setState({
                submitting: false,
                error: "You must add at least one product to stock"
              });
            }
            data["products"] = products;

            // console.log(data);
            // return;
            
  
            let queryName = "addStock";
            let query = MakeGraphQLQuery("mutation", queryName,{data}, [
              "id",
              "product{id name}",
              "quantity",
              "created_at",
              "updated_at"
            ]);

            // console.log(query);

            PostDataService(BASEAPIURL, query, {
              success: data => {
                if (data.data[queryName]) {
                  // console.log(data.data[queryName])
                  data = data.data[queryName].map(data => {
                    data["key"] = data.id;
                    return data;
                  });
                  console.log([...data, ...this.props.stock.datasource])
                  this.props.update_stock_datasource([...data, ...this.props.stock.datasource]);
                  message.success(`Stock successfully added`);
                  this.props.form.resetFields();
                } else if (data.errors) {
                  message.error(data.errors[0].message);
                }
              },
              always: () => {
                this.setState({ submitting: false });
              }
            });
          }
        });
      };
      
    render() {
      const {
        getFieldDecorator,
        getFieldValue,
        getFieldsError,
        getFieldError,
        isFieldTouched
      } = this.props.form;
      getFieldDecorator("keys", {
        initialValue: [0]
      });
      const keys = getFieldValue("keys");
      const formItems = keys.map((k, index) => {
        let productNameError =
          isFieldTouched(`products[${k}]`) && getFieldError(`products[${k}]`);
        let qtyNameError =
          isFieldTouched(`quantity[${k}]`) && getFieldError(`quantity[${k}]`);

        return (
          <Row key={`productsRow${k}`}>
            <Col span={12}>
              <Form.Item
                {...formItemLayoutWithOutLabel}
                validateStatus={productNameError ? "error" : ""}
                help={productNameError || ""}
                required={false}
                key={k}
              >
                {getFieldDecorator(`products[${k}]`, {
                  rules: [
                    {
                      
                      message: "Please select product!"
                    }
                  ]
                })(
                    <Select
                        showSearch
                        placeholder="product"
                        type="text"
                        filterOption={(input, option) =>
                          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                        style={{
                            width: "100%",
                            marginRight: 8
                        }}
                        // onChange={this.handleProductsChange.bind(this)}
                        name="products[]"
                    >
                        {this.props.product.datasource &&
                            this.props.product.datasource.map((product, index) => {
                                return (
                                    <Select.Option value={product.id} key={product.id}>
                                        {product.name}
                                    </Select.Option>
                                );
                            })}
                    </Select>

                    
                )}
              </Form.Item>
            </Col>

            <Col span={6}>
              <Form.Item
                key={`qty${k}`}
                validateStatus={qtyNameError ? "error" : ""}
                help={qtyNameError || ""}
                required={false}
              >
                {getFieldDecorator(`quantity[${k}]`, {
                  rules: [
                    {
                      required: true,
                      message: "Please input product quantity!"
                    }
                  ],
                  initialValue: 1
                })(
                  <Input
                    placeholder="quantity"
                    type="number"
                    min={1}
                    style={{
                      width: "100%",
                      marginRight: 8
                    }}
                    name="quantity[]"
                  />
                )}
              </Form.Item>
            </Col>

            <Col span={2} style={{ padding: 10 }}>
              {keys.length > 1 ? (
                <Icon
                  className="dynamic-delete-button"
                  type="minus-circle-o"
                  onClick={() => this.remove(k)}
                />
              ) : null}
            </Col>
          </Row>
        );
      });
      return (
        <Form layout="vertical" onSubmit={this.handleSubmit}>
         
          <Row style={{maxHeight: "40vh", backgroundColor: "#ccc",overflowY: "scroll", paddingTop:"10px", paddingLeft: "5px"}}>
            {formItems}
            <Form.Item {...formItemLayoutWithOutLabel}>
                <Button
                  
                  type="dashed"
                  onClick={add.bind(this,this.props)}
                  style={{
                    width: "60%"
                  }}
                >
                  <Icon type="plus" /> Add field{" "}
                </Button>
              </Form.Item>
          </Row>
          {this.state.error && <Row>
            <Alert
              message={`${this.state.error}`}
              type="error"
              showIcon
            />
          </Row>

          }
          <Row style={{marginTop: "10px"}}>
            <Col span={12}>
              <Form.Item>
                <Button
                  type="default"
                  htmlType="reset"
                  onClick={this.props.update_stock_drawer_visible.bind(
                    this,
                    false
                  )}
                >
                  Cancel
                </Button>
              </Form.Item>
            </Col>
            <Col span={12} style={{ textAlign: "right" }}>
              <Form.Item>
                <Button
                  
                  type="primary"
                  htmlType="submit"
                  loading={this.state.submitting}
                >
                  Submit
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      );
    }
    }
  );

const mapStateToProps = state => {
    return {
      stock: state.stock,
      product: state.product
    };
  };
  
  const mapDispatchToProps = dispatch => {
   return {
       update_stock_drawer_visible: (value) => dispatch(update_stock_drawer_visible(value)),
       update_stock_datasource: (values) => dispatch(update_stock_datasource(values))
      }
  };

  export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AppAddStock);
