import React, { Component } from "react";
import {
    Table,
    Button,
    Card,
    Row,
    Col,
    Input,
    Icon
} from "antd";
import Highlighter from "react-highlight-words";
import { connect } from "react-redux";
import { fetch_products } from "../../redux/actions/productActions";

class AppStockBalanceTable extends Component {
    getColumnSearchProps = dataIndex => ({
        filterDropdown: ({
            setSelectedKeys,
            selectedKeys,
            confirm,
            clearFilters
        }) => (
                <div
                    style={{
                        padding: 8
                    }}
                >
                    <Input
                        ref={node => {
                            this.searchInput = node;
                        }}
                        placeholder={`Search ${dataIndex}`}
                        value={selectedKeys[0]}
                        onChange={e =>
                            setSelectedKeys(e.target.value ? [e.target.value] : [])
                        }
                        onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
                        style={{
                            width: 188,
                            marginBottom: 8,
                            display: "block"
                        }}
                    />{" "}
                    <Button
                        type="primary"
                        onClick={() => this.handleSearch(selectedKeys, confirm)}
                        icon="search"
                        size="small"
                        style={{
                            width: 90,
                            marginRight: 8
                        }}
                    >
                        Search{" "}
                    </Button>{" "}
                    <Button
                        onClick={() => this.handleReset(clearFilters)}
                        size="small"
                        style={{
                            width: 90
                        }}
                    >
                        Reset{" "}
                    </Button>{" "}
                </div>
            ),
        filterIcon: filtered => (
            <Icon
                type="search"
                style={{
                    color: filtered ? "#1890ff" : undefined
                }}
            />
        ),
        onFilter: (value, record) =>
            record[dataIndex]
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select());
            }
        },
        render: text => (
            <Highlighter
                highlightStyle={{
                    backgroundColor: "#ffc069",
                    padding: 0
                }}
                searchWords={[this.state.searchText]}
                autoEscape
                textToHighlight={text.toString()}
            />
        )
    });

    state = {
        datasource: [],
        loading: false,
        filteredDatasource: null,
        columns: [
            {
                title: "Product",
                dataIndex: "name",
                key: "name",
                width: "20%",
                ...this.getColumnSearchProps('name')
            },
            {
                title: "Sold",
                dataIndex: "quantity_sold",
                key: "quantity_sold",
                render: (text,record) => (
                    (parseFloat(text) > 0 ) ? <b style={{color: "green"}}>{text}</b> : text 
                ),
                defaultSortOrder: 'descend',
                sorter: (a, b) => a.quantity_sold - b.quantity_sold
            },
            {
                title: "In Stock",
                dataIndex: "quantity",
                key: "quantity",
                defaultSortOrder: 'descend',
                sorter: (a, b) => a.quantity - b.quantity
            }
        ]
    };

    handleSearch = (selectedKeys, confirm) => {
        if (!selectedKeys) {

            this.setState({
                searchText: "",
                filteredDatasource: null
            });
            return
        }
        let pattern = new RegExp(`${selectedKeys}`, 'gi');
        let result = this.props.product.datasource.filter((product) =>
            product.name.match(pattern))
        this.setState({
            searchText: selectedKeys[0],
            filteredDatasource: result
        });
    };

    handleReset = clearFilters => {
        clearFilters();
        this.setState({
            searchText: ""
        });
    };

    
    componentWillMount() {
        if (
            !this.props.product.loading &&
            (!this.props.product.datasource || this.props.product.datasource.length === 0)
        ) {
            this.props.fetch_products();
        }
    }

    render() {
        const { product } = this.props;
        return (
            <Card>
                <Table
                    bordered
                    size="small"
                    loading={product.loading}
                    title={() => (
                        <Row>
                            <Col span={9}>
                                <h1> Stock Balance </h1>{" "}
                            </Col>
                            <Col span={15} id="dropdown-container">
                                <Input.Search
                                    disabled={this.state.loading}
                                    suffix={this.state.loading ? <Icon type="loading" /> : ""}
                                    placeholder="search product name"
                                    onSearch={value => this.handleSearch(value)}
                                    onChange={(e) => this.handleSearch(e.target.value)}
                                    style={{ width: 500 }}
                                />
                                
                                <Button
                                    style={{ marginLeft: "6px" }}
                                    onClick={this.props.fetch_products}
                                    type="primary"
                                    icon="reload"
                                />
                            </Col>
                        </Row>
                    )}
                    columns={this.state.columns}
                    dataSource={this.state.filteredDatasource || product.datasource}
                />
            </Card>
        );
    }
}

const mapStateToProps = state => {
    return {
        product: state.product
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetch_products: () => dispatch(fetch_products(dispatch)),
    };
};
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AppStockBalanceTable);
