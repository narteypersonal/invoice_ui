import React, { Component } from "react";
import {
    Table,
    Button,
    Card,
    Row,
    Col,
    Input,
    Icon
} from "antd";
import Highlighter from "react-highlight-words";
import { connect } from "react-redux";
import { fetch_stock, update_stock_datasource } from "../../redux/actions/stockActions";

class AppStocksTable extends Component {
    getColumnSearchProps = dataIndex => ({
        filterDropdown: ({
            setSelectedKeys,
            selectedKeys,
            confirm,
            clearFilters
        }) => (
                <div
                    style={{
                        padding: 8
                    }}
                >
                    <Input
                        ref={node => {
                            this.searchInput = node;
                        }}
                        placeholder={`Search ${dataIndex}`}
                        value={selectedKeys[0]}
                        onChange={e =>
                            setSelectedKeys(e.target.value ? [e.target.value] : [])
                        }
                        onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
                        style={{
                            width: 188,
                            marginBottom: 8,
                            display: "block"
                        }}
                    />{" "}
                    <Button
                        type="primary"
                        onClick={() => this.handleSearch(selectedKeys, confirm)}
                        icon="search"
                        size="small"
                        style={{
                            width: 90,
                            marginRight: 8
                        }}
                    >
                        Search{" "}
                    </Button>{" "}
                    <Button
                        onClick={() => this.handleReset(clearFilters)}
                        size="small"
                        style={{
                            width: 90
                        }}
                    >
                        Reset{" "}
                    </Button>{" "}
                </div>
            ),
        filterIcon: filtered => (
            <Icon
                type="search"
                style={{
                    color: filtered ? "#1890ff" : undefined
                }}
            />
        ),
        onFilter: (value, record) =>
            record[dataIndex]
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select());
            }
        },
        render: text => (
            <Highlighter
                highlightStyle={{
                    backgroundColor: "#ffc069",
                    padding: 0
                }}
                searchWords={[this.state.searchText]}
                autoEscape
                textToHighlight={text.toString()}
            />
        )
    });

    state = {
        datasource: [],
        loading: false,
        filteredDatasource:null,
        columns: [
            {
                title: "Product",
                dataIndex: "name",
                key: "name",
                width: "20%",
                filterDropdown: ({
                    setSelectedKeys,
                    selectedKeys,
                    confirm,
                    clearFilters
                }) => (
                        <div
                            style={{
                                padding: 8
                            }}
                        >
                            <Input
                                ref={node => {
                                    this.searchInput = node;
                                }}
                                placeholder={`Search receipt`}
                                value={selectedKeys[0]}
                                onChange={e =>
                                    setSelectedKeys(e.target.value ? [e.target.value] : [])
                                }
                                onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
                                style={{
                                    width: 188,
                                    marginBottom: 8,
                                    display: "block"
                                }}
                            />{" "}
                            <Button
                                type="primary"
                                onClick={() => this.handleSearch(selectedKeys, confirm)}
                                icon="search"
                                size="small"
                                style={{
                                    width: 90,
                                    marginRight: 8
                                }}
                            >
                                Search{" "}
                            </Button>{" "}
                            <Button
                                onClick={() => this.handleReset(clearFilters)}
                                size="small"
                                style={{
                                    width: 90
                                }}
                            >
                                Reset{" "}
                            </Button>{" "}
                        </div>
                    ),
                filterIcon: filtered => (
                    <Icon
                        type="search"
                        style={{
                            color: filtered ? "#1890ff" : undefined
                        }}
                    />
                ),
                onFilter: (value, record) =>
                    record["product"]["name"]
                        .toString()
                        .toLowerCase()
                        .includes(value.toLowerCase()),
                onFilterDropdownVisibleChange: visible => {
                    if (visible) {
                        setTimeout(() => this.searchInput.select());
                    }
                },
                render: (text, record) => (
                    <div>
                        {record.product.name}
                    </div>
                )
            },
            {
                title: "Stocked",
                dataIndex: "quantity",
                key: "quantity",
                defaultSortOrder: ['descend','ascend'],
                sorter: (a, b) => a.quantity - b.quantity
            },
            {
                title: "Stocked Date",
                dataIndex: "created_at",
                key: "created_at",
                render: (text, record) => (
                    <div>
                        {(new Date(record.created_at)).toDateString()}
                    </div>
                )
            },
            {
                title: "Action",
                dataIndex: "action",
                key: "action",
                render: (text, record) => (
                    <span>
                        {record.is_void ? (
                            <Button
                               
                                shape="circle"
                                size="small"
                                icon="check-square"
                                type="success"
                                loading={
                                    false
                                }
                                onClick={() => { }}
                            />
                        ) : (
                                <Button
                                    style={{
                                        marginLeft: "2vw"
                                    }}
                                    shape="circle"
                                    size="small"
                                    icon="close-square"
                                    type="danger"
                                    loading={
                                       false
                                    }
                                    onClick={() => { }}
                                />
                            )}
                       
                    </span>
                )
            }
        ]
    };
    handleSearch = (selectedKeys, confirm) => {
        if(!selectedKeys){
            
            this.setState({
                searchText: "",
                filteredDatasource: null
            });
            return
        }
        let pattern = new RegExp(`${selectedKeys}`,'gi');
        let result =this.props.stock.datasource.filter((stock) =>
            stock.product.name.match(pattern))
        this.setState({
            searchText: selectedKeys[0],
            filteredDatasource: result
        });
    };

    handleReset = clearFilters => {
        clearFilters();
        this.setState({
            searchText: ""
        });
    };

    
    componentWillMount() {
        if (
            !this.props.stock.loading &&
            this.props.stock.datasource.length === 0
        ) {
            this.props.fetch_stock();
        }
    }

    render() {
        const { stock } = this.props;
        return (
            <Card>
                <Table
                    bordered
                    size="small"
                    loading={stock.loading}
                    title={() => (
                        <Row>
                            <Col span={9}>
                                <h1> Stock Listing </h1>{" "}
                            </Col>
                            <Col span={15} id="dropdown-container">
                                <Input.Search
                                    disabled={this.state.loading}
                                    suffix={this.state.loading ? <Icon type="loading" /> : ""}
                                    placeholder="search product name"
                                    onSearch={value => this.handleSearch(value)}
                                    style={{ width: 500 }}
                                    onChange={(e) => this.handleSearch(e.target.value)}
                                />
                                
                                <Button
                                    style={{ marginLeft: "6px" }}
                                    onClick={this.props.fetch_stock}
                                    type="primary"
                                    icon="reload"
                                />
                            </Col>
                        </Row>
                    )}
                    columns={this.state.columns}
                    dataSource={this.state.filteredDatasource || stock.datasource}
                />
            </Card>
        );
    }
}

const mapStateToProps = state => {
    return {
        stock: state.stock
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetch_stock: () => dispatch(fetch_stock(dispatch)),
        update_stock_datasource: (values) => dispatch(update_stock_datasource(values))
    };
};
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AppStocksTable);
