import React, { Component } from "react";
import { Form, Input, Button, Card, message } from "antd";
import { BASEAPIURL, API_URL } from "../env";
import { MakeGraphQLQuery } from "../services/utils";
import { PostDataService } from "../services/post-data-service";
import {
  update_profile_password_mismatch,
  update_profile_password_resetable
} from "../redux/actions/profileActions";
import { connect } from "react-redux";
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 16
    }
  }
};

const AppPasswordResetForm = Form.create({
  name: "password_reset_form",
  onValuesChange(_, fieldObject, allValues) {
    //checking new password and confirm password match
    if (
      (allValues.conf_password === undefined ||
        allValues.conf_password === "") &&
      (allValues.new_password === undefined || allValues.new_password === "")
    ) {
      _.update_profile_password_mismatch(false);
    } else if (
      fieldObject.new_password &&
      fieldObject.new_password !== allValues.conf_password
    ) {
      _.update_profile_password_mismatch(true);
    } else if (
      fieldObject.conf_password &&
      fieldObject.conf_password !== allValues.new_password
    ) {
      _.update_profile_password_mismatch(true);
    } else {
      _.update_profile_password_mismatch(false);
    }
    if (allValues.old_password === undefined || allValues.old_password === "")
      _.update_profile_password_resetable(false);
    else _.update_profile_password_resetable(true);
  }
})(
  class PasswordResetForm extends Component {
    constructor(props) {
      super(props);
      this.state = {
        passwordMismatch: false
      };
    }

    handleSubmit = e => {
      e.preventDefault();
      console.log("submiting");
      this.props.form.validateFields((err, values) => {
        if (!err) {
          let dataObj = {};
          for (let key in values) {
            dataObj[key] = values[key] ? values[key] : "";
          }
          this.setState({
            disabledInputs: true
          });

          let data = dataObj;
          data["customProvider"] = "staff";
          data["username"] = `${this.props.profile.email}`;
          data = {
            data
          };
          let queryName = "";
          let query = "";

          queryName = "changePassword";
          query = MakeGraphQLQuery("mutation", queryName, data, [
            "name",
            "email",
            "phone"
          ]);

          // console.log(query);
          // return;
          PostDataService(BASEAPIURL, query, {
            success: data => {
              if (data.data[queryName]) {
                message.success("Password changed");
                this.props.form.resetFields();
              } else if (data.errors) {
                message.error(data.errors[0].message);
              }
            },
            always: () => {
              // this.setState({
              //   disabledInputs: false
              // });
            }
          });
        }
      });
    };

    render() {
      var { getFieldDecorator } = this.props.form;

      return (
        <Card title="Reset Password" bordered={false}>
          <Form
            {...formItemLayout}
            onSubmit={this.handleSubmit}
            className="reset-password-form"
          >
            <Form.Item label="New password">
              {" "}
              {getFieldDecorator("new_password", {
                rules: [
                  {
                    required: true,
                    message: "Please input your password!"
                  }
                ]
              })(
                <Input placeholder="Enter new password" type="password" />
              )}{" "}
            </Form.Item>{" "}
            <Form.Item
              label="Confirm password"
              validateStatus={
                this.props.profile.passwordMismatch ? "error" : ""
              }
              help={
                this.props.profile.passwordMismatch
                  ? "Password does not match"
                  : ""
              }
            >
              {" "}
              {getFieldDecorator("conf_password", {
                rules: [
                  {
                    required: true,
                    message: "Enter new password again."
                  }
                ]
              })(
                <Input placeholder="Confirm new password" type="password" />
              )}{" "}
            </Form.Item>{" "}
            <Form.Item label="password">
              {" "}
              {getFieldDecorator("password", {
                rules: [
                  {
                    required: true,
                    message: "Enter your current password."
                  }
                ]
              })(<Input placeholder="Provide password" type="password" />)}{" "}
            </Form.Item>{" "}
            <Form.Item>
              <Button
                disabled={
                  false
                  // this.props.profile.passwordResetable
                  //   ? !this.props.profile.passwordMismatch
                  //     ? false
                  //     : true
                  //   : !this.props.profile.passwordMismatch
                  //   ? this.props.profile.passwordResetable
                  //     ? false
                  //     : true
                  //   : true
                }
                className="login-form-button"
                htmlType="submit"
              >
                change password{" "}
              </Button>{" "}
            </Form.Item>{" "}
          </Form>
        </Card>
      );
    }
  }
);

const mapStateToProps = state => {
  return {
    profile: state.profile
  };
};

const mapDispatchToProps = dispatch => {
  return {
    update_profile_password_mismatch: value =>
      dispatch(update_profile_password_mismatch(value)),
    update_profile_password_resetable: value =>
      dispatch(update_profile_password_resetable(value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppPasswordResetForm);
