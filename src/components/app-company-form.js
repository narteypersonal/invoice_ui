import React, { Component } from "react";
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Upload,
  Icon,
  message,
  Modal
} from "antd";
import { BASEAPIURL, API_URL } from "../env";
import { MakeGraphQLQuery } from "../services/utils";
import { PostDataService } from "../services/post-data-service";
import { connect } from "react-redux";
import {
  fetch_settings,
  update_settings_datasource,
  update_settings_loading
} from "../redux/actions/settingActions";

const FormItem = Form.Item;
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 16
    }
  }
};

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

const AppCompanyForm = Form.create({
  name: "company_detail_form",
  mapPropsToFields(props) {
    return {
      company_name: Form.createFormField({
        value: props.settings.details.company_name
      }),
      company_phone: Form.createFormField({
        value: props.settings.details.company_phone
      }),
      company_address: Form.createFormField({
        value: props.settings.details.company_address
      }),
      company_email: Form.createFormField({
        value: props.settings.details.company_email
      })
    };
  }
})(
  class CompanyDetailForm extends Component {
    constructor(props) {
      super(props);
      this.state = {
        disabledInputs: true,
        previewVisible: false,
        previewImage: "",
        fileList: []
      };
    }

    handleEditClicked = () => {
      this.setState({
        disabledInputs: false
      });
    };

    handleResetClicked = () => {
      this.setState({
        disabledInputs: true
      });
    };

    handleCancel = () =>
      this.setState({
        previewVisible: false
      });

    handlePreview = async file => {
      if (!file.url && !file.preview) {
        file.preview = await getBase64(file.originFileObj);
      }

      this.setState({
        previewImage: file.url || file.preview,
        previewVisible: true
      });
    };
    handleChange = ({ fileList }) => {
      this.setState({
        fileList
      });
      if (!this.state.fileList.length > 0) return;
      let base64 = getBase64(this.state.fileList[0].originFileObj);
      base64.then(value => {
        this.props.form.setFields({
          fileData: {
            value
          }
        });
      });
    };

    handleSubmit = e => {
      e.preventDefault();
      if (this.state.disabledInputs) return;
      this.props.form.validateFields((err, values) => {
        if (!err) {
          this.props.update_settings_loading(true);
          let dataObj = {};
          for (let key in values) {
            if (key === "upload") continue;
            dataObj[key] = values[key] ? values[key] : "";
          }
          this.setState({
            disabledInputs: true
          });
          console.log(values);
          let data = dataObj;
          data = {
            data
          };
          let queryName = "";
          let query = "";

          queryName = "setSetting";
          query = MakeGraphQLQuery("mutation", queryName, data, [
            "company_name",
            "company_email",
            "company_phone",
            "company_address"
          ]);

          console.log(query);
          PostDataService(BASEAPIURL, query, {
            success: data => {
              if (data.data[queryName]) {
                this.props.update_settings_datasource(data.data[queryName]);
                message.success("Company details changed");
              } else if (data.errors) {
                message.error(data.errors[0].message);
              }
            },
            always: () => {
              this.props.update_settings_loading(false);

              this.setState({
                disabledInputs: false
              });
            }
          });
        }
      });
    };

    componentDidMount() {
      if (
        !this.props.settings.loading &&
        this.props.settings.details.length < 1
      ) {
        this.props.fetch_settings();
      }
    }

    render() {
      var { getFieldDecorator } = this.props.form;
      const { previewVisible, previewImage, fileList } = this.state;

      // this.props.handleDefaultImage.apply(this);
      const uploadButton = (
        <div>
          <Icon type="plus" />
          <div className="ant-upload-text"> Upload </div>{" "}
        </div>
      );
      return (
        <Form {...formItemLayout} onSubmit={this.handleSubmit}>
          <Form.Item label="Upload" extra="Company Logo">
            {" "}
            {getFieldDecorator("fileData", {
              rules: [
                {
                  message: "no image selected"
                }
              ]
            })(<Input type="text" name="fileData" hidden={true} />)}{" "}
            {getFieldDecorator("upload", {})(
              <div className="clearfix">
                <Upload
                  listType="picture-card"
                  fileList={fileList}
                  onPreview={this.handlePreview}
                  onChange={this.handleChange}
                  accept="images/*"
                  action={null}
                  style={{
                    backgroundImage: `url('${API_URL}/${this.props.settings.details.logo_url}')`,
                    backgroundSize: "contain"
                  }}
                >
                  {" "}
                  {fileList.length >= 1 ? null : uploadButton}{" "}
                </Upload>{" "}
                <Modal
                  visible={previewVisible}
                  footer={null}
                  onCancel={this.handleCancel}
                >
                  <img
                    alt="example"
                    style={{
                      width: "100%"
                    }}
                    src={previewImage}
                  />{" "}
                </Modal>{" "}
              </div>
            )}{" "}
          </Form.Item>{" "}
          <FormItem label="Company Name">
            {" "}
            {getFieldDecorator("company_name", {
              rules: [
                {
                  required: true,
                  message: "Please input organization full name!"
                }
              ]
            })(
              <Input
                disabled={this.state.disabledInputs}
                placeholder="ex: Example Inc."
                type="text"
              />
            )}{" "}
          </FormItem>{" "}
          <FormItem label="Company Email">
            {" "}
            {getFieldDecorator("company_email", {
              rules: [
                {
                  message: "Please input organization email!"
                }
              ]
            })(
              <Input
                disabled={this.state.disabledInputs}
                placeholder="ex: mail@example.inc"
                type="email"
              />
            )}{" "}
          </FormItem>{" "}
          <FormItem label="Company Phone">
            {" "}
            {getFieldDecorator("company_phone", {
              rules: [
                {
                  message: "Please input organization phone number!"
                }
              ]
            })(
              <Input
                disabled={this.state.disabledInputs}
                placeholder="ex: +233"
                type="text"
              />
            )}{" "}
          </FormItem>{" "}
          <FormItem label="Company Address">
            {" "}
            {getFieldDecorator("company_address", {
              rules: [
                {
                  message: "Please input organization address!"
                }
              ]
            })(
              <Input
                disabled={this.state.disabledInputs}
                placeholder="ex: +233"
                type="text"
              />
            )}{" "}
          </FormItem>{" "}
          <FormItem>
            <Row>
              <Col span={12}>
                <Button
                  type=""
                  htmlType="reset"
                  onClick={this.handleResetClicked}
                >
                  Reset{" "}
                </Button>{" "}
              </Col>{" "}
              <Col
                span={12}
                style={{
                  textAlign: "right"
                }}
              >
                <Button
                  type=""
                  htmlType={this.state.disabledInputs ? "button" : "submit"}
                  onClick={e => {
                    if (this.state.disabledInputs) {
                      e.preventDefault();
                      this.handleEditClicked.apply(this);
                    }
                  }}
                  icon={this.state.disabledInputs ? "edit" : "upload"}
                  loading={this.props.settings.loading}
                >
                  {" "}
                  {this.state.disabledInputs ? "Edit" : "Submit"}{" "}
                </Button>{" "}
              </Col>{" "}
            </Row>{" "}
          </FormItem>{" "}
        </Form>
      );
    }
  }
);

const mapStateToProps = state => {
  return {
    settings: state.settings
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetch_settings: () => dispatch(fetch_settings),
    update_settings_datasource: value =>
      dispatch(update_settings_datasource(value)),
    update_settings_loading: value => dispatch(update_settings_loading(value))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppCompanyForm);
