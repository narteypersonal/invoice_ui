import React from "react";
import { Row, Col, Form, Input, Icon, Button, InputNumber } from "antd";
let id = 0;
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 4
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 20
    }
  }
};
const formItemLayoutWithOutLabel = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0
    },
    sm: {
      span: 20,
      offset: 4
    }
  }
};
export class InvoiceProductDynamicFieldSet extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      update: true,
      keys: []
    };
  }

  remove = k => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = this.state.keys;
    // We need at least one passenger
    if (keys.length === 1) {
      return;
    }

    // can use data-binding to set
    form.setFieldsValue({
      keys: keys.filter(key => key !== k)
    });
  };

  add = () => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = this.state.keys;
    const nextKeys = keys.concat(id++);
    // can use data-binding to set
    // important! notify form to detect changes
    form.setFieldsValue({
      keys: nextKeys
    });
    this.setState({ update: true });
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    getFieldDecorator("keys", { initialValue: [] });
    const keys = this.state.keys;
    const formItems = keys.map((k, index) => (
      <Form.Item
        {...(index === 0 ? formItemLayout : formItemLayoutWithOutLabel)}
        label={index === 0 ? "Passengers" : ""}
        required={false}
        key={k}
      >
        {
          <Input
            placeholder="passenger name"
            style={{ width: "60%", marginRight: 8 }}
            type="text"
          />
        }
        {keys.length > 1 ? (
          <Icon
            className="dynamic-delete-button"
            type="minus-circle-o"
            onClick={() => this.remove(k)}
          />
        ) : null}
      </Form.Item>
    ));
    return (
      <Row>
        <Col span={24}>
          <div>
            {formItems}
            <Button type="dashed" onClick={this.add} style={{ width: "60%" }}>
              <Icon type="plus" /> Add field
            </Button>
          </div>{" "}
        </Col>{" "}
      </Row>
    );
  }
}
