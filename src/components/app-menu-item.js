import React, { Component } from "react";
import { Menu, Icon } from "antd";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import SubMenu from "antd/lib/menu/SubMenu";
import { update_invoice_modal_visible } from "../redux/actions/invoiceActions";
import {
  update_product_modal_visible,
  update_product_edit_details,
  update_product_add_action
} from "../redux/actions/productActions";
import { update_user_modal_visible } from "../redux/actions/userActions";
import { update_add_expenses_type_visible } from "../redux/actions/expensesTypeActions";
import { update_add_creditor_visible } from "../redux/actions/creditorActions";
import { update_purchase_invoice_drawer_visible } from "../redux/actions/purchaseActions";
import { update_stock_drawer_visible } from "../redux/actions/stockActions";

class AppMenuItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menus: [
        {
          key:"1",
          title: "Dashboard",
          icon: "home",
          hasPermission: true,
          is_admin: false,
          link: "/app"
        },
        {
          key: "2",
          title: "Invoices",
          icon: "gold",
          hasPermission: true,
          is_admin: false,
          subMenu: [
            {
              key: "3",
              title: "Add",
              icon: "plus",
              action: () => props.update_invoice_modal_visible(true)
            },
            {
              key: "4",
              title: "Records",
              icon: "ordered-list",
              link: "/app/invoices"
            }
          ]
        },
        {
          key: "5",
          title: "Receipts",
          icon: "transaction",
          hasPermission: true,
          is_admin: false,
          link: "/app/receipts"
        },
        {
          key: "6",
          title: "Reps Sales",
          icon: "monitor",
          hasPermission: true,
          is_admin: true,
          subMenu: [
            {
              key: "7",
              title: "Cash sales",
              icon: "account-book",
              link: "/app/sales/individual/accounts"
            },
            {
              key: "8",
              title: "Installments",
              icon: "credit-card",
              link: ""
            }
          ]
        },
        {
          key: "9",
          title: "General Sales",
          icon: "stock",
          hasPermission: true,
          is_admin: true,
          subMenu: [
            {
              key: "10",
              title: "Cash Sales",
              icon: "account-book",
              link: "/app/sales/general/accounts"
            },
            {
              key: "11",
              title: "Installements",
              icon: "credit-card",
              link: "/app/sales/general/journals"
            },
            {
              title: "Debtors",
              icon: "money-collect",
              link: "/app/sales/general/debtors-ledger"
            }
          ]
        },
        {
          key: "12",
          title: "Stocks",
          icon: "shopping-cart",
          hasPermission: true,
          is_admin: true,
          subMenu: [
            {
              key: "13",
              title: "Add Stock",
              icon: "plus",
              action: this.props.update_stock_drawer_visible.bind(this,true)
            },
            {
              key: "14",
              title: "Stock Balance",
              icon: "audit",
              hasPermission: true,
              is_admin: true,
              link: "/app/stocks/balance"
            },
            {
              key: "15",
              title: "Stock records",
              icon: "audit",
              hasPermission: true,
              is_admin: true,
              link: "/app/stocks"
            },
            {
              key: "16",
              title: "Add Product",
              icon: "plus",
              action: () => {
                this.props.update_product_edit_details({
                  productId: "",
                  productName: "",
                  productPrice: ""
                });
                this.props.update_product_modal_visible(true);
                this.props.update_product_add_action(false);
              }
            },
            {
              key: "17",
              title: "Product Records",
              icon: "ordered-list",
              link: "/app/products"
            }
          ]
        },
        {
          key: "18",
          title: "Cashbooks",
          icon: "fund",
          hasPermission: true,
          is_admin: true,
          subMenu: [
            {
              key: "19",
              title: "Cash",
              icon: "audit",
              hasPermission: true,
              is_admin: true,
              link: "/app/cashbooks"
            },
            {
              key: "20",
              title: "Bank",
              icon: "money-collect",
              link: "/app/sales/general/debtors-ledger"
            }
          ]
        },
        {
          key: "21",
          title: "Suppliers",
          icon: "shop",
          hasPermission: true,
          is_admin: true,
          subMenu: [
            {
              key: "22",
              title: "Add Supplier",
              icon: "plus",
              action: this.props.update_add_creditor_visible.bind(this, true)
            },
            {
              key: "23",
              title: "Suppliers",
              icon: "idcard",
              link: "/app/creditors"
            },

            {
              key: "24",
              title: "Records",
              icon: "ordered-list",
              link: "/app/credits"
            }
          ]
        },
        {
          key: "25",
          title: "Expenses",
          icon: "wallet",
          hasPermission: true,
          is_admin: true,
          subMenu: [
            {
              key: "26",
              title: "Add expense type",
              icon: "plus",
              action: this.props.update_add_expenses_type_visisble.bind(
                this,
                true
              )
            },
            {
              key: "27",
              title: "Expense Types",
              icon: "ordered-list",
              link: "/app/expensetype"
            },
            {
              key: "28",
              title: "Records",
              icon: "ordered-list",
              link: "/app/expenses"
            }
          ]
        },
        {
          key: "30",
          title: "Users",
          icon: "usergroup-add",
          hasPermission: true,
          is_admin: true,
          subMenu: [
            {
              key: "31",
              title: "Add",
              icon: "plus",
              action: this.props.update_user_modal_visible.bind(this, true)
            },
            {
              key: "32",
              title: "Records",
              icon: "ordered-list",
              link: "/app/users"
            }
          ]
        },
        {
          key: "29",
          title: "Options",
          icon: "setting",
          hasPermission: true,
          is_admin: true,
          action: null,
          link: "settings",
          subMenu: [
            {
              key: "33",
              title: "Settings",
              icon: "office",
              link: "/app/settings"
            }
          ]
        }
      ]
    };
  }

  makeSubMenu = (menu, keyIndex) => {
    return (
      <SubMenu
        key={`menu${menu.key}`}
        title={
          <span>
            <Icon type={menu.icon} /> <span> {menu.title} </span>{" "}
          </span>
        }
      >
        {" "}
        {menu.subMenu.map(sub => {
          ++keyIndex;
          return (
            <Menu.Item key={`sub${sub.key}`} onClick={sub.action}>
              {sub.link ? (
                <Link to={`${sub.link}`}>
                  {sub.icon && <Icon type={sub.icon} />}
                  <span className="nav-text">{sub.title}</span>
                </Link>
              ) : (
                  <div>
                    {sub.icon && <Icon type={sub.icon} />}
                    <span className="nav-text">{sub.title}</span>
                  </div>
                )}
            </Menu.Item>
          );
        })}{" "}
      </SubMenu>
    );;
  }
  generate = () => {
    let keyIndex = 0;
    return this.state.menus.map(menu => {
      let menuJSX = null;

      if (this.props.profile.is_admin)
        if (menu.subMenu && menu.subMenu.length > 0) {
          keyIndex++;
          menuJSX = this.makeSubMenu(menu, keyIndex)
        } else {
          menuJSX = (
            <Menu.Item key={`menu${menu.key}`}>
              {" "}
              {menu.link ? (
                <Link to={menu.link}>
                  {menu.icon && <Icon type={menu.icon} />}
                  <span className="nav-text"> {menu.title}</span>
                </Link>
              ) : (
                <div>
                  {menu.icon && <Icon type={menu.icon} />}
                  <span className="nav-text">{menu.title}</span>
                </div>
              )}
              <span className="nav-text"> {menu.title} </span>{" "}
            </Menu.Item>
          );
        }
      else if (menu.is_admin !== true)
        if (menu.subMenu && menu.subMenu.length > 0) {
          keyIndex++;
          menuJSX = (
            <SubMenu
              key={`menu${menu.key}`}
              title={
                <span>
                  <Icon type={menu.icon} /> <span> {menu.title} </span>{" "}
                </span>
              }
            >
              {" "}
              {menu.subMenu.map(sub => {
                ++keyIndex;
                return (
                  <Menu.Item key={`sub${sub.key}`} onClick={sub.action}>
                    {sub.link ? (
                      <Link to={sub.link}>
                        {sub.icon && <Icon type={sub.icon} />}
                        <span className="nav-text"> {sub.title}</span>
                      </Link>
                    ) : (
                      <div>
                        {sub.icon && <Icon type={sub.icon} />}
                        <span className="nav-text">{sub.title}</span>
                      </div>
                    )}
                  </Menu.Item>
                );
              })}{" "}
            </SubMenu>
          );
        } else {
          menuJSX = (
            <Menu.Item key={`menu${menu.key}`}>
              {" "}
              {menu.link ? (
                <Link to={menu.link}>
                  {menu.icon && <Icon type={menu.icon} />}
                  <span className="nav-text"> {menu.title}</span>
                </Link>
              ) : (
                <div>
                  {menu.icon && <Icon type={menu.icon} />}
                  <span className="nav-text">{menu.title}</span>
                </div>
              )}
              <span className="nav-text"> {menu.title} </span>{" "}
            </Menu.Item>
          );
        }
      return menuJSX;
    });
  };
  render() {
    return (
      <Menu
        theme="dark"
        mode="inline"
        defaultSelectedKeys={["menu1"]}
        style={{
          marginTop: "7vh",
          textAlign: "Left"
        }}
      >
        {" "}
        {this.generate()}{" "}
      </Menu>
    );
  }
}

const mapStateToProps = state => {
  return {
    profile: state.profile
  };
};

const mapDispatchToProps = dispatch => {
  return {
    update_invoice_modal_visible: value =>
      dispatch(update_invoice_modal_visible(value)),
    update_product_modal_visible: value =>
      dispatch(update_product_modal_visible(value)),
    update_product_edit_details: value =>
      dispatch(update_product_edit_details(value)),
    update_product_add_action: value =>
      dispatch(update_product_add_action(value)),
    update_user_modal_visible: value =>
      dispatch(update_user_modal_visible(value)),
    update_add_expenses_type_visisble: value =>
      dispatch(update_add_expenses_type_visible(value)),
    update_add_creditor_visible: value =>
      dispatch(update_add_creditor_visible(value)),
    update_purchase_invoice_drawer_visible: value => dispatch(update_purchase_invoice_drawer_visible(value)),
    update_stock_drawer_visible: value => dispatch(update_stock_drawer_visible(value))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AppMenuItem);
