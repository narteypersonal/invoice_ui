import React from "react";
import { DatePicker } from "antd";
import moment from "moment";
import { connect } from "react-redux";

const { RangePicker } = DatePicker;

function onChange(dates, dateStrings) {
  console.log("From: ", dates[0], ", to: ", dates[1]);
  console.log("From: ", dateStrings[0], ", to: ", dateStrings[1]);
}

const AppDateRangePicker = props => {
  return (
    <RangePicker
      getCalenderContainer={trigger => {
        console.log(trigger);
        return document.getElementById(props.containerId);
      }}
      ranges={{
        Today: [moment(), moment()],
        "This Month": [moment().startOf("month"), moment().endOf("month")]
      }}
      onChange={onChange}
    />
  );
};

export default connect()(AppDateRangePicker);
